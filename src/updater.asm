; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

UpdateDownload proc _ignore:DWORD
LOCAL temp[256]             :BYTE
LOCAL temp2[512]            :BYTE

    invoke  wsprintf, ADDR update_url2, ADDR update_url_template, ADDR updateReadBuf
    invoke  GetDesktopFolder, ADDR temp
    invoke  wsprintf, ADDR temp2, ADDR update_download_template, ADDR temp, ADDR updateReadBuf
    invoke  DownloadFile, ADDR update_url2, ADDR temp2, FALSE
    cmp     [updateStopStatus], TRUE
    jz      @@DEL_FILE
    mov     [updateDoneStatus], eax
    jmp     @@EXIT
  @@DEL_FILE:
    invoke  DeleteFile, ADDR temp2
  @@EXIT:
    ; Decrease thread count
    dec     [gThreadCount]
    ; Exit the thread
    invoke  ExitThread, 0
UpdateDownload endp

UpdateDialogProc proc hwndDlg:DWORD,uMsg:DWORD,wParam:DWORD,lParam:DWORD
LOCAL temp                  :DWORD

    .if [uMsg] == WM_INITDIALOG
        mov     [updateDoneStatus], -2
        mov     [updateStopStatus], FALSE
        invoke  SendMessage, [hwndDlg], WM_SETICON, ICON_BIG, [hMainIcon]
        invoke  CreateThread, 0, 0, ADDR UpdateDownload, 0, 0, ADDR temp
        ; Increase thread count
        inc     [gThreadCount]
        ; Update other thread variables
        mov     [updateThreadHandle], eax
        mov     [updateTimerFirstMsg], TRUE
        ; Spawn a timer to check periodically for the status of the update process
        invoke  SetTimer, [hwndDlg], ID_UPDATE_TIMER, 500, 0
    .elseif [uMsg] == WM_TIMER
        .if [updateTimerFirstMsg] == TRUE
            mov     [updateTimerFirstMsg], FALSE
            invoke  SetForegroundWindow, [hwndDlg]
        .endif
        .if [updateDoneStatus] != -2
            invoke  KillTimer, [hwndDlg], ID_UPDATE_TIMER
            ; Check if update is downloaded...
            .if [updateDoneStatus] == 0
                invoke  MessageBox, [hwndDlg], ADDR update_done, ADDR app_name, MB_ICONINFORMATION
            .elseif [updateDoneStatus] == -1
                invoke  MessageBox, [hwndDlg], ADDR update_error_hnf, ADDR app_name, MB_ICONEXCLAMATION
            .elseif [updateDoneStatus] == 1
                invoke  MessageBox, [hwndDlg], ADDR update_error_mem, ADDR app_name, MB_ICONEXCLAMATION
            .elseif [updateDoneStatus] == 2
                invoke  MessageBox, [hwndDlg], ADDR update_error_conn, ADDR app_name, MB_ICONEXCLAMATION
            .elseif [updateDoneStatus] == 3
                invoke  MessageBox, [hwndDlg], ADDR update_error_fnf, ADDR app_name, MB_ICONEXCLAMATION
            .elseif [updateDoneStatus] == 4
                invoke  MessageBox, [hwndDlg], ADDR update_error_fna, ADDR app_name, MB_ICONEXCLAMATION
            .endif
            invoke  SendMessage, [hwndDlg], WM_CLOSE, 0, 0
        .endif
    .elseif [uMsg] == WM_COMMAND
        .if [wParam] == ID_UPDATE_STOP
            mov     [updateStopStatus], TRUE
            invoke  SendMessage, [hwndDlg], WM_CLOSE, 0, 0
        .elseif [wParam] == IDCANCEL
            invoke  SendMessage, [hwndDlg], WM_CLOSE, 0, 0
        .endif
    .elseif [uMsg] == WM_CLOSE
        invoke  KillTimer, [hwndDlg], ID_UPDATE_TIMER
        invoke  EndDialog, [hwndDlg], 0
    .endif
    xor     eax, eax
    ret
UpdateDialogProc endp

UpdateThread proc bNotify:DWORD
LOCAL numb                  :DWORD

    mov     [UpdateAvailable], FALSE
    invoke  GetEnvironmentVariable, ADDR EnvironmentTemp, ADDR updateTempBuf, SIZEOF updateTempBuf
    invoke  StrCat, ADDR updateTempBuf, ADDR update_versiontxt
    invoke  DownloadFile, ADDR update_url, ADDR updateTempBuf, TRUE
    .if eax == 0
        invoke  CreateFile, ADDR updateTempBuf, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, \
                FILE_ATTRIBUTE_NORMAL, 0
        cmp     eax, -1
        jz      @@END_UPDATE_CHECK
        push    eax
        push    eax
        invoke  GetFileSize, eax, 0
        pop     ecx
        lea     edx, [numb]
        invoke  ReadFile, ecx, ADDR updateReadBuf, eax, edx, 0
        pop     eax
        invoke  CloseHandle, eax
        invoke  DeleteFile, ADDR updateTempBuf
        mov     eax, OFFSET updateReadBuf
        add     eax, [numb]
        mov     byte ptr [eax], 0
        ; check if it's necessary to update
        invoke  lstrcmp, ADDR updateReadBuf, ADDR update_string
        .if eax == 1
            mov     [UpdateAvailable], TRUE
            ; wait until the user opens the main dialog again (from the notification area)
            .while [MainDialogHidden] == TRUE
                invoke  Sleep, 10
            .endw
            ; ask the user if they want to download the update
            invoke  MessageBox, [ghwndDlg], ADDR update_available, ADDR app_name, MB_ICONINFORMATION or MB_YESNOCANCEL
            .if eax == IDYES
                ; yes, then update...
                invoke  DialogBoxParam, [hInstance], ID_UPDATE_DIALOG, 0, ADDR UpdateDialogProc, 0
            .endif
        .elseif [bNotify] == TRUE
            invoke  SendMessage, [ghwndDlg], WM_UPDATE_NOTIFY, MB_ICONINFORMATION, ADDR update_check_no_updates
        .endif
    .elseif [bNotify] == TRUE
        invoke  SendMessage, [ghwndDlg], WM_UPDATE_NOTIFY, MB_ICONEXCLAMATION, ADDR update_fail
    .endif
  @@END_UPDATE_CHECK:
    mov     [updateMainThreadHandle], 0
    ; Decrease thread count
    dec     [gThreadCount]
    ; Exit the thread
    invoke  ExitThread, 0
UpdateThread endp

CheckForProgramUpdates proc hwndDlg:DWORD, uMsg:DWORD, idEvent:DWORD, dwTime:DWORD
LOCAL bNotify               :DWORD

    .if [uMsg] == UPDATE_CHECK_NOTIFY
        mov [bNotify], 1
    .else
        mov [bNotify], 0
    .endif

    invoke  CreateThread, 0, 0, ADDR UpdateThread, [bNotify], 0, ADDR updateMainThreadHandle
    ; Update the main update thread handle (used by the thread itself)
    mov     [updateMainThreadHandle], eax
    ; Increase thread count
    inc     [gThreadCount]
    ret
CheckForProgramUpdates endp

UpdateProgramUpdateTimer proc hwndDlg:DWORD
    mov     al, [prefs_updateinterval]
    .if al == 3 ; No automatic update checks
        .if [status_bUpdateTimerRunning] == TRUE
            invoke  KillTimer, [hwndDlg], ID_UPDATE_CHECK_TIMER
        .endif
        mov     [status_bUpdateTimerRunning], FALSE
    .else
        .if al == 0
            mov     eax, 1000 * 60 * 60 * 24        ; Daily
        .elseif al == 1
            mov     eax, 1000 * 60 * 60 * 24 * 7    ; Weekly
        .elseif al == 2
            mov     eax, 1000 * 60 * 60 * 24 * 31   ; Monthly
        .endif
        invoke  SetTimer, [hwndDlg], ID_UPDATE_CHECK_TIMER, eax, ADDR CheckForProgramUpdates
        mov     [status_bUpdateTimerRunning], TRUE
    .endif
    ret
UpdateProgramUpdateTimer endp

UpdateSettingsDialogProc proc hwndDlg:DWORD, uMsg:DWORD, wParam:DWORD, lParam:DWORD
LOCAL hwndItem              :DWORD

    .if [uMsg] == WM_INITDIALOG
        ; Set the window icon
        invoke  SendMessage, [hwndDlg], WM_SETICON, ICON_BIG, [hMainIcon]

        ; Add the strings 'Never', 'Daily', 'Weekly' and 'Monthly' to the combobox
        invoke  GetDlgItem, [hwndDlg], ID_SETTINGS_UPDATE_INTERVAL
        mov     [hwndItem], eax
        invoke  SendMessage, eax, CB_ADDSTRING, 0, ADDR settings_update_daily
        invoke  SendMessage, [hwndItem], CB_ADDSTRING, 0, ADDR settings_update_weekly
        invoke  SendMessage, [hwndItem], CB_ADDSTRING, 0, ADDR settings_update_monthly
        invoke  SendMessage, [hwndItem], CB_ADDSTRING, 0, ADDR settings_update_never

        ; Set the current selection
        invoke  SendMessage, [hwndItem], CB_SETCURSEL, [prefs_updateinterval], 0

        ; Set the current 'check for updates at startup' setting
        .if [prefs_update_atstart] == 1
            invoke  GetDlgItem, [hwndDlg], IDC_SETTINGS_UPDATE_ATSTART
            invoke  SendMessage, eax, BM_SETCHECK, BST_CHECKED, 0
        .endif
    .elseif [uMsg] == WM_COMMAND
        mov     eax, [wParam]
        .if ax == ID_SETTINGS_UPDATE_INTERVAL
        .elseif ax == IDC_SETTINGS_UPDATE_ATSTART
            invoke  GetDlgItem, [hwndDlg], IDC_SETTINGS_UPDATE_ATSTART
            mov     [hwndItem], eax
            invoke  SendMessage, eax, BM_GETCHECK, 0, 0
            .if eax == BST_CHECKED
                mov     eax, BST_UNCHECKED
            .elseif eax == BST_UNCHECKED
                mov     eax, BST_CHECKED
            .endif
            invoke  SendMessage, [hwndItem], BM_SETCHECK, eax, 0
        .elseif ax == ID_SETTINGS_UPDATE_APPLY
            ; Get the update interval setting
            invoke  GetDlgItem, [hwndDlg], ID_SETTINGS_UPDATE_INTERVAL
            invoke  SendMessage, eax, CB_GETCURSEL, 0, 0
            ; Update the run-time setting
            mov     [prefs_updateinterval], al
            ; Save the update interval setting
            invoke  cfg_SaveInteger, [hwndDlg], [hIniFile], ADDR cfg_update_interval_header, eax
            ; Check for errors
            .if eax == 0
                jmp     @@ERROR_SAVE
            .endif

            ; Update the program update check timer (assign to main window)
            invoke  UpdateProgramUpdateTimer, [ghwndDlg]

            ; Save the 'auto update at program startup' setting
            invoke  GetDlgItem, [hwndDlg], IDC_SETTINGS_UPDATE_ATSTART
            invoke  SendMessage, eax, BM_GETCHECK, 0, 0
            xor     ecx, ecx
            .if eax == BST_CHECKED
                setz    cl
            .endif
            mov     [prefs_update_atstart], cl
            invoke  cfg_SaveInteger, [hwndDlg], [hIniFile], ADDR cfg_update_atstart_header, ecx
            ; Check for errors
            .if eax == 0
                jmp     @@ERROR_SAVE
            .endif

            ; If everything went fine, close the dialog
            invoke  SendMessage, [hwndDlg], WM_CLOSE, 0, 0
            jmp     @@EXIT

          @@ERROR_SAVE:
            invoke  MessageBox, [hwndDlg], ADDR settings_save_error, ADDR sorry_caption, MB_ICONSTOP
        .elseif ax == IDCANCEL
            invoke  SendMessage, [hwndDlg], WM_CLOSE, 0, 0
        .endif
    .elseif [uMsg] == WM_CLOSE
        invoke  EndDialog, [hwndDlg], 0
    .endif

    @@EXIT:
    xor     eax, eax
    ret
UpdateSettingsDialogProc endp
