; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

; Returns pointer to commandline arguments if a commandline has been found.
; Returns 0 if no commandline arguments found.
GetCmdLineArgs proc uses edi
LOCAL CmdLine               :DWORD
LOCAL LastLoop              :DWORD

    invoke  GetCommandLine
    mov     edi, eax
    mov     [CmdLine], eax
    invoke  StrLen, edi
    mov     ecx, eax                    ; ECX = string length of commandline
    cld                                 ; Clear Direction Flag (search forward)
    cmp     byte ptr [edi], '"'
    jnz     @@CMDLINE_SEARCH_SPACE      ; Search next space (use as separator)
    mov     eax, '"'                    ; Else, search for next " character (use as separator)
    inc     edi                         ; +1 to search after the first " character
    repnz   scasb                       ; Do a search
    jnz     @@CMDLINE_NO_ARGS           ; Not found?!
    jmp     @@CMDLINE_PARSE
  @@CMDLINE_SEARCH_SPACE:
    ; ECX = string length of commandline
    mov     eax, ' '
    repnz   scasb
    jnz     @@CMDLINE_ERROR
    jmp     @@CMDLINE_DONE
  @@CMDLINE_PARSE:
    cmp     byte ptr [edi], 0           ; Commandline ends already?
    jz      @@CMDLINE_NO_ARGS
    inc     edi                         ; +1 to point to first argument
    cmp     byte ptr [edi], 0           ; Commandline ends already?
    jz      @@CMDLINE_NO_ARGS           ; No arguments passed on commandline
    jmp     @@CMDLINE_DONE
  @@CMDLINE_ERROR:
    mov     eax, -1
    jmp     @@CMDLINE_DONE
  @@CMDLINE_NO_ARGS:
    xor     eax, eax                    ; Indicate no arguments found
    ret
  @@CMDLINE_DONE:
    mov     eax, edi
    ret
GetCmdLineArgs endp

; Returns a pointer to the requested argument from the commandline.
; Everything between the '"' characters not parsed. e.g. "test".
;
; Also every argument (i.e. "test test2", separated by spaces) not
; starting with the first character of the argument to search for will
; not be parsed. This is done to prevent the parser from thinking that
; an argument has been found within another argument.
; e.g. "-h .\ta-del" would otherwise let the parser think that "-de" has
; been found when we were searching for "-h" instead.
;
; Returns the pointer on success, or -1 on failure.
GetCmdLineArgPtr proc uses ebx esi edi lpszCommandLine:DWORD, lpszArg:DWORD
LOCAL fDontParse            :DWORD

    mov     [fDontParse], FALSE
    mov     esi, [lpszCommandLine]

    ; Loop through all characters until a matching character was found
  @@LOOP:
    lodsb
    or      al, al
    jnz    @F
    ; Failure
    xor     eax, eax
    dec     eax
    ret

  @@:
    ; Don't parse characters between '"' characters. e.g.: "test" (here, test is not parsed)
    .if al == '"'
        xor     [fDontParse], TRUE ; Set DontParse flag to TRUE if it was FALSE, and FALSE if it was TRUE
    .elseif [fDontParse] == FALSE
        ; Check if the argument starts with the first character of the argument that we are in search of.
        invoke  StrLen, [lpszArg]
        mov     ecx, eax
        mov     al, ' '
        mov     edi, esi
        dec     edi
        std
        repnz   scasb
        cld
        inc     edi
        mov     edx, [lpszArg]
        mov     dl, [edx]

        mov     eax, [lpszCommandLine]
        cmp     byte ptr [eax], dl
        jz      @F
        cmp     dl, byte ptr [edi+1]
        jnz     @@LOOP

      @@:
        mov     ebx, [lpszArg]
        invoke  StrLen, ebx
        push    eax
        push    ebx
        push    esi
        dec     dword ptr [esp] ; one less because "lodsb" increased ESI
        call    StrCmpIn
        .if eax == 0
            ; now check if a space or NULL byte follows the argument (should be)
            invoke  StrLen, ebx
            mov     edx, esi
            add     edx, eax
            cmp     byte ptr [edx-1], 0
            jz      @F
            cmp     byte ptr [edx-1], ' '
            jnz     @@LOOP
          @@:

            ; success
            .while byte ptr [edi] == ' '
                inc     edi
            .endw
            mov     eax, esi
            dec     eax ; one less because "lodsb" increased ESI
            sub     eax, [lpszCommandLine]
            jmp     @@RET
        .endif
    .endif
    jmp     @@LOOP

  @@RET:
    ret
GetCmdLineArgPtr endp

; Returns the index of where the string after the requested argument
; from the commandline.was found.
;
; Returns the index in EAX and the length of the string in ECX on success.
; On failure (i.e. argument or string not found) a value of -1 is returned.
GetCmdLineArgString proc uses esi edi lpszCommandLine:DWORD, lpszArg:DWORD
    invoke  GetCmdLineArgPtr, [lpszCommandLine], [lpszArg]
    cmp     eax, -1
    jz      @@RET

    add     eax, [lpszCommandLine]
    mov     edi, eax
    add     edi, 2
    cmp     byte ptr [edi], 0       ; If null ..
    jz      @@PARSING_ERROR         ; .. then the argument is invalid
    inc     edi
    mov     esi, edi
    invoke  StrLen, edi
    mov     ecx, eax
    cmp     byte ptr [edi], '"'
    jz      @@SEARCH_QUOTMARK
    mov     al, ' '
    cld
    repnz   scasb
    pushfd
    pop     eax
    cmp     byte ptr [edi], 0
    lea     edi, [edi+1]            ; EDI = EDI + 1 (without modifying CPU flags)
    jz      @@DONE
    dec     edi                     ; Decrease if not NULL (means not last argument and length already calculated correctly)
    push    eax
    popfd
    jnz     @@PARSING_ERROR
    jmp     @@DONE

  @@SEARCH_QUOTMARK:
    mov     eax, '"'
    cld
    inc     edi
    inc     esi
    repnz   scasb
    jnz     @@PARSING_ERROR

  @@DONE:
    mov     ecx, edi
    sub     ecx, esi
    mov     eax, esi
    sub     eax, [lpszCommandLine]

    invoke  StrLen, esi
    inc     eax ; preserve one NULL byte (always needed, we can't garantuee it's already
                ; preserved because the string not always ends with a '"' character)
    invoke  GlobalAlloc, GPTR, eax
    cmp     eax, -1
    jz      @@PARSING_ERROR
    mov     ecx, edi
    sub     ecx, esi
    push    eax
    invoke  lstrcpyn, eax, esi, ecx
    pop     eax
    jmp     @@RET

  @@PARSING_ERROR:
    mov     eax, -1

  @@RET:
    ret
GetCmdLineArgString endp

; Retrieves the integer after the requested argument from the commandline.
; Returns the pointer on success, or -1 on failure.
GetCmdLineArgInt proc uses ebx esi edi lpszCommandLine:DWORD, lpszArg:DWORD
LOCAL lpIntAscii[16]        :BYTE

    invoke  GetCmdLineArgPtr, [lpszCommandLine], [lpszArg]
    cmp     eax, -1
    jz      @@RET

    mov     ebx, [lpszCommandLine]
    add     eax, 2
    cmp     byte ptr [ebx+eax], ' ' ; Check if there is a space between the port and the argument
    jnz     @@PARSING_ERROR
    inc     ebx
    add     ebx, eax                ; EBX = pointer to port number
    mov     edi, ebx
    invoke  StrLen, ebx
    mov     ebx, edi
    mov     eax, ' '
    mov     ecx, eax
    ; cld
    repnz   scasb
    jz      @@GET_INT_LENGTH_TILL_SPACE
    invoke  StrLen, ebx
    mov     ecx, eax
    inc     ecx
    jmp     @@COPY_INT

  @@GET_INT_LENGTH_TILL_SPACE:
    mov     ecx, edi
    sub     ecx, ebx
    ; jmp   @@COPY_INT

  @@COPY_INT:
    invoke  lstrcpyn, ADDR lpIntAscii, ebx, ecx
    invoke  atodw, ADDR lpIntAscii
    jmp     @@RET

  @@PARSING_ERROR:
    mov     eax, -1

  @@RET:
    ret
GetCmdLineArgInt endp
