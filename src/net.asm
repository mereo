; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

; Converts a dotted IP address string to a DWORD value
; Input  = A dotted IP address string
; Output = converted value (4 bytes in length)
;          bits 31-0 represent the four IP address octets from left to right
; For example: 123.75.12.157 will be converted to 0x7B (bits 31 to 24),
;              0x4B (bits 23 to 16), 0x0C (bits 15 to 8), 0x9D (bits 7 to 0)
ConvertDottedIPStringToDw proc uses ebx esi edi lpszIPString:DWORD
LOCAL convertDw             :DWORD
LOCAL convertPos            :BYTE
LOCAL tempOctet[4]          :BYTE

    mov     [convertDw], 0
    mov     [convertPos], 24
    xor     ebx, ebx
    dec     ebx
    mov     esi, [lpszIPString]

  @@SEARCH_LOOP:
    invoke  ClearBuffer, ADDR tempOctet, SIZEOF tempOctet
    xor     edi, edi
    dec     edi
  @@SEARCH:
    inc     ebx
    inc     edi
    mov     al, byte ptr [esi+ebx]
    or      al, al
    jz      @@CONVERT
    cmp     al, '.'
    jz      @@CONVERT
    mov     byte ptr [tempOctet+edi], al
    jmp     @@SEARCH

  @@CONVERT:
    invoke  atodw, ADDR tempOctet
    mov     cl, [convertPos]
    shl     eax, cl
    or      [convertDw], eax
    sub     [convertPos], 8
    cmp     [convertPos], 0
    jnl     @@SEARCH_LOOP
    ; jmp   @@DONE

  @@DONE:
    mov     eax, [convertDw]
    jmp     @@RET

  @@ERROR:
    mov     eax, -1
    ; jmp   @@RET

  @@RET:
    ret
ConvertDottedIPStringToDw endp

; Inserts HTML links to go back to a specified directory.
; e.g. "/test1/test2" is converted to two HTML links targetting at "test1" and "test2".
;
; Return values:
; Returns a pointer to the new HTML backlinks buffer on success or FALSE on failure.
; The buffer returned _MUST_ be freed using GlobalFree.
DirBrowseInsertHtmlBackLinks proc uses ebx esi edi lpszDirBrowseLinks:DWORD
LOCAL lStrLen               :DWORD
LOCAL lcCounter             :DWORD
LOCAL lcCountsLeft          :DWORD
LOCAL szBackLinkHref[4]     :BYTE
LOCAL szSpacer[4]           :BYTE
LOCAL szSpacer2[2]          :BYTE
LOCAL lSearchFor[2]         :BYTE
LOCAL pLastOccurrence       :DWORD
LOCAL pNewDirBrowseLinks    :DWORD

    ; Initialize locals
    invoke  GlobalAlloc, GPTR, 8192
    .if eax == FALSE
        ret
    .endif
    mov     [pNewDirBrowseLinks], eax

    mov     [lcCounter], 0

    mov     byte ptr [lSearchFor], '/'
    mov     byte ptr [lSearchFor+1], 0

    mov     byte ptr [szBackLinkHref], '.'
    mov     byte ptr [szBackLinkHref+1], '.'
    mov     byte ptr [szBackLinkHref+2], '/'
    mov     byte ptr [szBackLinkHref+3], 0

    mov     byte ptr [szSpacer], ' '
    mov     byte ptr [szSpacer+1], '/'
    mov     byte ptr [szSpacer+2], 0

    mov     byte ptr [szSpacer2], ' '
    mov     byte ptr [szSpacer2+1], 0

    ; Loop initialization
    invoke  StrLen, [lpszDirBrowseLinks]
    mov     [lStrLen], eax
    mov     esi, [lpszDirBrowseLinks]
    inc     esi
    mov     edi, [pNewDirBrowseLinks]
    invoke  StrCpy, edi, ADDR backlinks_top

    ; Find the total number of '/' characters
    invoke  strchrcnt, esi, '/'
    dec     eax
    mov     [lcCountsLeft], eax

  @@LOOP:
    invoke  instringi, esi, ADDR lSearchFor
    .if eax == -1
        jmp     @@DONE
    .endif
    inc     [lcCounter]
    mov     [pLastOccurrence], eax ; Save the position of where the '/' character has been found

    invoke  StrLen, edi
    add     edi, eax
    invoke  StrCpy, edi, ADDR szSpacer2

    invoke  StrLen, edi
    add     edi, eax
    invoke  StrCpy, edi, ADDR html_a_tag_open

    mov     ebx, [lcCountsLeft]
    .while ebx != 0
        dec     ebx

        invoke  StrLen, edi
        add     edi, eax
        invoke  StrCpy, edi, ADDR szBackLinkHref
    .endw
    dec     [lcCountsLeft]

    invoke  StrLen, edi
    add     edi, eax
    invoke  StrCpy, edi, ADDR html_a_tag_first_close

    invoke  StrLen, edi
    add     edi, eax
    mov     ecx, [pLastOccurrence]
    inc     ecx
    invoke  lstrcpyn, edi, esi, ecx

    invoke  StrLen, edi
    add     edi, eax
    invoke  StrCpy, edi, ADDR html_a_tag_second_close

    invoke  StrLen, edi
    add     edi, eax
    invoke  StrCpy, edi, ADDR szSpacer

    add     esi, [pLastOccurrence]
    inc     esi
    jmp     @@LOOP

  @@DONE:
    .if [lcCounter] == 0
        invoke  GlobalFree, [pNewDirBrowseLinks]
        xor     eax, eax
    .else
        mov     eax, [pNewDirBrowseLinks]
    .endif
    ret
DirBrowseInsertHtmlBackLinks endp

; Returns the hostname found in the URL in the buffer specified as lpBuffer.
.data
url_http        db "http://", 0
.code
GetHostNameFromURL proc uses edi lpBuffer:DWORD, lpURL:DWORD
LOCAL tempBuf[64]           :BYTE

    mov     edi, [lpURL]
    mov     al, byte ptr [edi+7]
    push    eax     ; Save 8th byte
    mov     byte ptr [edi+7], 0
    invoke  StrCmp, edi, ADDR url_http
    pop     ecx     ; Restore 8th byte
    mov     byte ptr [edi+7], cl
    .if eax == 0
        ; Means 'http://' is in URL
        add     edi, 7
    .endif
    invoke  StrCpy, ADDR tempBuf, edi
    lea     edi, [tempBuf]
    .while byte ptr [edi] != 0 && byte ptr [edi] != ':' && byte ptr [edi] != '/'
        inc     edi
    .endw
    mov     al, byte ptr [edi]
    push    eax ; Save byte
    mov     byte ptr [edi], 0
    invoke  StrCpy, [lpBuffer], ADDR tempBuf
    pop     eax ; Restore byte
    mov     byte ptr [edi], al
    ret
GetHostNameFromURL endp

GetHostFromURL proc uses esi edi lpBuffer:DWORD, lpURL:DWORD
LOCAL temp                  :DWORD

    mov     edi, [lpURL]
    mov     al, byte ptr [edi+7]
    push    eax     ; Save 8th byte
    mov     byte ptr [edi+7], 0
    invoke  StrCmp, edi, ADDR url_http
    pop     ecx     ; Restore 8th byte
    mov     byte ptr [edi+7], cl
    .if eax == 0
        ; Means 'http://' is in URL
        add     edi, 7
    .endif
    mov     [temp], edi
    invoke  StrLen, edi
    mov     ecx, eax
    mov     eax, '.'
    repnz   scasb
    jnz     @@DEFAULT
    mov     esi, edi
    repnz   scasb
    jnz     @@DEFAULT
    mov     al, byte ptr [esi-1]
    mov     byte ptr [esi-1], 0
    push    eax
    invoke  StrCpy, [lpBuffer], [temp]
    pop     ecx
    mov     byte ptr [esi-1], cl
    jmp     @@RET
  @@DEFAULT:
  @@RET:
    ret
GetHostFromURL endp

GetPortFromURL proc uses esi edi lpPort:DWORD,lpURL:DWORD
LOCAL tempBuf[64]           :BYTE
LOCAL temp                  :DWORD

    mov     edi, [lpURL]
    mov     al, byte ptr [edi+7]
    push    eax     ; Save 8th byte
    mov     byte ptr [edi+7], 0
    invoke  StrCmp, edi, ADDR url_http
    pop     ecx     ; Restore 8th byte
    mov     byte ptr [edi+7], cl
    .if eax == 0
        ; Means 'http://' is in URL
        add     edi, 7
    .endif
    invoke  StrCpy, ADDR tempBuf, edi
    lea     edi, [tempBuf]
    .while byte ptr [edi] != 0 && byte ptr [edi] != ':' && byte ptr [edi] != '/'
        inc     edi
    .endw
    cmp     byte ptr [edi], '/'
    jz      @@NONE
    cmp     byte ptr [edi], 0
    jz      @@NONE
    inc     edi
    mov     [temp], edi
    invoke  StrLen, edi
    mov     ecx, eax
    mov     al, '/'
    cld
    repnz   scasb
    dec     edi
    mov     al, byte ptr [edi]
    push    eax
    mov     byte ptr [edi], 0
    invoke  atodw, [temp]
    pop     ecx
    mov     byte ptr [edi], cl
    jmp     @@RET
  @@NONE:
    mov     eax, -1
  @@RET:
    ret
GetPortFromURL endp

; Returns -1 if no valid file path found in URL
GetFilePathFromURL proc uses edi lpURL:DWORD, lpFileBuffer:DWORD
LOCAL tempBuf[64]           :BYTE
LOCAL temp                  :DWORD

    mov     edi, [lpURL]
    mov     al, byte ptr [edi+7]
    push    eax     ; Save 8th byte
    mov     byte ptr [edi+7], 0
    invoke  StrCmp, edi, ADDR url_http
    pop     ecx     ; Restore 8th byte
    mov     byte ptr [edi+7], cl
    .if eax == 0
        ; Means 'http://' is in URL
        add     edi, 7
    .endif
    invoke  StrLen, edi
    mov     ecx, eax
    mov     eax, '/'
    cld
    repnz   scasb
    jnz     @@ERROR
    dec     edi
    invoke  StrCpy, [lpFileBuffer], edi
    jmp     @@RET
  @@ERROR:
    mov     eax, -1
  @@RET:
    ret
GetFilePathFromURL endp

; Download File Function
; Error code 0, File Successfully Downloaded

; Error code 1, Memory Error:
; No memory available for the function to work properly.

; Error code 2, Connection Error:
; Bad connection or faulty server

; Error code 3, File Not Found:
; Server didn't find file or server is busy/not functioning.

; Error code 4, File Could Not Be Created:
; File could not be created on the local PC, probably
; the directory where the file should be saved doesn't
; exist or no access is allowed.
DownloadFile proc uses edi lpURL:DWORD, lpDestFile:DWORD, overwriteIfExists:DWORD
LOCAL hSock:DWORD, hFile:DWORD, pFile:DWORD, saddr:sockaddr_in
LOCAL port:DWORD, tempBuf[64]:BYTE, recvData:DWORD, int_true:DWORD, FileSize:DWORD
LOCAL at_end:DWORD, FirstPartSize:DWORD, recvCount:DWORD, temp:DWORD, numb:DWORD

    mov     [at_end], FALSE
    mov     [int_true], TRUE
    invoke  socket, AF_INET, SOCK_STREAM, IPPROTO_IP
    mov     [hSock], eax
    mov     [saddr.sin_family], AF_INET

    ; Parse hostname
    invoke  GetHostNameFromURL, ADDR tempBuf, [lpURL]
    invoke  gethostbyname, ADDR tempBuf
    .if eax != 0
        mov     eax, [eax+12]
        mov     eax, [eax]
        mov     eax, [eax]
    .else
        jmp     @@ERROR
    .endif
    mov     [saddr.sin_addr.S_un], eax

    ; Parse port
    invoke  GetPortFromURL, ADDR port, [lpURL]
    .if eax != -1
        xchg    al, ah      ; Convert to TCP/IP network byte order
    .else
        mov     ax, 5000h   ; Port 80 converted to TCP/IP network byte order (default HTTP port)
    .endif

    mov     [saddr.sin_port], ax
    mov     [saddr.sin_zero], 0

    invoke  connect, [hSock], ADDR saddr, SIZEOF sockaddr_in
    .if eax != 0
        jmp     @@ERROR
    .endif

    ; Construct HTTP GET request message
    invoke  GetFilePathFromURL, [lpURL], ADDR tempBuf
    invoke  GetHostFromURL, ADDR DownloadFile_host, [lpURL]
    invoke  wsprintf, ADDR http_msgBuffer, ADDR http_msgGET, ADDR tempBuf, ADDR DownloadFile_host
    invoke  StrLen, ADDR http_msgBuffer
    inc     eax
    invoke  send, [hSock], ADDR http_msgBuffer, eax, 0
    invoke  ioctlsocket, [hSock], FIONBIO, ADDR int_true

    ; Allocate recvData buffer
    invoke  GlobalAlloc, GPTR, ONE_MEGABYTE
    cmp     eax, -1
    jz      @@MEM_ERROR
    mov     [recvData], eax

    .while TRUE
        invoke  recv, [hSock], [recvData], ONE_MEGABYTE, 0
        mov     [recvCount], eax
        .if eax == -1
            invoke  WSAGetLastError
            cmp     eax, WSAEWOULDBLOCK
            jnz     @@CONN_ERROR
            invoke  Sleep, 10
        .else
            .break
        .endif
    .endw

    ; Check if 200 OK status received
    invoke  StrLen, [recvData]
    mov     ecx, eax
    mov     eax, ' '
    mov     edi, [recvData]
    cld
    repnz   scasb
    jnz     @@CONN_ERROR
    mov     [temp], edi
    repnz   scasb
    jnz     @@CONN_ERROR
    dec     edi
    mov     al, byte ptr [edi]
    push    eax
    mov     byte ptr [edi], 0
    invoke  StrCmpI, [temp], ADDR http_status200
    pop     ecx
    mov     byte ptr [edi], cl
    or      eax, eax
    jnz     @@FILE_NOT_FOUND_ERROR

    ; Parse content length
    invoke  instringi, edi, ADDR content_length
    .if eax == -1
        invoke  instringi, edi, ADDR double_crlf
        cmp     eax, -1
        jz      @@CONN_ERROR
        add     eax, SIZEOF double_crlf-1
        mov     [at_end], TRUE
    .else
        add     eax, SIZEOF content_length-1
    .endif
    add     edi, eax
    cmp     byte ptr [edi], ' '
    jnz     @F
    inc     edi
  @@:
    mov     [temp], edi
    invoke  StrLen, edi
    mov     ecx, eax
    mov     eax, CR
    cld
    repnz   scasb
    dec     edi
    mov     byte ptr [edi], 0
    invoke  atodw, [temp]
    mov     [FileSize], eax
    mov     byte ptr [edi], CR
    .if [at_end] == TRUE
        inc     edi
        cmp     byte ptr [edi], LF
        jnz     @F
        inc     edi
    @@:
        mov     [pFile], edi
    .else
        invoke  instringi, edi, ADDR double_crlf
        add     eax, SIZEOF double_crlf-1
        add     edi, eax
        mov     [pFile], edi
        sub     edi, [recvData]
        sub     edi, [recvCount]
        neg     edi
        mov     [FirstPartSize], edi
    .endif

    push    0
    push    FILE_ATTRIBUTE_NORMAL
    .if [overwriteIfExists] == TRUE
        push    CREATE_ALWAYS
    .else
        push    CREATE_NEW
    .endif
    push    0
    push    0
    push    GENERIC_READ or GENERIC_WRITE
    push    [lpDestFile]
    call    CreateFile
    cmp     eax, -1
    jz      @@FILE_CREATE_ERROR
    mov     [hFile], eax

    push    0
    lea     eax, [numb]
    push    eax
    mov     eax, [recvCount]
    .if [FileSize] >= eax
        push    [FirstPartSize]
    .else
        push    [FileSize]
    .endif
    push    [pFile]
    push    [hFile]
    call    WriteFile
    ;invoke WriteFile, [hFile], [pFile], XXX, ADDR numb, 0

    ; Check if file bigger than 1 MiB
    mov     eax, [FirstPartSize]
    sub     [FileSize], eax
    cmp     [FileSize], 0
    jle     @@DONE

    .while [FileSize] != 0
        xor     eax, eax
        mov     ecx, (ONE_MEGABYTE/4)
        mov     edi, [recvData]
        cld
        rep     stosd
        invoke  recv, [hSock], [recvData], ONE_MEGABYTE, 0
        .if eax == -1
            invoke  WSAGetLastError
            cmp     eax, WSAEWOULDBLOCK
            jnz     @@CONN_ERROR
            invoke  Sleep, 10
        .else
            push    eax
            lea     edx, [numb]
            invoke  WriteFile, [hFile], [recvData], eax, edx, 0
            pop     eax
        .endif
        sub     [FileSize], eax
        .if [FileSize] == 0
            .break
        .endif
    .endw

  @@DONE:
    invoke  CloseHandle, [hFile]
    invoke  GlobalFree, [recvData]
    jmp     @@CLOSE_SOCKET

  @@ERROR:
    mov     eax, -1
    jmp     @@RET
  @@CLOSE_SOCKET:
    invoke  closesocket, [hSock]
    xor     eax, eax
    jmp     @@RET
  @@MEM_ERROR:
    mov     eax, 1
    jmp     @@RET
  @@CONN_ERROR:
    mov     eax, 2
    jmp     @@RET
  @@FILE_NOT_FOUND_ERROR:
    mov     eax, 3
    jmp     @@RET
  @@FILE_CREATE_ERROR:
    mov     eax, 4
  @@RET:
    ret
DownloadFile endp

; custom send procedure, automatically checks for errors and socket closures
ecSend proc hSock:DWORD, pData:DWORD, lData:DWORD, pConnInfo:DWORD
  @@SEND_TRY_AGAIN:
    ; check if we should check that the connection should be closed
    .if [pConnInfo] != 0
        mov     eax, [pConnInfo]
        assume eax:PTR ConnInfo
        cmp     [eax].fd_close, TRUE
        assume eax:NOTHING
        jz      @@ERROR ; when returning an error the caller will exit the connection
    .endif
    invoke  send, [hSock], [pData], [lData], 0
    cmp     eax, -1
    jnz     @@DONE
    invoke  WSAGetLastError
    cmp     eax, WSAEWOULDBLOCK
    jnz     @@ERROR
    invoke  Sleep, 1 ; Make sure this is not a loop which gets the CPU usage to 100%.
                     ; This can happen for example when the server is used locally. Otherwise,
                     ; when the CPU usage is 100%, the other threads won't get much CPU time.
    jmp     @@SEND_TRY_AGAIN
  @@ERROR:
    or      eax, -1
  @@DONE:
    ret
ecSend endp
