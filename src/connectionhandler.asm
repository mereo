; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

; Closes an incoming connection
CloseConnectionThread proc hSocket:DWORD
LOCAL temp_buffer[4096]        :BYTE
LOCAL li                       :linger

    ; Set linger option
    mov     [li.l_onoff], TRUE
    mov     [li.l_linger], 1
    invoke  setsockopt, [hSocket], SOL_SOCKET, SO_LINGER, ADDR li, SIZEOF linger

    ; Create an HTTP status 403 message
    invoke  ecSend, [hSocket], ADDR http_msg403_close, SIZEOF http_msg403_close-1, 0

    ; Close the socket
    invoke  closesocket, [hSocket]

    ; Decrease thread count
    dec     [gThreadCount]

    ; Exit the thread
    invoke  ExitThread, 0
CloseConnectionThread endp

; Handles an incoming connection
HandleConnectionThread proc uses ebx esi edi pConnInfo:DWORD
LOCAL temp1_buffer[6144]        :BYTE
LOCAL temp2_buffer[6144]        :BYTE
LOCAL decoded_filename[PATH_BUFFER_WIDE_LEN]:BYTE
LOCAL content_type_buffer[256]  :BYTE
LOCAL datetime_buffer[32]       :BYTE
LOCAL last_mod_buffer[32]       :BYTE
LOCAL FileSizeTotal[32]         :BYTE
LOCAL Resume_Temp1[32]          :BYTE
LOCAL Resume_Temp2[32]          :BYTE
LOCAL Resume_Temp3[32]          :BYTE
LOCAL Resume_String[32]         :BYTE
LOCAL End_String[32]            :BYTE
LOCAL Resume_BCD                :TBYTE
LOCAL FileSizeLow               :DWORD
LOCAL FileSizeHigh              :DWORD
LOCAL FileSizeReal              :REAL8
LOCAL TempFileSize              :DWORD
LOCAL Resume_Offset             :TBYTE
LOCAL End_Offset                :TBYTE
LOCAL hFile                     :DWORD
LOCAL lpMem                     :DWORD
LOCAL hEventStart               :DWORD
LOCAL HeadReq                   :DWORD
LOCAL hSock                     :DWORD
LOCAL pContentBuffer            :DWORD
LOCAL pTotalBuffer              :DWORD
LOCAL keep_alive                :DWORD
LOCAL canresume                 :DWORD
LOCAL temp                      :DWORD
LOCAL TempRsrcSize              :DWORD
LOCAL numb                      :DWORD

    mov     ebx, [pConnInfo]
    assume ebx:PTR ConnInfo
    mov     [ebx].ThreadRunning, TRUE
    mov     eax, [ebx].hSock
    mov     [hSock], eax
    mov     eax, [ebx].hEventStart
    mov     [hEventStart], eax
    assume ebx:NOTHING

  @@BEGIN_THREAD:
    mov     [canresume], FALSE
    mov     eax, [pConnInfo]
    assume eax:PTR ConnInfo
    mov     [eax].WaitingStatus, TRUE
    cmp     [eax].fd_close, TRUE
    jz      @@CLOSE_SOCKET
    assume eax:NOTHING
    invoke  WaitForSingleObject, [hEventStart], 4000 ; 4 sec, should be enough
    ; Most browsers do not even close the socket so we have to watch out for
    ; this by ourselves
    cmp     eax, WAIT_TIMEOUT ; Same as STATUS_TIMEOUT
    jz      @@SET_THREAD_RUNNING_FALSE_AND_EXIT
    mov     eax, [pConnInfo]
    assume eax:PTR ConnInfo
    mov     [eax].WaitingStatus, FALSE
    assume eax:NOTHING
    invoke  ClearBuffer, ADDR temp1_buffer, 4096

  @@:
    invoke  recv, [hSock], ADDR temp1_buffer, 4094, 0 ; preserve one NULL byte and preserve '/' byte for redirecting
    cmp     eax, -1
    jnz     @F
    invoke  WSAGetLastError
    cmp     eax, WSAEWOULDBLOCK
    jnz     @@CLOSE_SOCKET
    ; Otherwise, sleep (to not stress CPU too much) and try again
    ; The 'select' API could be used too but this way it consumes very
    ; little CPU time too :)
    invoke  Sleep, 10
    jmp     @B

  @@:
    mov     eax, [pConnInfo]
    assume eax:PTR ConnInfo
    invoke  ResetEvent, [eax].hEventStart
    assume eax:NOTHING
    invoke  GetRequestEnd, ADDR temp1_buffer
    invoke  StrCpy, ADDR temp2_buffer, ADDR temp1_buffer
    mov     [HeadReq], TRUE
    invoke  CheckIfHEADRequest, ADDR temp1_buffer
    test    eax, eax
    jz      @F
    mov     [HeadReq], FALSE

    @@:
    invoke  FindHostOfRequest, ADDR temp1_buffer
    test    eax, eax
    jz      @@STATUS400
    invoke  CheckForResume, ADDR temp1_buffer, ADDR Resume_Offset, ADDR End_Offset, ADDR Resume_String, ADDR End_String, ADDR canresume ; Will set [canresume] accordingly and the file offset of where to begin sending
    invoke  StrCpy, ADDR temp1_buffer, ADDR temp2_buffer
    invoke  CheckForConnectionStatus, ADDR temp1_buffer
    mov     [keep_alive], -1
    cmp     eax, -1
    jz      @F
    cmp     eax, 1
    jz      @F
    mov     [keep_alive], TRUE

    @@:
    invoke  StrCpy, ADDR temp1_buffer, ADDR temp2_buffer
    invoke  ClearBuffer, ADDR temp2_buffer, 4096
    invoke  ParseRequestPath, ADDR temp1_buffer, ADDR temp2_buffer
    cmp     eax, -1
    jz      @@STATUS400
    .if [HeadReq] == FALSE
        invoke  StrCmp, ADDR temp2_buffer, ADDR http_get
        test    eax, eax
        jnz     @@STATUS501
    .endif
    cmp     byte ptr [temp1_buffer+4], 0
    jz      @@STATUS400
    ; Relative path to resource
    invoke  StrLen, ADDR temp1_buffer+4
    mov     ecx, eax
    mov     eax, CR
    lea     edi, [temp1_buffer+4]
    cld
    repnz   scasb
    jnz     @@STATUS400
    mov     eax, ' '
    mov     ecx, edi
    lea     edx, [temp1_buffer+4]
    sub     ecx, edx
    sub     edi, 2
    std
    repnz   scasb
    cld
    jnz     @@STATUS400
    inc     edi
    mov     cl, byte ptr [edi]
    mov     byte ptr [edi], 0
    push    ecx
    invoke  StrCpy, ADDR temp2_buffer, ADDR temp1_buffer+4
    pop     ecx
    mov     byte ptr [edi], cl
    cmp     eax, -1
    jz      @@STATUS400
    invoke  HexStringToString, ADDR temp2_buffer, ADDR temp2_buffer
    invoke  StrCutSpaces, ADDR temp2_buffer
    invoke  ParsePath, ADDR temp2_buffer
    invoke  FilterDoubleSlash, ADDR temp2_buffer
    invoke  StrCpy, ADDR temp1_buffer, ADDR temp2_buffer ; Copy parsed path back to the other buffer
    invoke  CheckForDDots, ADDR temp2_buffer
    cmp     eax, 1
    jz      @@STATUS404
    invoke  ParseAbsURI, ADDR temp2_buffer
    mov     al, byte ptr [temp2_buffer + (SIZEOF icons_path - 1)]
    push    eax
    mov     byte ptr [temp2_buffer + (SIZEOF icons_path - 1)], 0
    invoke  StrCmpI, ADDR temp2_buffer, ADDR icons_path
    pop     ecx
    mov     byte ptr [temp2_buffer + (SIZEOF icons_path - 1)], cl
    .if eax == 0
        invoke  StrCmpI, ADDR temp2_buffer + (SIZEOF icons_path - 1), ADDR icons_dir
        push    eax
        invoke  StrCmpI, ADDR temp2_buffer + (SIZEOF icons_path - 1), ADDR icons_file
        pop     ecx
        .if eax == 0 ; File icon
            ; Client requests an Mereo icon for the Directory Browsing feature...
            invoke  LoadRsrc, [hInstance], ID_IMG_HTTP_DB_FILE, RT_RCDATA, ADDR TempRsrcSize
        .elseif ecx == 0 ; Directory icon
            ; Client requests an Mereo icon for the Directory Browsing feature...
            invoke  LoadRsrc, [hInstance], ID_IMG_HTTP_DB_DIR, RT_RCDATA, ADDR TempRsrcSize
        .else
            jmp     @@STATUS404
        .endif
        push    eax ; Save memory pointer
        invoke  WriteTempFile, ADDR temp1_buffer, 4096, eax, [TempRsrcSize]
        mov     [hFile], eax
        pop     ecx ; Restore memory pointer
        push    eax ; Save WriteTempFile's return value
        invoke  GlobalFree, ecx ; Free memory
        pop     eax ; Restore WriteTempFile's return value
        .if eax == INVALID_HANDLE_VALUE
            jmp     @@STATUS500
        .endif
        ; Load the correct MIME type
        invoke  FindMIME, ADDR icons_dir, ADDR content_type_buffer, SIZEOF content_type_buffer
        mov     eax, [hFile]
        jmp     @@SEND_HTTPSV_DB_IMG
    .endif
    invoke  StrLen, ADDR temp2_buffer
    lea     edi, [temp2_buffer]
    add     edi, eax
    dec     edi
    .if byte ptr [edi] == "\" ; Client requested a directory
        ; Directory

        ; check if 'Directory Browsing' is enabled
        invoke  SendMessage, [hDirBrowsing], BM_GETCHECK, 0, 0
        cmp     eax, BST_CHECKED
        mov     ebx, 0
        setz    bl
        ; check if 'Show contents of directories even if index.htm(l) exists.' is enabled
        .if bl && [prefs_dirbrowse_force]
            jmp @@BROWSE_DIR
        .endif

        invoke  StrCpy, ADDR temp1_buffer, ADDR host_path
        invoke  StrCat, ADDR temp1_buffer, ADDR temp2_buffer
        invoke  StrCat, ADDR temp1_buffer, ADDR file_index_html
        invoke  FilterDoubleSlash, ADDR temp1_buffer
        invoke  CheckForDDots, ADDR temp1_buffer
        cmp     eax, 1 ; means '../' is in the URI, send 404 (File Not Found) response
        jz      @@STATUS404
        invoke  GetFileAttributes, ADDR temp1_buffer
        cmp     eax, -1
        jnz     @@SEND_FILE
        ; we still have 'index.html', cut off the 'l' to get 'index.htm'
        invoke  StrLen, ADDR temp1_buffer
        mov     byte ptr [temp1_buffer+eax-1], 0
        ; CheckForDDots proc not needed now, because in the requested path no
        ; '../' was found before
        invoke  GetFileAttributes, ADDR temp1_buffer
        cmp     eax, -1
        jnz     @@SEND_FILE
        ; if 'Directory Browsing' is enabled, show the contents of the directory
        or      ebx, ebx ; dirbrowse setting
        jnz     @@BROWSE_DIR
        jmp     @@STATUS404

      @@BROWSE_DIR:
        invoke  StrCpy, ADDR temp1_buffer, ADDR host_path
        invoke  StrCat, ADDR temp1_buffer, ADDR temp2_buffer
        invoke  GetFileAttributes, ADDR temp1_buffer
        cmp     eax, -1
        jz      @@STATUS404
        invoke  StrLen, ADDR temp1_buffer
        lea     edi, [temp1_buffer]
        add     edi, eax
        dec     edi
        .if byte ptr [edi] == '\'
            mov     byte ptr [edi], 0
        .endif
        invoke  ConvertToSlashes, ADDR temp2_buffer
        ; now send directory contents
        invoke  GlobalAlloc, GPTR, ONE_MEGABYTE ; should do the job
        test    eax, eax
        jz      @@STATUS503
        mov     [pTotalBuffer], eax
        invoke  StrLen, ADDR temp1_buffer
        inc     eax
        lea     eax, [eax+01000h] ; GlobalFree sometimes won't free the memory block when the block is smaller than 4kB
        invoke  GlobalAlloc, GPTR, eax
        test    eax, eax
        jz      @@FREE_AND_STATUS503
        mov     [pContentBuffer], eax
        invoke  StrCpy, [pContentBuffer], ADDR temp1_buffer
        invoke  StrCat, [pContentBuffer], ADDR dir_browse_all
        invoke  StrCpy, [pTotalBuffer], ADDR dir_browse_page_part1_1
        invoke  StrCat, [pTotalBuffer], ADDR str_charset
        invoke  StrCat, [pTotalBuffer], ADDR dir_browse_page_part1_2
        invoke  StrCat, [pTotalBuffer], ADDR temp2_buffer
        invoke  StrCat, [pTotalBuffer], ADDR dir_browse_page_part2

        invoke  DirBrowseInsertHtmlBackLinks, ADDR temp2_buffer
        .if eax != 0
            push    eax
            invoke  StrCat, [pTotalBuffer], eax
            pop     eax
            invoke  GlobalFree, eax
        .else
            invoke  StrCat, [pTotalBuffer], ADDR temp2_buffer
        .endif

        invoke  StrCat, [pTotalBuffer], ADDR dir_browse_page_part3
        invoke  FilterDoubleSlash, [pContentBuffer]
        invoke  Utf8ToWide, [pContentBuffer], ADDR decoded_filename, (SIZEOF decoded_filename/2)
        invoke  MakeContents, CONTENT_DIRECTORIES, ADDR decoded_filename, [pTotalBuffer]
        .if eax != -1
            invoke  StrCat, [pTotalBuffer], ADDR dir_browse_page_part4
        .endif
        invoke  MakeContents, CONTENT_FILES, ADDR decoded_filename, [pTotalBuffer]
        invoke  StrCat, [pTotalBuffer], ADDR dir_browse_page_part5
        invoke  GlobalFree, [pContentBuffer]
        invoke  ClearBuffer, ADDR temp1_buffer, 4096
        invoke  StrLen, [pTotalBuffer]
        invoke  wsprintf, ADDR temp1_buffer, ADDR http_msg200_dec, ADDR type_html, eax
        invoke  ecSend, [hSock], ADDR temp1_buffer, eax, [pConnInfo]
        cmp     eax, -1
        jz      @@CLOSE_SOCKET
        cmp     [HeadReq], TRUE
        jz      @@DONE_SENDING
        invoke  StrLen, [pTotalBuffer]
        invoke  ecSend, [hSock], [pTotalBuffer], eax, [pConnInfo]
        cmp     eax, -1
        jz      @@CLOSE_SOCKET
        invoke  GlobalFree, [pTotalBuffer]
        jmp     @@DONE_SENDING
    .else
        ; File
        invoke  StrCpy, ADDR temp1_buffer, ADDR host_path
        invoke  StrCat, ADDR temp1_buffer, ADDR temp2_buffer
        ; Check if the file exists
        invoke  Utf8ToWide, ADDR temp1_buffer, ADDR decoded_filename, (SIZEOF decoded_filename/2)
        invoke  GetFileAttributesW, ADDR decoded_filename
        cmp     eax, -1
        jz      @@STATUS404
        and     eax, 10h
        jz      @@SEND_FILE

        ; Check if we should redirect or not
        invoke  SendMessage, [hDirBrowsing], BM_GETCHECK, 0, 0
        cmp     eax, BST_CHECKED
        jz      @@BROWSE_DIR
        jmp     @@STATUS404

      @@SEND_FILE:
        invoke  FindMIME, ADDR temp1_buffer, ADDR content_type_buffer, SIZEOF content_type_buffer
        ; We can't be 100% sure if the filename has already been converted to a
        ; wide character string, so let's make sure it is.
        invoke  Utf8ToWide, ADDR temp1_buffer, ADDR decoded_filename, (SIZEOF decoded_filename/2)
        invoke  CreateFileW, ADDR decoded_filename, GENERIC_READ, \
                             FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0
        cmp     eax, -1
        jz      @@STATUS404

      @@SEND_HTTPSV_DB_IMG:
        ; Store file handle
        mov     [hFile], eax

        ; Get file size (+ support file size over max possible 32-bits CPU register size value)
        invoke  GetFileSize, [hFile], ADDR FileSizeHigh
        mov     [FileSizeLow], eax
        ; Had to use [FileSizeLow] instead of EAX because EAX is overwritten by the INVOKE macro
        invoke  dqtoa, [FileSizeLow], [FileSizeHigh], ADDR FileSizeTotal, ADDR FileSizeReal
        invoke  StrCpy, ADDR FileSizeTotal, ADDR FileSizeTotal+1

        ; Get file time & convert to HTTP header
        invoke  GetFileTime, [hFile], 0, 0, ADDR last_write_time
        test    eax, eax
        jz      @@CHECK_RESUME ; no file time could be retrieved, now check if we can resume
        invoke  FileTimeToSystemTime, ADDR last_write_time, ADDR system_time
        test    eax, eax
        jz      @@CHECK_RESUME ; time could not be converted, now check if we can resume
        invoke  GetDateFormat, LOCALE_SYSTEM_DEFAULT, 0, ADDR system_time, ADDR date_format, ADDR datetime_buffer, 32
        test    eax, eax
        jz      @@CHECK_RESUME ; string could not be formatted, now check if we can resume
        invoke  StrCpy, ADDR last_mod_buffer, ADDR datetime_buffer
        invoke  GetTimeFormat, LOCALE_SYSTEM_DEFAULT, TIME_FORCE24HOURFORMAT, ADDR system_time, ADDR time_format, ADDR datetime_buffer, 32
        test    eax, eax
        jz      @@CHECK_RESUME ; string could not be formatted, now check if we can resume
        invoke  StrLen, ADDR last_mod_buffer
        lea     edx, [last_mod_buffer]
        add     eax, edx
        mov     byte ptr [eax], 20h
        mov     byte ptr [eax+1], 0
        invoke  StrCat, ADDR last_mod_buffer, ADDR datetime_buffer
        invoke  StrCat, ADDR last_mod_buffer, ADDR time_gmt

        ; Finally we have the full HTTP standard formatted time for the Last-Modified header
      @@CHECK_RESUME:
        cmp     [canresume], TRUE
        jnz     @@STATUS200 ; status 200

        ; 1st:  Resume_Offset (Decimal converted string of REAL4)
        ;   -> Resume_String
        ; 2nd:  FileSizeTotal-1 (Decimal converted string of FileSizeReal-1)
        ;   -> Resume_Temp1
        ; 3rd:  FileSizeTotal (Decimal converted string of FileSizeReal)
        ;   -> Resume_Temp2
        ; 4th:  FileSizeTotal (Decimal converted string of FileSizeReal)
        ;   -> Resume_Temp2
        ; IMPORTANT: FileSizeReal = FileSizeReal - Resume_Offset

        ; Content-Range: bytes FpuFLtoA(Resume_Offset)-FpuFLtoA(FileSizeReal-1)/FpuFLtoA(FileSizeReal)
        ; Content-Length: FpuFLtoA(FileSizeReal-Resume_Offset)

        mov     [temp], 1
        fld     qword ptr [FileSizeReal]
        fild    [temp]
        fsubp   st(1), st(0)
        invoke  FpuFLtoA, 0, 0, ADDR Resume_Temp1, SRC1_FPU
        ffree   st(0)

        fld     qword ptr [FileSizeReal]
        invoke  FpuFLtoA, 0, 0, ADDR Resume_Temp2, SRC1_FPU
        ffree   st(0)

        fld     qword ptr [FileSizeReal]
        fld     tbyte ptr [Resume_Offset]
        fabs
        fsubp   st(1), st(0)
        fst     qword ptr [FileSizeReal]
        invoke  FpuFLtoA, 0, 0, ADDR Resume_Temp3, SRC1_FPU
        ffree   st(0)

        ; Send HTTP message 206 (Partial Content)
        invoke  wsprintf, ADDR temp1_buffer, ADDR http_msg206, ADDR last_mod_buffer, ADDR content_type_buffer, ADDR Resume_String, ADDR Resume_Temp1+1, ADDR Resume_Temp2+1, ADDR Resume_Temp3+1

        fld     [Resume_Offset]
        fld     [max_32_bits]
        ; St 0 = 1 0000 0000
        ; St 1 = Resume Offset
        fcomp   st(1)
        fstsw   ax
        test    ax, 256
        jz      @@BELOW ; below '1 0000 0000' if it jumps, so we can use offset without problems for SetFilePointer

        ; Calculate FileOffsetLow (low DWORD of hexadecimal number) and FileOffsetHigh (high DWORD of hexadecimal number)
        ; Formula:  FileOffsetLow = Lower DWORD of Resume_Offset
        ;       FileOffsetHigh = Higher DWORD of Resume_Offset

        mov     [FileSizeHigh], 0
      @@SUB_LOOP:
        inc     [FileSizeHigh]

        fld     [max_32_bits_plus1]

        ; FileSizeReal - 2^32 = FileOffsetLow
        fsubp   st(1), st(0)

        fld     [max_32_bits]
        fcomp   st(1)
        fstsw   ax
        test    ax, 256
        jnz     @@SUB_LOOP

        invoke  FpuFLtoA, 0, 0, ADDR Resume_Temp1, SRC1_FPU
        ffree   st(0)

        invoke  StrCpy, ADDR Resume_Temp2, ADDR Resume_Temp1
        invoke  atodw, ADDR Resume_Temp2+1
        ; EAX = FileSizeLow
        mov     [FileSizeLow], eax

        invoke  SetFilePointerLong, [FileSizeLow], [FileSizeHigh], [hFile]
        jmp     @@BEGIN_SENDING

      @@BELOW:
        invoke  FpuFLtoA, 0, 0, ADDR Resume_Temp1, SRC1_FPU
        ffree   st(0)
        invoke  atodw, ADDR Resume_Temp1+1
        invoke  SetFilePointerLong, eax, 0, [hFile]
        jmp     @@BEGIN_SENDING

      @@STATUS200:
        ; Send HTTP message 200 (OK)
        invoke  wsprintf, ADDR temp1_buffer, ADDR http_msg200, ADDR last_mod_buffer, ADDR content_type_buffer, ADDR FileSizeTotal
        invoke  SetFilePointer, [hFile], 0, 0, FILE_BEGIN

      @@BEGIN_SENDING:
        invoke  StrLen, ADDR temp1_buffer
        invoke  ecSend, [hSock], ADDR temp1_buffer, eax, [pConnInfo]
        cmp     eax, -1
        jz      @@CLOSE_SOCKET
        cmp     [HeadReq], TRUE
        jz      @@DONE_SENDING

        invoke  GlobalAlloc, GPTR, SOCK_STREAM_SEND_SIZE
        mov     [lpMem], eax

      @@SEND_NEXT_PART:
        push    0
        lea     eax, [numb]
        push    eax

        ; Compare FileSizeReal with SOCK_STREAM_SEND_SIZE
        ; IF >= SOCK_STREAM_SEND_SIZE then PUSH SOCK_STREAM_SEND_SIZE
        ; ELSE PUSH (dword converted last part of REAL)
        fld     qword ptr [FileSizeReal]
        mov     [temp], SOCK_STREAM_SEND_SIZE
        fild    [temp]
        ; St 0 = ONE_MEG
        ; St 1 = FileSizeReal
        fcomp   st(1)
        fstsw   ax
        ffree   st(0)
        test    ax, 256
        jz      @F      ; Below one megabyte if jumps
        mov     [TempFileSize], SOCK_STREAM_SEND_SIZE
        push    SOCK_STREAM_SEND_SIZE
        jmp     @@FILESIZE_PUSH_DONE
      @@:
        fld     qword ptr [FileSizeReal]
        invoke  FpuFLtoA, 0, 0, ADDR Resume_Temp1, SRC1_FPU
        invoke  atodw, ADDR Resume_Temp1+1
        ffree   st(0)
        mov     [TempFileSize], eax
        push    eax

        ; Now send... (no need to update file pointer as this is updated automatically)
      @@FILESIZE_PUSH_DONE:
        mov     ebx, 60 ; Retry counter (in the case it fails)
      @@SEND_RETRY:
        push    [lpMem]
        push    [hFile]
        call    [ReadFile]
        test    eax, eax
        jnz     @@SEND_OK       ; Everything fine
        ; ERROR :(
        ; Nothing we can do about a read failure than just retry a few times.
        ; The computer is probably out of memory, maximum retries is 60.
        ; 60 (retries) * 500 (ms) = 30000 (ms), we keep retrying for at least 30 seconds
        ; to hope some applications freed memory. We don't have to update the file
        ; pointer because it won't increase automatically if ReadFile fails.
        dec     ebx
        jz      @@ALERT_AND_CLOSE_FILE_AND_SOCKET   ; Aww, time up... this thread is not going
                                                    ; to wait forever, let's close it and log
                                                    ; an alert entry in the log dialog.
        invoke  Sleep, 500      ; Wait 500 ms
        push    0
        lea     eax, [numb]
        push    eax
        push    [TempFileSize]
        jmp     @@SEND_RETRY    ; Retry

      @@SEND_OK:
        push    [pConnInfo]
        push    [TempFileSize]
        push    [lpMem]
        push    [hSock]
        call    ecSend
        cmp     eax, -1
        jnz     @@GO_ON_SENDING_FILE
        jmp     @@CLOSE_FILE_AND_SOCKET

      @@ALERT_AND_CLOSE_FILE_AND_SOCKET:
        invoke  SendMessage, [ghwndDlg], WM_LOG_READ_ERROR, 0, 0
        ; jmp    @@CLOSE_FILE_AND_SOCKET

      @@CLOSE_FILE_AND_SOCKET:
        invoke  CloseHandle, [hFile]
        invoke  GlobalFree, [lpMem]
        jmp     @@CLOSE_SOCKET

      @@GO_ON_SENDING_FILE:
        ; IF [FileSizeReal] >= SOCK_STREAM_SEND_SIZE
        ;   Sub SOCK_STREAM_SEND_SIZE of [FileSizeReal]
        ;   JMP @@SEND_NEXT_PART
        fld     qword ptr [FileSizeReal]
        mov     [temp], SOCK_STREAM_SEND_SIZE
        fild    [temp]
        ; St 0 = ONE_MEG
        ; St 1 = FileSizeReal
        fcom    st(1)
        fstsw   ax
        test    ax, 256
        jz      @F
        fsubp   st(1), st(0)
        fst     qword ptr [FileSizeReal]
        ffree   st(0)
        jmp     @@SEND_NEXT_PART
      @@:
        ffree   st(0)
        ffree   st(1)
        invoke  CloseHandle, [hFile]
        invoke  GlobalFree, [lpMem]
        jmp     @@DONE_SENDING
    .endif

  @@STATUS301:
    ; File was actually a directory, redirect
    invoke  StrLen, ADDR temp2_buffer
    lea     edi, [temp2_buffer]
    add     edi, eax
    mov     byte ptr [edi], '/'
    mov     byte ptr [edi+1], 0
    invoke  wsprintf, ADDR temp1_buffer, ADDR http_msg301, ADDR temp2_buffer
    jmp     @@SEND_STATUS_MSG

    ; Error pages
  @@STATUS400:
    invoke  LoadErrorPage, ADDR temp1_buffer, SIZEOF temp1_buffer, ADDR http_msg400, 400, ADDR http_msg400_page
    jmp     @@SEND_STATUS_MSG

  @@STATUS403:
    invoke  LoadErrorPage, ADDR temp1_buffer, SIZEOF temp1_buffer, ADDR http_msg403, 403, ADDR http_msg403_page
    jmp     @@SEND_STATUS_MSG

  @@STATUS404:
    invoke  LoadErrorPage, ADDR temp1_buffer, SIZEOF temp1_buffer, ADDR http_msg404, 404, ADDR http_msg404_page
    jmp     @@SEND_STATUS_MSG

  @@STATUS500:
    invoke  LoadErrorPage, ADDR temp1_buffer, SIZEOF temp1_buffer, ADDR http_msg500, 500, ADDR http_msg500_page
    jmp     @@SEND_STATUS_MSG

  @@STATUS501:
    invoke  LoadErrorPage, ADDR temp1_buffer, SIZEOF temp1_buffer, ADDR http_msg501, 501, ADDR http_msg501_page
    jmp     @@SEND_STATUS_MSG

  @@STATUS503:
    ; This message won't be customizable because it's a message send when the server is
    ; very busy, so it would be inefficient to load a page from the harddisk.
    invoke  wsprintf, ADDR temp1_buffer, ADDR http_msg503, [http_msg503_size]
    jmp     @@SEND_STATUS_MSG

  @@SEND_STATUS_MSG:
    invoke  StrLen, ADDR temp1_buffer
    invoke  ecSend, [hSock], ADDR temp1_buffer, eax, [pConnInfo]
    cmp     eax, -1
    jz      @@CLOSE_SOCKET
    jmp     @@DONE_SENDING

  @@FREE_AND_STATUS503:
    invoke  GlobalFree, [pTotalBuffer]
    jmp     @@STATUS503

  @@DONE_SENDING:
    cmp     [keep_alive], TRUE
    jnz     @@CLOSE_SOCKET
    mov     eax, [pConnInfo]
    assume eax:PTR ConnInfo
    cmp     [eax].RecvData, TRUE
    jz      @@SET_RECVDATA_FALSE_AND_CONTINUE_THREAD
    assume eax:NOTHING
    jmp     @@CLOSE_SOCKET

  @@SET_THREAD_RUNNING_FALSE_AND_EXIT:
    mov     eax, [pConnInfo]
    assume eax:PTR ConnInfo
    cmp     [eax].RecvData, TRUE
    jz      @@SET_RECVDATA_FALSE_AND_CONTINUE_THREAD
    mov     [eax].ThreadRunning, FALSE
    assume eax:NOTHING
    invoke  WSAAsyncSelect, [hSock], [ghwndDlg], WM_SOCKET, FD_READ or FD_CLOSE
    jmp     @@EXITTHREAD

  @@SET_RECVDATA_FALSE_AND_CONTINUE_THREAD:
    assume eax:PTR ConnInfo
    mov     [eax].RecvData, FALSE
    assume eax:NOTHING
    jmp     @@BEGIN_THREAD

  @@CLOSE_SOCKET:
    invoke  RemoveStruct, [pConnInfo]
    invoke  closesocket, [hSock]

  @@EXITTHREAD:
    ; Decrease thread count
    dec     [gThreadCount]
    ; Exit the thread
    invoke  ExitThread, 0
HandleConnectionThread endp

ParsePath proc SRC:DWORD
    ; Takes argument 'SRC' as OFFSET to the string buffer
    invoke  StrLen, [SRC]
    mov     edx, [SRC]
    mov     ecx, eax
    test    ecx, ecx
    jnz     @F
    mov     eax, -1
    ret

    @@:
    mov     al, byte ptr [edx]
    .if al == '/'
        mov     byte ptr [edx], 05Ch ; '\'
    .endif
    inc     edx
    sub     ecx, 1
    jnz     @B
    ret
ParsePath endp

ParseRequestPath proc uses esi edi SRC:DWORD,
                                   DST:DWORD
    ; Returns 1 if everything went fine, returns -1 if 20h character not found (20h = space)
    mov     edi, [SRC]

  @@LOOP_CRLF_CUTOUT:
    cmp     byte ptr [edi], CR
    jz      @@CUT_CRLF
    cmp     byte ptr [edi], LF
    jz      @@CUT_CRLF
    jmp     @@CRLF_ARE_CUTOUT

  @@CUT_CRLF:
    inc     edi
    jmp     @@LOOP_CRLF_CUTOUT

  @@CRLF_ARE_CUTOUT:
    ; EDI is now source to begin of request, leading CR LF MUST be ignored
    xor     ecx, ecx

  @@SCASB_LOOP:
    cmp     byte ptr [edi], 20h
    jz      @F
    inc     ecx
    inc     edi
    cmp     ecx, 4096
    jz      @@NO_SPACE_FOUND_IN_REQUEST
    jmp     @@SCASB_LOOP

  @@NO_SPACE_FOUND_IN_REQUEST:
    or      eax, -1
    ret

  @@:
    mov     esi, [SRC]
    mov     edi, [DST]
    cld
    rep     movsb
    mov     byte ptr [edi], 0
    xor     eax, eax
    inc     eax
    ret
ParseRequestPath endp

; Takes a path to a filename as argument, and put's the type of file in content_type_buffer
; Returns 1 if everything went fine, returns -1 if content type not found (and loads default content type)
FindMIME proc SRC:DWORD, DST:DWORD, CNT:DWORD
    invoke  StrLen, [SRC]
    mov     ecx, eax
    mov     edi, [SRC]
    add     edi, ecx
    dec     edi
    mov     al, "."
    std
    repnz   scasb
    jnz     @@CONTENT_TYPE_ERROR
    cld
    inc     edi
    invoke  RegOpenKeyEx, HKEY_CLASSES_ROOT, edi, 0, KEY_READ, ADDR hReg
    test    eax, eax
    jnz     @@CONTENT_TYPE_ERROR
    mov     eax, [CNT]
    mov     [buffer_size], eax
    invoke  RegQueryValueEx, [hReg], ADDR content_type, 0, 0, [DST], ADDR buffer_size
    test    eax, eax
    jz      @F
    invoke  RegCloseKey, [hReg]

    @@CONTENT_TYPE_ERROR:
    cld
    invoke  ClearBuffer, [DST], 128
    invoke  StrCpy, [DST], ADDR type_text
    or      eax, -1
    ret

    @@:
    invoke  RegCloseKey, [hReg]
    xor     eax, eax
    inc     eax
    ret
FindMIME endp

FilterDoubleSlash proc uses edi SRCDST:DWORD
    mov     edi, [SRCDST]

    @@:
    cmp     byte ptr [edi], 0
    jz      @F
    cmp     byte ptr [edi], '\'
    add     edi, 1 ;add does not affect Z flag
    jnz     @B
    mov     byte ptr [edi-1], '/'
    jmp     @B

    @@:
    ;all '\' are now '/'
    ;now filter '//' to '/'
    invoke  StrLen, [SRCDST]
    mov     ecx, eax
    mov     eax, '/'
    mov     edi, [SRCDST]
    cld

    @@:
    repnz   scasb
    jnz     @F
    cmp     byte ptr [edi], '/'
    jnz     @B
    pushad
    push    edi
    dec     edi
    invoke  StrLen, edi
    pop     edi
    mov     esi, edi
    dec     edi
    mov     ecx, eax
    cld
    rep     movsb
    popad
    dec     edi
    jmp     @B

    @@:
    ret
FilterDoubleSlash endp

CheckForDDots proc uses edi SRC:DWORD
; Returns 0 in EAX if no '../' found, returns 1 if '../' present (or ..\)
    invoke  StrLen, [SRC]
    mov     ecx, eax
    mov     edi, [SRC]
    mov     eax, '.'
    cld

    @@:
    repnz   scasb
    jnz     @F
    .if (byte ptr [edi] == '.') || (byte ptr [edi+1] == '/') || (byte ptr [edi+1] == '\')
        xor     eax, eax
        inc     eax
        ret
    .else
        jmp     @B
    .endif

    @@:
    xor     eax, eax
    ret
CheckForDDots endp

ParseAbsURI proc uses ebx esi edi SRC:DWORD
    mov     eax, [SRC]
    cmp     byte ptr [eax], 'h'
    jnz     @@FALSE
    inc     eax
    cmp     byte ptr [eax], 't'
    jnz     @@FALSE
    inc     eax
    cmp     byte ptr [eax], 't'
    jnz     @@FALSE
    inc     eax
    cmp     byte ptr [eax], 'p'
    jnz     @@FALSE
    inc     eax
    cmp     byte ptr [eax], '/'
    jnz     @@FALSE
    inc     eax
    cmp     byte ptr [eax], '/'
    jnz     @@FALSE
    mov     edi, eax
    invoke  StrLen, edi
    mov     ecx, eax
    mov     al, '/'
    cld
    repnz   scasb
    jnz     @@FALSE
    mov     eax, edi
    dec     eax
    jmp     @@DONE

    @@FALSE:
    mov     eax, [SRC]

    @@DONE:
    ret
ParseAbsURI endp

StringToHexString proc uses ebx esi edi SRC:DWORD,
                                        DST:DWORD
    mov     esi, [SRC]
    mov     edi, [DST]

    @@:
    mov     al, byte ptr [esi]
    .if al == 0
        mov     byte ptr [edi], 0
        ret
    .elseif al > "~" || al < "("
        mov     bl, al
        mov     al, "%"
        stosb
        mov     al, bl
        shr     al, 4
        .if al < 10
            add     al, 30h
        .else
            add     al, 37h
        .endif
        stosb
        mov     al, bl
        and     al, 15 ; keep low nibble of AL register
        .if al < 10
            add     al, 30h
        .else
            add     al, 37h
        .endif
    .endif
    stosb
    inc     esi
    jmp     @B
StringToHexString endp

GetHexValue proc SRC:DWORD
    mov     eax, [SRC]
    cmp     al, "0"
    jb      @@RET_ERROR
    cmp     al, "9"
    ja      @F
    sub     al, 30h ;return raw value
    jmp     @@RET

  @@:
    ; First the uppercase check otherwise
    ; we couldn't say anything lower than 'A' would
    ; be wrong, because 'A' is lower than 'a', so 'A'
    ; is the lowest value after numeric characters.
    cmp     al, "A"
    jb      @@RET_ERROR
    cmp     al, "F"
    ja      @F
    sub     al, 37h ; return raw value
    jmp     @@RET

  @@:
    cmp     al, "a"
    jb      @@RET_ERROR
    cmp     al, "f"
    ja      @@RET_ERROR
    sub     al, 57h ; return raw value
    jmp     @@RET

  @@RET_ERROR:
    or      eax, -1

  @@RET:
    ret
GetHexValue endp

HexStringToString proc uses esi edi SRC:DWORD,
                                    DST:DWORD
    invoke  ParsePath, [SRC]
    mov     esi, [SRC]
    mov     edi, [DST]

    @@:
    mov     al, byte ptr [esi]
    .if al == 0
        mov     byte ptr [edi], 0
        ret
    .endif
    .if al == "%"
        invoke  GetHexValue, dword ptr [esi+1]
        .if al == -1
            mov     al, "%"
            stosb
            inc     esi
            jmp     @B
        .endif
        mov     bl, al
        invoke  GetHexValue, dword ptr [esi+2]
        .if al == -1
            mov     al, "%"
            stosb
            inc     esi
            jmp     @B
        .endif
        shl     bl, 4
        add     al, bl
        add     esi, 2
    .endif
    stosb
    inc     esi
    jmp     @B
HexStringToString endp

; Loads an HTML error page
;
; Parameters:
; lpszBuffer       = A pointer to the buffer in which to store the error page (which will be NULL terminated)
;                    This buffer MUST be at least 4096 bytes long.
; lpszHTTPMessage  = a pointer to an HTTP message to use as template
; iHTTPStatusCode  = (integer value) the HTTP status code (should match the one used for 'lpszHTTPMessage')
; lpszFallbackPage = a pointer to a fallback error page if no error page was found
;
; The 'lpszHTTPMessage' format should match the syntax required for 'wsprintf'.
;
; Return Values:
; On success, a value of TRUE is returned.
; On failure, the fallback error page is used (if it's length does not exceed
; the maximum of 4kB) and a value of FALSE or -1 is returned, FALSE is returned
; when the fallback page is used, -1 is returned when the fallback page is too
; large for the buffer.
LoadErrorPage proc lpszBuffer:DWORD, iBufferSize:DWORD, lpszHTTPMessage:DWORD, iHTTPStatusCode:DWORD, lpszFallbackPage:DWORD
LOCAL hFile:DWORD, ignore:DWORD, iFileSize:DWORD

    ; clear the output buffer
    invoke  ClearBuffer, [lpszBuffer], ERRORPAGE_MAX_LENGTH
    ; generate a filename string
    invoke  wsprintf, ADDR error_page_filename_buffer, ADDR error_page_filename_format, [iHTTPStatusCode]
    ; make it an absolute path from the base path
    invoke  GetAbsPathFromBase, ADDR error_page_filename_buffer, ADDR error_page_filename_buffer
    ; try to open it
    invoke  CreateFile, ADDR error_page_filename_buffer, GENERIC_READ, FILE_SHARE_READ or FILE_SHARE_WRITE, \
            0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0
    cmp     eax, -1
    jz      @@FALLBACK ; fall back to the provided error (the 'lpszFallbackPage' parameter) page if none is found
    mov     [hFile], eax
    invoke  GetFileSize, [hFile], 0
    cmp     eax, -1
    jz      @@CLOSE_HANDLE_AND_FALLBACK ; fall back if an error occurred while trying to retrieve the filesize
    cmp     eax, ERRORPAGE_MAX_LENGTH
    ja      @@CLOSE_HANDLE_AND_FALLBACK ; fall back if the page exceeds the maximum length
    mov     [iFileSize], eax
    ; generate the first part of the message and append the error page
    invoke  wsprintf, [lpszBuffer], [lpszHTTPMessage], eax
    mov     ecx, [iBufferSize]
    sub     ecx, eax
    cmp     [iFileSize], ecx
    ja      @@CLOSE_HANDLE_AND_FALLBACK ; fall back if the buffer is too small to store the HTTP message including the error page
    mov     edx, [lpszBuffer]
    lea     edx, [edx+eax]
    invoke  ReadFile, [hFile], edx, [iFileSize], ADDR ignore, 0
    or      eax, eax
    jz      @@CLOSE_HANDLE_AND_FALLBACK
    invoke  CloseHandle, [hFile] ; tidy up
    xor     eax, eax
    inc     eax
    ret

  @@CLOSE_HANDLE_AND_FALLBACK:
    invoke  CloseHandle, [hFile] ; tidy up
    ; jmp    @@FALLBACK

  @@FALLBACK:
    invoke  StrLen, [lpszFallbackPage]
    cmp     eax, ERRORPAGE_MAX_LENGTH
    ja      @@FAILURE
    invoke  wsprintf, [lpszBuffer], [lpszHTTPMessage], eax
    invoke  StrCat, [lpszBuffer], [lpszFallbackPage]
    xor     eax, eax
    ret

  @@FAILURE:
    xor     eax, eax
    dec     eax
    ret
LoadErrorPage endp

GetRequestEnd proc uses edi SRCDST:DWORD
    mov     edi, [SRCDST]
    mov     al, CR
    mov     ecx, 4092
    cld

    @@:
    repnz   scasb
    jnz     @@SEARCH_FOR_ZERO_END
    cmp     byte ptr [edi], 0
    jz      @F
    cmp     byte ptr [edi], LF
    jnz     @B
    cmp     byte ptr [edi+1], 0
    jz      @F
    cmp     byte ptr [edi+1], CR
    jnz     @B
    cmp     byte ptr [edi+2], 0
    jz      @F
    cmp     byte ptr [edi+2], LF
    jnz     @B

    @@SEARCH_FOR_ZERO_END:
    mov     edi, [SRCDST]
    xor     eax, eax
    mov     ecx, 4092
    cld
    repnz   scasb
    jnz     @F

    @@DONE:
    mov     byte ptr [edi+3], 0

    @@:
    ret
GetRequestEnd endp

CheckForConnectionStatus proc uses edi SRC:DWORD
    invoke  CharLower, [SRC]
    invoke  StrLen, [SRC]
    mov     ecx, eax
    mov     edi, [SRC]
    push    ecx
    dec     edi

    @@:
    inc     edi
    pop     ecx
    mov     al, 'c'
    cld
    repnz   scasb
    mov     eax, -1
    jnz     @@END_PROC
    dec     edi
    push    ecx
    movzx   eax, byte ptr [edi+11]
    push    eax
    mov     byte ptr [edi+11], 0
    invoke  StrCmp, edi, ADDR http_connection
    test    eax, eax
    pop     eax
    mov     byte ptr [edi+11], al
    mov     eax, -1
    jnz     @B
    pop     ecx
    ;'connection:' string found
    add     edi, 11
    cmp     byte ptr [edi], 20h
    jnz     @F
    inc     edi

    @@:
    movzx   eax, byte ptr [edi+11]
    push    eax
    mov     byte ptr [edi+10], 0
    invoke  StrCmp, edi, ADDR http_keep_alive
    test    eax, eax
    pop     eax
    mov     byte ptr [edi+10], al
    jz      @@KEEP_ALIVE
    movzx   eax, byte ptr [edi+11]
    push    eax
    mov     byte ptr [edi+5], 0
    invoke  StrCmp, edi, ADDR http_close
    test    eax, eax
    pop     eax
    mov     byte ptr [edi+5], al
    jz      @@CLOSE
    mov     eax, -1
    jmp     @@END_PROC

    @@CLOSE:
    mov     eax, 1 ;when returning 1, CLOSE was found
    jmp     @@END_PROC

    @@KEEP_ALIVE:
    mov     eax, 2 ;when returning 2, KEEP_ALIVE was found

    @@END_PROC:
    ret
CheckForConnectionStatus endp

FindHostOfRequest proc uses edi SRC:DWORD
    invoke  StrLen, [SRC]
    mov     ecx, eax
    mov     al, ' '
    mov     edi, [SRC]
    cld
    repnz   scasb
    jnz     @@RETURN_ERROR
    repnz   scasb
    jnz     @@RETURN_ERROR
    cmp     byte ptr [edi+7], '1'
    mov     eax, 1
    jnz     @@RETURN

    invoke  CharLower, [SRC]
    invoke  StrLen, [SRC]
    mov     ecx, eax
    mov     eax, 'h'
    mov     edi, [SRC]

    @@:
    cld
    repnz   scasb
    jnz     @@RETURN_ERROR ;when ECX = 0 and last byte was not equal too return an error
    cmp     byte ptr [edi], 'o'
    jnz     @B
    inc     edi
    cmp     byte ptr [edi], 's'
    jnz     @B
    inc     edi
    cmp     byte ptr [edi], 't'
    jnz     @B
    inc     edi
    cmp     byte ptr [edi], ':'
    jnz     @B
    mov     eax, 1
    jmp     @@RETURN

    @@RETURN_ERROR:
    xor eax, eax

    @@RETURN:
    ret
FindHostOfRequest endp

CheckIfHEADRequest proc SRC:DWORD
    mov     eax, [SRC]
    cmp     byte ptr [eax], 'H'
    jnz     @@RETURN_FALSE
    cmp     byte ptr [eax+1], 'E'
    jnz     @@RETURN_FALSE
    cmp     byte ptr [eax+2], 'A'
    jnz     @@RETURN_FALSE
    cmp     byte ptr [eax+3], 'D'
    jnz     @@RETURN_FALSE
    cmp     byte ptr [eax+4], ' '
    jnz     @@RETURN_FALSE
    xor     eax, eax
    ret

    @@RETURN_FALSE:
    or      eax, -1
    ret
CheckIfHEADRequest endp

CheckForResume proc uses esi edi SRC:DWORD, roffset:DWORD, eoffset:DWORD, rstring:DWORD, estring:DWORD, cresume:DWORD
    invoke  CharLower, [SRC]
    invoke  StrLen, [SRC]
    mov     edi, [SRC]
    mov     ecx, eax
    mov     al, 'r'     ; Range:............
    cld

  @@SEARCH:
    repnz   scasb       ; Search for 'R' character
    jnz     @@NORESUME  ; Not found
    ; Found, now check if strings are equal
    inc     ecx         ; One forward again (as scasb decreased)
    dec     edi         ; One back (as scasb increased)
    ; Now check :)
    push    eax
    push    edi
    push    ecx
    movzx   eax, byte ptr [edi+12]
    mov     byte ptr [edi+12], 0
    push    eax
    invoke  StrCmp, edi, ADDR http_range
    test    eax, eax    ; Same as cmp eax, 0 but more optimized (usually faster)
    pop     eax
    pop     ecx
    pop     edi
    mov     byte ptr [edi+12], al
    pop     eax
    jz      @@CANRESUME ; w00t!
    dec     ecx
    inc     edi
    jmp     @@SEARCH    ; Let's continue search if not same string

  @@CANRESUME:
    add     edi, 13
    mov     eax, [roffset]
    cmp     byte ptr [edi], ' '
    jnz     @F
    inc     edi

  @@:
    invoke  StrLen, edi
    mov     ecx, eax
    mov     eax, '-'
    push    edi
    repnz   scasb
    pop     esi
    jnz     @@NORESUME
    mov     eax, [cresume]
    mov     dword ptr [eax], TRUE
    mov     byte ptr [edi-1], 0
    invoke  StrCpy, [rstring], esi
    invoke  FpuAtoFL, esi, [roffset], 0
    mov     byte ptr [edi-1], "-"
    .if byte ptr [edi] >= "0" && byte ptr [edi] <= "9"
        push    edi         ; Save start offset of number
        invoke  StrLen, edi
        .while byte ptr [edi] >= "0" && byte ptr [edi] <= "9" && eax != 0
            inc     edi
        .endw
        mov     al, byte ptr [edi]
        mov     byte ptr [edi], 0
        mov     esi, edi    ; Put in ESI
        pop     edi         ; Restore start offset of number
        push    eax         ; Save byte from [EDI]
        push    esi         ; Save pointer
        invoke  StrCpy, [estring], edi
        pop     esi         ; Restore pointer
        pop     eax         ; Restore saved byte
        mov     byte ptr [esi], al  ; Restore byte to [ESI]
        invoke  FpuAtoFL, edi, [eoffset], 0
    .endif
    jmp     @@RET

  @@NORESUME:
    mov     eax, [cresume]
    mov     dword ptr [eax], FALSE

  @@RET:
    ret
CheckForResume endp

; Get the contents of a directory.
; flag = CONTENT_DIRECTORIES or CONTENT_FILES
; content_path = a wide character string
; total_buffer = the buffer to append the formatted data to
; Returns 1 on success, -1 on error.
MakeContents proc uses edi flag:DWORD, content_path:DWORD, total_buffer:DWORD
LOCAL find_data             :WIN32_FIND_DATA
LOCAL hFindData             :DWORD
LOCAL temp_buffer           :DWORD
LOCAL pMem                  :DWORD
LOCAL file_found            :DWORD
LOCAL datetime_buffer[32]   :BYTE
LOCAL last_mod_buffer[32]   :BYTE
LOCAL filesize_buffer[32]   :BYTE
LOCAL FileSizeReal          :QWORD
LOCAL temp                  :DWORD

    invoke  ClearBuffer, ADDR find_data, SIZEOF find_data

    mov     [file_found], FALSE

    mov     [temp_buffer], 0
    invoke  GlobalAlloc, GPTR, PATH_BUFFER_WIDE_LEN ; 12K
    or      eax, eax
    jz      @@RET
    mov     [temp_buffer], eax

    mov     [pMem], 0
    invoke  GlobalAlloc, GPTR, PATH_BUFFER_WIDE_LEN ; 12K
    or      eax, eax
    jz      @@RET
    mov     [pMem], eax

    .if [flag] == CONTENT_DIRECTORIES
        invoke  FindFirstFileW, [content_path], ADDR find_data
        cmp     eax, -1
        jz      @@RET
        mov     [hFindData], eax
        xor     eax, eax
        inc     eax
        .while eax == 1
            ; check if it's a file
            and     [find_data].dwFileAttributes, 10h
            jz      @@GET_NEXT_DIRECTORY

            ; skip POSIX '.' and '..' directories
            invoke  lstrcmpW, ADDR find_data.cFileName, ADDR posix_dot_dirW
            or      eax, eax
            jz      @@GET_NEXT_DIRECTORY
            invoke  lstrcmpW, ADDR find_data.cFileName, \
                              ADDR posix_dotdot_dirW
            or      eax, eax
            jz      @@GET_NEXT_DIRECTORY

        @@:
            ; File was not '.' or '..', ok, let's send :)
            ; Filename in find_data.cFileName
            cmp     [file_found], TRUE
            jz      @F
            invoke  StrCat, [total_buffer], ADDR dir_browse_directories
        @@:
            mov     [file_found], TRUE
            invoke  StrCat, [total_buffer], ADDR html_tr_tag_open
            invoke  StrCat, [total_buffer], ADDR html_img_dir
            invoke  StrCat, [total_buffer], ADDR html_td_a_tag_open

            invoke  lstrcpyW, [temp_buffer], ADDR find_data.cFileName
            invoke  lstrlenW, [temp_buffer]
            mov     edx, 2
            mul     edx ; strlen times two because we're dealing with WCHARs
            mov     edi, [temp_buffer]
            add     edi, eax

            ; throw in a '/' WCHAR to prevent an extra HTTP request to forward the client
            mov     byte ptr [edi], '/'
            mov     byte ptr [edi+1], 0

            ; two NULL bytes to ensure the WCHAR after '/' is NULL
            mov     byte ptr [edi+2], 0
            mov     byte ptr [edi+3], 0

            invoke  WideToUtf8, [temp_buffer], [pMem], (PATH_BUFFER_WIDE_LEN/2)
            invoke  StringToHexString, [pMem], [temp_buffer]
            invoke  StrCat, [total_buffer], [temp_buffer]

            ; Cut the '/' off again because we reuse the buffer for the HTML
            ; A tag.
            invoke  StrLen, [pMem]
            mov     edx, [pMem]
            mov     byte ptr [edx+eax-1], 0

            invoke  StrCat, [total_buffer], ADDR html_a_tag_first_close
            ; insert a few dots to indicate the filename is too long
            mov     edx, [pMem]
            mov     byte ptr [edx+28], '.'
            mov     byte ptr [edx+29], '.'
            mov     byte ptr [edx+30], '.'
            mov     byte ptr [edx+31], 0
            invoke  StrCat, [total_buffer], edx
            invoke  StrCat, [total_buffer], ADDR html_td_a_tag_second_close
            invoke  FileTimeToSystemTime, ADDR find_data.ftLastWriteTime, ADDR system_time
            test    eax, eax
            jz      @@GET_NEXT_DIRECTORY
            invoke  GetDateFormat, LOCALE_SYSTEM_DEFAULT, 0, ADDR system_time, ADDR date_format, ADDR datetime_buffer, 32
            test    eax, eax
            jz      @@GET_NEXT_DIRECTORY
            invoke  StrCat, [total_buffer], ADDR datetime_buffer
            @@GET_NEXT_DIRECTORY:
            invoke  FindNextFileW, [hFindData], ADDR find_data
        .endw
        invoke  FindClose, [hFindData]
        invoke  StrCat, [total_buffer], ADDR html_table_tag_close
    .elseif [flag] == CONTENT_FILES
        invoke  FindFirstFileW, [content_path], ADDR find_data
        cmp     eax, -1
        jz      @@RET
        mov     [hFindData], eax
        xor     eax, eax
        inc     eax
        .while eax == 1
            ; skip directories
            and     [find_data].dwFileAttributes, 10h
            jnz     @@GET_NEXT_FILE

            ; skip POSIX '.' and '..' directories
            invoke  lstrcmpW, ADDR find_data.cFileName, ADDR posix_dot_dirW
            or      eax, eax
            jz      @@GET_NEXT_FILE
            invoke  lstrcmpW, ADDR find_data.cFileName, \
                              ADDR posix_dotdot_dirW
            or      eax, eax
            jz      @@GET_NEXT_FILE

        @@:
            ; File was not '.' or '..', ok, let's send :)
            ; Filename in find_data.cFileName
            cmp     [file_found], TRUE
            jz      @F
            invoke  StrCat, [total_buffer], ADDR dir_browse_files
        @@:
            mov     [file_found], TRUE
            invoke  StrCat, [total_buffer], ADDR html_tr_tag_open
            invoke  StrCat, [total_buffer], ADDR html_img_file
            invoke  StrCat, [total_buffer], ADDR html_td_a_tag_open
            invoke  WideToUtf8, ADDR find_data.cFileName, [temp_buffer], (PATH_BUFFER_WIDE_LEN/2)
            invoke  StringToHexString, [temp_buffer], [pMem]
            invoke  StrCat, [total_buffer], [pMem]
            invoke  StrCat, [total_buffer], ADDR html_a_tag_first_close
            mov     edx, [temp_buffer]
            mov     byte ptr [edx+28], '.'
            mov     byte ptr [edx+29], '.'
            mov     byte ptr [edx+30], '.'
            mov     byte ptr [edx+31], 0
            invoke  StrCat, [total_buffer], edx
            invoke  StrCat, [total_buffer], ADDR html_td_a_tag_second_close
            invoke  FileTimeToSystemTime, ADDR find_data.ftLastWriteTime, ADDR system_time
            test    eax, eax
            jz      @F
            invoke  GetDateFormat, LOCALE_SYSTEM_DEFAULT, 0, ADDR system_time, ADDR date_format, ADDR datetime_buffer, 32
            test    eax, eax
            jz      @F
            invoke  StrCat, [total_buffer], ADDR datetime_buffer
        @@:
            invoke  StrCat, [total_buffer], ADDR html_td_tag_close
            invoke  StrCat, [total_buffer], ADDR html_td_tag_open
            ; Calculate the file's size
            .if [find_data.nFileSizeLow] < ONE_KILOBYTE && [find_data.nFileSizeHigh] == 0
                invoke  wsprintf, ADDR filesize_buffer, ADDR filesize_format, [find_data.nFileSizeLow]
                invoke  StrCat, [total_buffer], ADDR filesize_buffer
                invoke  StrCat, [total_buffer], ADDR filesize_b
            .elseif [find_data.nFileSizeLow] < ONE_MEGABYTE && [find_data.nFileSizeHigh] == 0
                ; Calculate filesize in KiB using x87 FPU coprocessor
                fild    [find_data.nFileSizeLow]
                mov     [temp], ONE_KILOBYTE
                fild    [temp]
                fdivp   st(1), st(0)
                invoke  FpuFLtoA, 0, 2, ADDR filesize_buffer, SRC1_FPU
                ffree   st(0)
                invoke  StrCpy, ADDR filesize_buffer, ADDR filesize_buffer+1
                invoke  StrCat, [total_buffer], ADDR filesize_buffer
                invoke  StrCat, [total_buffer], ADDR filesize_kib
            .elseif [find_data.nFileSizeLow] < ONE_GIGABYTE && [find_data.nFileSizeHigh] == 0
                ; Calculate filesize in MiB using x87 FPU coprocessor
                fild    [find_data.nFileSizeLow]
                mov     [temp], ONE_KILOBYTE
                fild    [temp]
                fdiv    st(1), st(0)
                fdivp   st(1), st(0)
                invoke  FpuFLtoA, 0, 2, ADDR filesize_buffer, SRC1_FPU
                ffree   st(0)
                invoke  StrCpy, ADDR filesize_buffer, ADDR filesize_buffer+1
                invoke  StrCat, [total_buffer], ADDR filesize_buffer
                invoke  StrCat, [total_buffer], ADDR filesize_mib
            .else
                ; Calculate filesize in GiB using x87 FPU coprocessor
                invoke  dqtoa, [find_data.nFileSizeLow], [find_data.nFileSizeHigh], 0, ADDR FileSizeReal
                fld     [FileSizeReal]
                mov     [temp], ONE_KILOBYTE
                fild    [temp]
                fdiv    st(1), st(0)
                fdiv    st(1), st(0)
                fdivp   st(1), st(0)
                invoke  FpuFLtoA, 0, 2, ADDR filesize_buffer, SRC1_FPU
                ffree   st(0)
                invoke  StrCpy, ADDR filesize_buffer, ADDR filesize_buffer+1
                invoke  StrCat, [total_buffer], ADDR filesize_buffer
                invoke  StrCat, [total_buffer], ADDR filesize_gib
            .endif
            invoke  StrCat, [total_buffer], ADDR html_tdtr_tag_close

            @@GET_NEXT_FILE:
            invoke  FindNextFileW, [hFindData], ADDR find_data
        .endw
        invoke  FindClose, [hFindData]
        invoke  StrCat, [total_buffer], ADDR html_table_tag_close
    .else
        or  eax, -1
    .endif
    jmp     @@RET

    @@RET:
    .if [temp_buffer] != 0
        invoke  GlobalFree, [temp_buffer]
    .endif

    .if [pMem] != 0
        invoke  GlobalFree, [pMem]
    .endif

    .if [file_found] != TRUE
        or      eax, -1
    .else
        xor     eax, eax
        inc     eax
    .endif
    ret
MakeContents endp

InitializeStructures proc
    mov     eax, [ConnInfoStructs]
    assume eax:PTR ConnInfo

  @@SEARCH:
    cmp     eax, [EndAddress]
    jae     @@DONE
    mov     [eax].hSock, 0
    mov     [eax].hThread, 0
    mov     [eax].hEventStart, 0
    mov     [eax].WaitingStatus, 0
    mov     [eax].ThreadRunning, 0
    mov     [eax].RecvData, 0
    mov     [eax].fd_close, 0
    mov     [eax].kaTimer, 0
    add     eax, SIZEOF ConnInfo
    jmp     @@SEARCH

  @@DONE:
    assume eax:NOTHING
    ret
InitializeStructures endp

CloseAllConnectionsAndCleanup proc
    mov     eax, [ConnInfoStructs]
    assume eax:PTR ConnInfo

  @@SEARCH:
    cmp     eax, [EndAddress]
    ja      @@DONE
    .if [eax].hSock != 0 && [eax].hSock != -1
        push    eax
        invoke  closesocket, [eax].hSock
        pop     eax
    .endif
    add     eax, SIZEOF ConnInfo
    jmp     @@SEARCH

  @@DONE:
    assume eax:NOTHING
    invoke  InitializeStructures
    ret
CloseAllConnectionsAndCleanup endp

AddStruct proc BaseAddress:DWORD, hSock:DWORD, hThread:DWORD, hEventStart:DWORD, WaitingStatus:DWORD
    ;tries to find an empty place, then it
    ;places the params in the found address
    mov     eax, [BaseAddress]
    assume eax:PTR ConnInfo

    @@SEARCH:
    cmp     eax, EndAddress
    ja      @@FAILURE
    cmp     [eax].hSock, -1
    jz      @@FOUND_PLACE
    cmp     [eax].hSock, 0
    jz      @@FOUND_PLACE
    jmp     @@ADD_AND_CONTINUE

    @@FOUND_PLACE:
    mov     ecx, [hSock]
    mov     [eax].hSock, ecx
    mov     ecx, [hThread]
    mov     [eax].hThread, ecx
    mov     ecx, [hEventStart]
    mov     [eax].hEventStart, ecx
    mov     ecx, [WaitingStatus]
    mov     [eax].WaitingStatus, ecx
    jmp     @@DONE

    @@ADD_AND_CONTINUE:
    add     eax, SIZEOF ConnInfo
    jmp     @@SEARCH

    @@FAILURE:
    ;means all memory is full, after return --> HTTP 503 message should be send to the client to indicate
    ;that the server is busy!
    mov     eax, -1

    @@DONE:
    assume eax:NOTHING
    ret
AddStruct endp

RemoveStruct proc BaseAddressOfStructure:DWORD
;when removing a structure, fill structure with -1 (each param of the struct)
;to indicate the structure is not used, and can be reused
    mov     eax, [BaseAddressOfStructure]
    assume eax:PTR ConnInfo
    mov     [eax].hSock, -1
    mov     [eax].hThread, -1
    mov     ecx, [eax].hEventStart
    push    eax
    invoke  CloseHandle, ecx
    pop     eax
    mov     [eax].hEventStart, -1
    mov     [eax].WaitingStatus, -1
    mov     [eax].ThreadRunning, -1
    mov     [eax].RecvData, -1
    mov     [eax].fd_close, -1
    assume eax:NOTHING
    ret
RemoveStruct endp

FindStruct proc BaseAddress:DWORD, hSock:DWORD, hThread:DWORD, hEventStart:DWORD
    ;begins searching for hSock / hThread or hEventStart, when found,
    ;it must search the base address of the structure, (find a dword with -1 backwards from
    ;the address of the found param)
    mov     eax, [BaseAddress]
    assume eax:PTR ConnInfo

    @@SEARCH:
    cmp     eax, [EndAddress]
    ja      @@FAILURE
    cmp     [eax].hSock, -1
    jz      @@ADD_AND_CONTINUE
    cmp     [eax].hThread, -1
    jz      @@ADD_AND_CONTINUE
    cmp     [eax].hEventStart, -1
    jz      @@ADD_AND_CONTINUE
    cmp     [eax].hSock, 0
    jz      @@ADD_AND_CONTINUE
    cmp     [eax].hThread, 0
    jz      @@ADD_AND_CONTINUE
    cmp     [eax].hEventStart, 0
    jz      @@ADD_AND_CONTINUE
    .if [hSock] != 0
        mov     edx, [hSock]
        cmp     [eax].hSock, edx
        jz      @@DONE
    .elseif [hThread] != 0
        mov     edx, [hThread]
        cmp     [eax].hThread, edx
        jz      @@DONE
    .elseif [hEventStart] != 0
        mov     edx, [hEventStart]
        cmp     [eax].hEventStart, edx
        jz      @@DONE
    .else
        or      eax, -1
        jmp     @@DONE
    .endif

    @@ADD_AND_CONTINUE:
    add     eax, SIZEOF ConnInfo
    jmp     @@SEARCH

    @@FAILURE:
    mov     eax, -1

    @@DONE:
    assume eax:NOTHING
    ret
FindStruct endp

; update current thread count
CheckThreadCount proc hWnd:DWORD, uMsg:DWORD, idEvent:DWORD, dwTime:DWORD
LOCAL sep                       :DWORD

    ; don't update
    cmp     [status_bRunning], FALSE
    jz      @@RET

    mov     [sep], 000202d20h ; ' - '

    ; update main window
    invoke  wsprintf, ADDR status_buffer, ADDR status_conncount, [gThreadCount]
    invoke  SendMessage, [hStatus], SB_SETTEXT, 2, ADDR status_buffer

    ; update notification area icon tooltip
    invoke  StrCpy, ADDR notarea_icon_data.szTip, ADDR app_name
    invoke  StrCat, ADDR notarea_icon_data.szTip, ADDR sep
    invoke  StrCat, ADDR notarea_icon_data.szTip, ADDR status_buffer
    invoke  Shell_NotifyIcon, NIM_MODIFY, ADDR notarea_icon_data

@@RET:
    ret
CheckThreadCount endp

CheckKeepAliveTimeout proc hWnd:DWORD, uMsg:DWORD, idEvent:DWORD, dwTime:DWORD
LOCAL temp                      :DWORD

    mov     eax, [ConnInfoStructs]
    assume eax:PTR ConnInfo

    @@SEARCH:
    cmp     eax, [EndAddress]
    ja      @@DONE
    cmp     [eax].hSock, 0
    jz      @@ADD_AND_CONTINUE
    cmp     [eax].hSock, -1
    jz      @@ADD_AND_CONTINUE
    cmp     [eax].ThreadRunning, TRUE
    jz      @@ADD_AND_CONTINUE
    cmp     [eax].kaTimer, 300
    ja      @@KILLCONN_IF_NOT_ACTIVE
    inc     [eax].kaTimer

    @@KILLCONN_IF_NOT_ACTIVE:
    cmp     [eax].WaitingStatus, TRUE
    jnz     @@ADD_AND_CONTINUE
    mov     [temp], eax
    invoke  RemoveStruct, eax
    invoke  closesocket, [eax].hSock
    mov     eax, [temp]
    assume eax:NOTHING
    ; jmp        @@ADD_AND_CONTINUE

    @@ADD_AND_CONTINUE:
    add     eax, SIZEOF ConnInfo
    jmp     @@SEARCH

    @@DONE:
    ret
CheckKeepAliveTimeout endp
