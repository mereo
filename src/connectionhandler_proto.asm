; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

CloseConnectionThread PROTO hSocket:DWORD
HandleConnectionThread PROTO pConnInfo:DWORD
ParsePath PROTO SRC:DWORD
ParseRequestPath PROTO SRC:DWORD, DST:DWORD
FindMIME PROTO SRC:DWORD, DST:DWORD, CNT:DWORD
FilterDoubleSlash PROTO SRCDST:DWORD
CheckForDDots PROTO SRC:DWORD
ParseAbsURI PROTO SRC:DWORD
StringToHexString PROTO SRC:DWORD, DST:DWORD
GetHexValue PROTO SRC:DWORD
HexStringToString PROTO SRC:DWORD, DST:DWORD
LoadErrorPage PROTO lpszBuffer:DWORD, iBufferSize:DWORD, lpszHTTPMessage:DWORD, iHTTPStatusCode:DWORD, lpszFallbackPage:DWORD
GetRequestEnd PROTO SRCDST:DWORD
CheckForConnectionStatus PROTO SRC:DWORD
FindHostOfRequest PROTO SRC:DWORD
CheckIfHEADRequest PROTO SRC:DWORD
CheckForResume PROTO SRC:DWORD, roffset:DWORD, eoffset:DWORD, rstring:DWORD, estring:DWORD, cresume:DWORD
MakeContents PROTO flag:DWORD, content_path:DWORD, total_buffer:DWORD
InitializeStructures PROTO
CloseAllConnectionsAndCleanup PROTO
AddStruct PROTO BaseAddress:DWORD, hSock:DWORD, hThread:DWORD, hEventStart:DWORD, WaitingStatus:DWORD
RemoveStruct PROTO BaseAddressOfStructure:DWORD
FindStruct PROTO BaseAddress:DWORD, hSock:DWORD, hThread:DWORD, hEventStart:DWORD
CheckThreadCount PROTO hWnd:DWORD, uMsg:DWORD, idEvent:DWORD, dwTime:DWORD
CheckKeepAliveTimeout PROTO hWnd:DWORD, uMsg:DWORD, idEvent:DWORD, dwTime:DWORD
