; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

Log_AllocateMemory proc
    ; Parse log memory option
    invoke  cfg_ParseInteger, [hIniFile], ADDR cfg_prefs_logmem_header
    .if eax != -1
        mov     [prefs_logmem], eax
    .endif

    ; Allocate memory
    invoke  GlobalAlloc, GMEM_ZEROINIT, [prefs_logmem]
    test    eax, eax
    jz      @@NO_MEMORY ; in 'mereo.asm'
    mov     [http_logBuffer], eax

    ; Update log status
    mov     [logfullflag], 0
    ret
Log_AllocateMemory endp

Log_FreeMemory proc
    invoke  GlobalFree, [http_logBuffer]
    ret
Log_FreeMemory endp

SaveLog proc hwndDlg:DWORD
LOCAL hFile:DWORD, numb:DWORD

    ; Write it to file
    mov     [ofn.lStructSize], SIZEOF ofn
    mov     eax, [hwndDlg]
    mov     [ofn.hwndOwner], eax
    lea     eax, [FilePath]
    mov     [ofn.lpstrFile], eax
    mov     [ofn.lpstrFilter], OFFSET FilterString
    mov     [ofn.nMaxFile], 260
    mov     [ofn.Flags], OFN_LONGNAMES or OFN_EXPLORER or OFN_HIDEREADONLY or OFN_OVERWRITEPROMPT
    mov     [ofn.lpstrInitialDir], 0
    mov     [ofn.lpstrDefExt], OFFSET FileExt
    invoke  GetSaveFileName, ADDR ofn
    .if eax != 0
        invoke  CreateFile, ADDR FilePath, GENERIC_READ or GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0
        mov     [hFile], eax
        .if eax == -1
            invoke  MessageBox, [hwndDlg], ADDR SavingLogError, ADDR app_name, MB_ICONSTOP
        .else
            invoke  StrLen, [http_logBuffer]
            lea     edx, [numb]
            invoke  WriteFile, [hFile], [http_logBuffer], eax, edx, 0
            invoke  CloseHandle, [hFile]
            invoke  MessageBox, [hwndDlg], ADDR SavingLogSuccess, ADDR app_name, MB_ICONINFORMATION
        .endif
    .endif
    ret
SaveLog endp

LogDialogProc proc hwndDlg:DWORD, uMsg:DWORD, wParam:DWORD, lParam:DWORD
.if [uMsg] == WM_INITDIALOG
    mov     eax, [hwndDlg]
    mov     [hLogDialog], eax
    invoke  LoadIcon, [hInstance], ID_MAIN_ICON
    push    eax
    invoke  SendMessage, [hwndDlg], WM_SETICON, ICON_BIG, eax
    pop     eax
    invoke  DestroyIcon, eax
    invoke  SetDlgItemText, [hwndDlg], ID_LOG_TEXT, [http_logBuffer]
.elseif [uMsg] == WM_COMMAND
    .if [wParam] == IDCANCEL
        jz  @F
    .elseif [wParam] == IDM_SAVELOG
        invoke  SaveLog, [hwndDlg]
    .endif
.elseif [uMsg] == WM_CLOSE
  @@:
    mov     [hLogDialog], 0
    invoke  EndDialog, [hwndDlg], 0
.endif
xor     eax, eax
ret
LogDialogProc endp
