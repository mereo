; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

; input = the encoded UTF-8 string
; buffer = the output buffer to store the Unicode string in
; bufferSize = the amount of WCHAR values that fit into 'buffer'
Utf8ToWide proc input:DWORD, buffer:DWORD, bufferSize:DWORD
    invoke  MultiByteToWideChar, CP_UTF8, 0, [input], -1, [buffer], \
                                 [bufferSize]
    ret
Utf8ToWide endp

; input = the Unicode string
; buffer = the output buffer to store the encoded UTF-8 string in
; bufferSize = the amount of WCHAR values that fit into 'buffer'
WideToUtf8 proc input:DWORD, buffer:DWORD, bufferSize:DWORD
    invoke  WideCharToMultiByte, CP_UTF8, 0, [input], -1, [buffer], \
                                 [bufferSize], NULL, NULL
    ret
WideToUtf8 endp
