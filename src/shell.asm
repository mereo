; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

; Opens a dialog with SHBrowseForFolder
;
; Parameters:
; lpszFilename  = receives the filename of the file chosen by the user
;
; Return values:
; Returns TRUE on success. On failure, a value of FALSE is returned.
SHGetOpenDirName proc hwndDlg:DWORD, lpszFilename:DWORD
LOCAL browse_info           :BROWSEINFO

    invoke  ClearBuffer, ADDR browse_info, SIZEOF BROWSEINFO

    mov     [browse_info.pidlRoot], 0
    mov     eax, [hwndDlg]
    mov     [browse_info.hwndOwner], eax
    mov     [browse_info.lpszTitle], OFFSET browse_title
    mov     [browse_info.ulFlags], BIF_RETURNONLYFSDIRS or BIF_EDITBOX or BIF_USENEWUI or BIF_BROWSEINCLUDEURLS

    invoke  SHBrowseForFolder, ADDR browse_info
    or      eax, eax
    jnz     @F
    ret
    @@:
    invoke  SHGetPathFromIDList, eax, [lpszFilename]

    ret
SHGetOpenDirName endp

OpenWebrootThread proc lpszFile:DWORD
    invoke  StrLen, [lpszFile]
    add     eax, [lpszFile]
    .if byte ptr [eax-1] != '\' && byte ptr [eax-1] != '/'
        mov     byte ptr [eax], '\'
        mov     byte ptr [eax+1], 0
    .endif
    invoke  ShellExecute, 0, ADDR help_operation, [lpszFile], 0, 0, SW_SHOWDEFAULT
    invoke  ExitThread, 0
OpenWebrootThread endp
