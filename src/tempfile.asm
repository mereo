; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

; Creates a temporary file and writes data to it
;
; Parameters:
; lpszFilename  = receives the filename for the temporary file
; pData         = the data to be written to the temporary file
;
; Return values:
; The file handle of the new temporary file is returned on success.
; On failure, a value of INVALID_HANDLE_VALUE is returned.
;
; Remarks:
; The file pointer of the temporary file is set to 0 after the
; data has been written to it.
WriteTempFile proc lpszFilename:DWORD, sizeofFilenameBuffer:DWORD, pData:DWORD, sizeofData:DWORD
LOCAL lpPrefix[4]           :BYTE
LOCAL hFile                 :DWORD

    ; Put 'htt' prefix in the prefix buffer
    lea     eax, [lpPrefix]
    mov     byte ptr [eax], 'h'
    inc     eax
    mov     byte ptr [eax], 't'
    inc     eax
    mov     byte ptr [eax], 't'
    inc     eax
    mov     byte ptr [eax], 0

    invoke  GetTempPath, [sizeofFilenameBuffer], [lpszFilename]
    .if eax == 0
        ret
    .endif
    invoke  GetTempFileName, [lpszFilename], ADDR lpPrefix, 0, [lpszFilename]
    .if eax == 0
        ret
    .endif
    invoke  CreateFile, [lpszFilename], GENERIC_READ or GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL or FILE_FLAG_DELETE_ON_CLOSE, 0
    .if eax == INVALID_HANDLE_VALUE
        ret
    .endif
    mov     [hFile], eax
    ; Write the data to the temporary file, use lpPrefix as 'lpNumberOfBytesWritten' because the variable is no longer in use
    invoke  WriteFile, [hFile], [pData], [sizeofData], ADDR lpPrefix, 0
    invoke  SetFilePointer, [hFile], 0, 0, FILE_BEGIN

    mov     eax, [hFile]
    ret
WriteTempFile endp
