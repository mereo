; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

; Retrieves the position of a favorite header in the configuration file
; Returns a pointer to the favorite data in EAX.
; Returns -1 on failure.
GetFavoriteFilePosition proc uses ebx esi edi _fav_no:DWORD
LOCAL pMem:DWORD, fav_buffer[16]:BYTE, numb:DWORD

    ; make sure [pMem] is NULL
    mov     [pMem], NULL

    invoke  GetFileSize, [hIniFile], 0
    cmp     eax, -1
    jz      @@FILE_ERROR
    push    eax
    inc     eax ; preserve one NULL byte
    invoke  GlobalAlloc, GPTR, eax
    add     esp, 4
    or      eax, eax
    jz      @@MEM_ERROR
    sub     esp, 4
    mov     [pMem], eax
    invoke  SetFilePointer, [hIniFile], 0, 0, FILE_BEGIN
    pop     ecx
    invoke  ReadFile, [hIniFile], [pMem], ecx, ADDR numb, 0
    or      eax, eax
    jz      @@FILE_ERROR

    ; Check for [FavoriteXX], when found, check for DirectoryBrowsing=X in the current [FavoriteXX]
    ; block. Blocks are split with double CR LF's
    invoke  wsprintf, ADDR fav_buffer, ADDR fav_number, [_fav_no]
    invoke  instringi, [pMem], ADDR fav_buffer
    cmp     eax, -1
    jz      @@GETFAV_END
    mov     edi, eax
    add     edi, [pMem]
    ; Check split up the found block so it's the only block visible in the memory block
    inc     edi ; increase by one so we skip the header we've already found
    invoke  instringi, edi, ADDR fav_header_check
    .if eax != -1
        ; Now split up the current [FavoriteXX] block by a NULL byte in memory
        add     eax, edi
        mov     byte ptr [eax], 0
    .endif
    dec     edi ; and decrease back by one
    invoke  StrLen, edi
    mov     ecx, eax
    mov     esi, edi
    mov     edi, [pMem]
    cld
    rep     movsb
    mov     byte ptr [edi], 0
    mov     eax, [pMem]
    jmp     @@GETFAV_RET

  @@MEM_ERROR:
    invoke  MessageBox, [ghwndDlg], ADDR no_memory, ADDR app_name, MB_ICONSTOP
    mov     eax, -1
    jmp     @@GETFAV_END

  @@FILE_ERROR:
    invoke  MessageBox, [ghwndDlg], ADDR favNoFavFileCreated, ADDR app_name, MB_ICONSTOP
    mov     eax, -1
    ; jmp    @@GETFAV_END

  @@GETFAV_END:
    ; Free memory allocated for file
    .if [pMem] != NULL
        push    eax
        invoke  GlobalFree, [pMem]
        pop     eax
    .endif
    ; jmp    @@GETFAV_RET

  @@GETFAV_RET:
    ret
GetFavoriteFilePosition endp

; Returns the "bind address" for the favorite in ECX. Returns TRUE in EAX on success or -1 in EAX on failure.
GetFavoriteHeaderValue proc uses ebx esi edi _fav_no:DWORD, favHeaderName:DWORD
LOCAL pMem                  :DWORD
LOCAL fav_buffer[16]        :BYTE

    ; make sure [pMem] is NULL
    mov     [pMem], NULL

    invoke  GetFavoriteFilePosition, [_fav_no]
    cmp     eax, -1
    jz      @@GETFAV_END
    mov     [pMem], eax
    mov     edi, eax

    invoke  instringi, edi, [favHeaderName]
    cmp     eax, -1
    jz      @@GETFAV_END

    add     eax, edi
    mov     edi, eax
    invoke  StrLen, [favHeaderName]
    add     edi, eax ; length of header name
    mov     esi, edi
    invoke  StrLen, edi
    mov     ecx, eax
    mov     eax, CR
    ; cld
    repnz   scasb
    jnz     @F
    dec     edi
    mov     byte ptr [edi], 0
  @@:

    invoke  StrCpy, [pMem], esi
    mov     eax, [pMem]
    jmp     @@GETFAV_END

  @@MEM_ERROR:
    invoke  MessageBox, [ghwndDlg], ADDR no_memory, ADDR app_name, MB_ICONSTOP
    mov     eax, -1
    ; jmp     @@GETFAV_END

  @@GETFAV_END:
    ret
GetFavoriteHeaderValue endp

; Retrieves a favorite name and inserts the favorite number as prefix
GetFavoriteName proc uses esi edi _fav_no:DWORD
    invoke  GetFavoriteHeaderValue, [_fav_no], ADDR fav_name
    cmp     eax, -1
    jz      @@RET
    mov     esi, eax
    invoke  StrLen, eax
    add     eax, 8
    invoke  GlobalAlloc, GPTR, eax
    mov     edi, eax
    invoke  wsprintf, edi, ADDR favTemplate, [_fav_no], esi
    invoke  GlobalFree, esi
    mov     eax, edi
  @@RET:
    ret
GetFavoriteName endp

GetNextFavNumber proc
LOCAL fav_no                :DWORD
LOCAL last_found_number     :DWORD

    mov     [fav_no], 0
    mov     [last_found_number], 0
    .while [fav_no] <= 32
        inc     [fav_no]
        invoke  GetFavoriteHeaderValue, [fav_no], ADDR fav_name
        .if eax != -1
            invoke  GlobalFree, eax
            mov     eax, [fav_no]
            mov     [last_found_number], eax
        .else
            mov     eax, [fav_no]
            dec     eax
            mov     [last_found_number], eax
            .break
        .endif
    .endw
    mov     eax, [last_found_number]
    inc     eax
    ret
GetNextFavNumber endp

ParseFavorites proc uses ebx esi edi hwndDlg:DWORD
LOCAL fav_no:DWORD, favCurrentID:DWORD, first_added_item:DWORD, numb:DWORD

    mov     [favCurrentID], (IDM_FAV-1)
    mov     [fav_no], 0
    mov     [first_added_item], TRUE

    invoke  ReadFile, [hIniFile], ADDR favTemp, 1, ADDR numb, 0
    or      eax, eax
    jz      @@FILE_ERROR

    invoke  GetSubMenu, [hMenu], 3
    mov     ebx, eax

    mov     eax, TRUE
    mov     edi, MF_BYPOSITION
    .while eax != FALSE
        invoke  DeleteMenu, ebx, 2, edi
    .endw

    mov     esi, -1
    .while [fav_no] <= 32
        inc     [favCurrentID]
        mov     eax, [favCurrentID]
        cmp     eax, IDM_MAXFAV
        jz      @@PARSE_FAV_FINISH

        .if esi != -1
            .if [first_added_item] == TRUE
                mov     [first_added_item], FALSE
                mov     [favMenuInfo.cbSize], SIZEOF MENUITEMINFO
                mov     [favMenuInfo.fMask], MIIM_STATE or MIIM_TYPE
                mov     [favMenuInfo.fType], MFT_SEPARATOR
                mov     [favMenuInfo.fState], 0
                mov     [favMenuInfo.hSubMenu], 0
                mov     [favMenuInfo.hbmpChecked], 0
                mov     [favMenuInfo.hbmpUnchecked], 0
                mov     [favMenuInfo.dwItemData], 0
                mov     [favMenuInfo.wID], 0
                mov     [favMenuInfo.dwTypeData], 0
                mov     [favMenuInfo.cch], 0
                invoke  InsertMenuItem, ebx, 3, FALSE, ADDR favMenuInfo
            .endif
            mov     [favMenuInfo.cbSize], SIZEOF MENUITEMINFO
            mov     [favMenuInfo.fMask], MIIM_ID or MIIM_STATE or MIIM_TYPE
            mov     [favMenuInfo.fType], MFT_STRING
            mov     [favMenuInfo.fState], MFS_ENABLED
            mov     [favMenuInfo.hSubMenu], 0
            mov     [favMenuInfo.hbmpChecked], 0
            mov     [favMenuInfo.hbmpUnchecked], 0
            mov     [favMenuInfo.dwItemData], 0
            mov     eax, [favCurrentID]
            mov     [favMenuInfo.wID], eax
            mov     [favMenuInfo.dwTypeData], esi
            invoke  StrLen, esi
            mov     [favMenuInfo.cch], eax
            invoke  InsertMenuItem, ebx, 3, FALSE, ADDR favMenuInfo
        .endif

        .if esi != -1 && [fav_no] != 0
            invoke  GlobalFree, esi
        .endif
        inc     [fav_no]
        invoke  GetFavoriteName, [fav_no]
        mov     esi, eax
    .endw
    jmp     @@PARSE_FAV_FINISH
  @@FILE_ERROR:
    invoke  MessageBox, [ghwndDlg], ADDR favNoFavFileCreated, ADDR app_name, MB_ICONSTOP
  @@PARSE_FAV_FINISH:
    ret
ParseFavorites endp

AddFavDialogProc proc uses ebx esi edi hwndDlg:DWORD, uMsg:DWORD, wParam:DWORD, lParam:DWORD
LOCAL pTempPathBuffer[4096]     :BYTE
LOCAL temp                      :DWORD
LOCAL hWndItem                  :DWORD
LOCAL lbGetInt                  :DWORD

    .if [uMsg] == WM_INITDIALOG
        mov     [fav_data_path], 0
        invoke  SendMessage, [hwndDlg], WM_SETICON, ICON_BIG, [hMainIcon]

        invoke  GetDlgItem, [hwndDlg], IDC_DIRBROWSING
        mov     [hFavDirBrowsing], eax

        invoke  GetDlgItem, [hwndDlg], IDE_ADDFAV_BIND_ADDRESS
        mov     [hFavBindAddress], eax

        ; Parse the port (if any) and set it (or set the default).
        invoke  GetDlgItemInt, [ghwndDlg], ID_MAIN_PORT, ADDR lbGetInt, FALSE
        .if eax == 0 || eax > 0FFFFh
            mov     eax, [HTTP_DEFAULT_PORT]
        .endif
        invoke  SetDlgItemInt, [hwndDlg], ID_ADDFAV_PORT, eax, FALSE

        ; Get the webroot path.
        invoke  GetDlgItemText, [ghwndDlg], IDE_ROOT, ADDR pTempPathBuffer, SIZEOF pTempPathBuffer
        invoke  SetDlgItemText, [hwndDlg], ID_ADDFAV_PATH, ADDR pTempPathBuffer

        ; Check if 'Directory Browsing' enabled in main dialog.
        ; If it is, it will be enabled in this dialog as well.
        invoke  SendMessage, [hDirBrowsing], BM_GETCHECK, 0, 0
        cmp     eax, BST_CHECKED
        jnz     @F
        invoke  SendMessage, [hFavDirBrowsing], BM_SETCHECK, BST_CHECKED, 0
      @@:

        ; Copy 'Bind Address' from the main dialog
        invoke  SendMessage, [hBindAddress], WM_GETTEXT, SIZEOF fav_ip_bind_address, ADDR fav_ip_bind_address
        invoke  SendMessage, [hFavBindAddress], WM_SETTEXT, 0, ADDR fav_ip_bind_address

        ; Set focus on the name field.
        invoke  GetDlgItem, [hwndDlg], ID_ADDFAV_NAME
        mov     [hWndItem], eax
        invoke  SetFocus, [hWndItem]
    .elseif [uMsg] == WM_COMMAND
        .if [wParam] == IDCANCEL
            jmp     @@WM_CLOSE
        .elseif [wParam] == IDC_DIRBROWSING
            invoke  SendMessage, [hFavDirBrowsing], BM_GETCHECK, 0, 0
            .if eax == BST_CHECKED
                mov     eax, BST_UNCHECKED
            .else
                mov     eax, BST_CHECKED
            .endif
            invoke  SendMessage, [hFavDirBrowsing], BM_SETCHECK, eax, 0
        .elseif [wParam] == ID_ADDFAV_BROWSE
            invoke  SHGetOpenDirName, [hwndDlg], ADDR pTempPathBuffer
            or      eax, eax
            jz      @@EXIT
            invoke  SetDlgItemText, [hwndDlg], ID_ADDFAV_PATH, ADDR pTempPathBuffer
        .elseif [wParam] == ID_ADDFAV_FINISH || [wParam] == IDOK
            ; Retrieve favorite name
            invoke  GetDlgItemText, [hwndDlg], ID_ADDFAV_NAME, ADDR fav_data_name, 64
            .if eax == 0
                invoke  MessageBox, [hwndDlg], ADDR input_name_error, ADDR app_name, MB_ICONEXCLAMATION
                jmp     @@EXIT
            .else
                mov     byte ptr [temp], '='
                mov     byte ptr [temp+1], 0
                invoke  instringi, ADDR fav_data_name, ADDR temp
                push    eax
                mov     byte ptr [temp], '['
                mov     byte ptr [temp+1], 0
                invoke  instringi, ADDR fav_data_name, ADDR temp
                push    eax
                mov     byte ptr [temp], ']'
                mov     byte ptr [temp+1], 0
                invoke  instringi, ADDR fav_data_name, ADDR temp
                pop     edx
                pop     ecx
                .if eax != -1 || ecx != -1 || edx != -1
                    invoke  MessageBox, [hwndDlg], ADDR fav_invalid_name, ADDR app_name, MB_ICONEXCLAMATION
                    jmp     @@EXIT
                .endif
            .endif

            ; Retrieve path
            invoke  GetDlgItemText, [hwndDlg], ID_ADDFAV_PATH, ADDR fav_data_path, 512
            .if eax == 0
                invoke  MessageBox, [hwndDlg], ADDR input_path_error, ADDR app_name, MB_ICONEXCLAMATION
                jmp     @@EXIT
            .else
                invoke  instringi, ADDR fav_data_path, ADDR fav_header_check
                .if eax != -1
                    invoke  MessageBox, [hwndDlg], ADDR fav_path_invalid, ADDR app_name, MB_ICONSTOP
                    jmp     @@EXIT
                .else
                    invoke  StrCpy, ADDR favCheckBuf, ADDR fav_data_path
                    .if ! ((byte ptr [fav_data_path] >= 'a' && byte ptr [fav_data_path] <= 'z') || (byte ptr [fav_data_path] >= 'A' && byte ptr [fav_data_path] <= 'Z'))
                        invoke  GetAbsPathFromBase, ADDR favCheckBuf, ADDR fav_data_path
                    .endif
                    invoke  GetFileAttributes, ADDR favCheckBuf
                    .if eax == -1
                        invoke  MessageBox, [hwndDlg], ADDR host_path_invalid, ADDR app_name, MB_ICONSTOP
                        jmp     @@EXIT
                    .endif
                .endif
            .endif

            ; Retrieve "Directory Browsing" setting
            invoke  SendMessage, [hFavDirBrowsing], BM_GETCHECK, 0, 0
            .if eax == BST_CHECKED
                mov     [favDirBrowse], TRUE
            .else
                mov     [favDirBrowse], FALSE
            .endif

            ; Retrieve "Bind Address" setting
            invoke  SendMessage, [hFavBindAddress], WM_GETTEXT, SIZEOF fav_ip_bind_address, ADDR fav_ip_bind_address

            ; Retrieve port number
            invoke  GetDlgItemText, [hwndDlg], ID_ADDFAV_PORT, ADDR fav_data_port, 6
            .if eax != 0
                invoke  atodw, ADDR fav_data_port
                .if eax != 0 && eax <= 0FFFFh
                    invoke  EndDialog, [hwndDlg], 0
                .else
                    jmp     @@INVALID_PORT
                .endif
            .else
                mov     [fav_data_port], FALSE
                invoke  EndDialog, [hwndDlg], 0
            .endif
        .endif
        jmp     @@EXIT

    @@INVALID_PORT:
        invoke  MessageBox, [hwndDlg], ADDR invalid_port, ADDR app_name, MB_ICONSTOP
    .elseif [uMsg] == WM_CLOSE
    @@WM_CLOSE:
        invoke  EndDialog, [hwndDlg], -1
    .endif

  @@EXIT:
    xor     eax, eax
    ret
AddFavDialogProc endp

RemFavDialogProc proc hwndDlg:DWORD, uMsg:DWORD, wParam:DWORD, lParam:DWORD
LOCAL number_ok             :DWORD

    .if [uMsg] == WM_INITDIALOG
        invoke  LoadIcon, [hInstance], ID_MAIN_ICON
        push    eax
        invoke  SendMessage, [hwndDlg], WM_SETICON, ICON_BIG, eax
        pop     eax
        invoke  DestroyIcon, eax
        invoke  GetDlgItem, [hwndDlg], ID_REMFAV_INTEGER
        invoke  SetFocus, eax
    .elseif [uMsg] == WM_COMMAND
        .if [wParam] == IDCANCEL
            jz  @@WM_CLOSE
        .elseif [wParam] == IDOK
            invoke  GetDlgItemInt, [hwndDlg], ID_REMFAV_INTEGER, ADDR number_ok, 0
            .if [number_ok] != FALSE
                .if eax != 0 && eax <= 32
                    mov     [favRemoveNumber], eax
                    invoke  EndDialog, [hwndDlg], 0
                .else
                    invoke  MessageBox, [hwndDlg], ADDR favRemError, ADDR app_name, MB_ICONEXCLAMATION
                .endif
            .endif
        .endif
    .elseif [uMsg] == WM_CLOSE
    @@WM_CLOSE:
        invoke  EndDialog, [hwndDlg], -1
    .endif

    xor     eax, eax
    ret
RemFavDialogProc endp
