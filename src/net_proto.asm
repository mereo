; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

ConvertDottedIPStringToDw PROTO lpszIPString:DWORD
DirBrowseInsertHtmlBackLinks PROTO lpszDirBrowseLinks:DWORD
GetHostNameFromURL PROTO lpBuffer:DWORD, lpURL:DWORD
GetHostFromURL PROTO lpBuffer:DWORD, lpURL:DWORD
GetPortFromURL PROTO lpPort:DWORD,lpURL:DWORD
GetFilePathFromURL PROTO lpURL:DWORD, lpFileBuffer:DWORD
DownloadFile PROTO lpURL:DWORD, lpDestFile:DWORD, overwriteIfExists:DWORD
ecSend PROTO hSock:DWORD, pData:DWORD, lData:DWORD, pConnInfo:DWORD
