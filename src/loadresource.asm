; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

; Loads a resource from the resource section
;
; Parameters:
; hInstance = module handle
; resId     = resource id / string
; rsrcType  = resource type (ie. RCDATA, DIALOG etc)
; rsrcSize  = pointer to a DWORD that receives the resource size
;
; Return values:
; The return value is a pointer to the data of the resource. A value
; of FALSE will be returned on failure.
LoadRsrc proc uses esi edi hInst:DWORD, resId:DWORD, rsrcType:DWORD, pRsrcSize:DWORD
LOCAL hData                 :DWORD
LOCAL hRsrc                 :DWORD
LOCAL pData                 :DWORD
LOCAL rsrcSize              :DWORD

    mov     [pData], FALSE

    invoke  FindResource, [hInst], [resId], [rsrcType]
    mov     [hRsrc], eax

    .if [hRsrc] != FALSE
        invoke  LoadResource, [hInst], [hRsrc]
        mov     [hData], eax
        invoke  SizeofResource, [hInst], [hRsrc]
        mov     ecx, [pRsrcSize]
        mov     [ecx], eax
        mov     [rsrcSize], eax
        invoke  GlobalAlloc, GPTR, eax
        mov     [pData], eax

        ; If there was memory available at the computer then the
        ; resource data is copied into the memory, otherwise return FALSE.
        .if [pData] != FALSE
            invoke  LockResource, [hData]
            mov     esi, eax
            mov     edi, [pData]
            mov     ecx, [rsrcSize]
            cld
            rep     movsb
        .endif
    .endif

    mov     eax, [pData]
    ret
LoadRsrc endp
