; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

.386
.model flat, stdcall
option casemap:none

; Generic constants and defines for Windows API programming
include windows.inc

; Macro's
include macros.asm

; API Includes
incboth kernel32
incboth user32
incboth comdlg32
incboth comctl32
incboth gdi32
incboth shell32
incboth ole32
incboth advapi32
incboth ws2_32

; Functions and Macro's by MASM
incboth masm32

; Functions by Raymond Filiatreault
incboth fpu

; Include file for version info
include data_hdr.asm

; Include constants
include const_hdr.asm

; Function prototypes
include autostart_proto.asm
include cfg_proto.asm
include cmdline_proto.asm
include connectionhandler_proto.asm
include error_proto.asm
include favorites_proto.asm
include file_proto.asm
include loadresource_proto.asm
include log_proto.asm
include net_proto.asm
include preferences_proto.asm
include shell_proto.asm
include string_proto.asm
include tempfile_proto.asm
include updater_proto.asm
include utf8_proto.asm
include wndproc_proto.asm

.data
ArgB                        dd FALSE
ArgDE                       dd FALSE
ArgH                        dd FALSE
ArgHide                     dd FALSE

about_hyperlink             db WEBSITE_URL, 0
about_msg                   db "Mereo", CR, LF
                            db "Copyright (C) 2008, Jelle Geerts", 0
about_msg2                  db "Release date:", 9, MEREO_RELEASEDATE_STRING, CR, LF
                            db "Version:", 9, 9, "v", MEREO_VERSIONSTRING, 0
about_msg3                  db "This program is conditionally compliant to the specifications"
                            db " of HTTP/1.1. This means that all the `MUST' requirements"
                            db " specified in RFC2616 are met.", 0
about_thanksto              db "Thanks to:", CR, LF
                            db "All users and bug reporters.", 0
app_name                    db "Mereo", 0
autostart_done              db "New auto-start setting saved.", 0
autostart_error             db "The new auto-start setting could not be saved. Your privileges"
                            db " are probably limited to prevent registry changes.", 0
autostart_input_error       db "No '\' characters allowed in input.", 0
autostart_run               db "Software\Microsoft\Windows\CurrentVersion\Run", 0
autostart_runtemplate       db 34, "%s", 34, " %s", 0

backlinks_top               db "<a href=", 34, "/", 34, ">/</a>", 0
browse_title                db "Please select a folder to host", 0

; *'new_cfg_item-header'*
cfg_charset_header          db "Charset=", 0
cfg_ignore_errors           db 0
cfg_prefs_db_force_header   db "DirBrowseForce=", 0
cfg_prefs_logenable_header  db "LogEnable=", 0
cfg_prefs_maxthreads_header db "MaxThreads=", 0
cfg_prefs_logmem_header     db "LogMem=", 0
cfg_update_interval_header  db "UpdateInterval=", 0
cfg_update_atstart_header   db "UpdateCheckAtProgramStart=", 0
cfg_save_error              db "Could not save the 'mereo.cfg' settings file. Please verify that the file does not have the read-only"
                            db " attribute set and verify that you have access to the file.", CR, LF, CR, LF
                            db "Changed settings will have effect but are lost when exiting.", CR, LF, CR, LF
                            db "Do you want to continue (this decision will be remembered for the session)?", 0
cfgNoAccess                 db "The file 'mereo.cfg' could not be opened/created. This file is used to store your favorite hosted"
                            db " directories and other settings. Please verify you have access to this file and that it's not in u"
                            db "se by another program or service.", CR, LF, CR, LF, "Do you want to continue?", 0
cfgReadOnly                 db "The file 'mereo.cfg' had to be opened with read-only access. This probably happened because another"
                            db " instance of the program was already running.", CR, LF, CR, LF
                            db "The program will not be able to save settings when they're modified."
                            db CR, LF, CR, LF, "Do you want to continue?", 0
charset_standard            db "UTF-8", 0
clientlen                   dd SIZEOF sockaddr_in
CmdArgB                     db "-b", 0
CmdArgDE                    db "-de", 0
CmdArgH                     db "-h", 0
CmdArgHide                  db "-hide", 0
CmdArgP                     db "-p", 0
comp_name                   db "Computer Name:", 9, 9, "%s", 0
comp_ipaddress              db "Computer IP Address:", 9, "%s", 0
comp_noipaddress            db "failed retrieving IP Address", 0
content_length              db "Content-Length:", 0
content_type                db "Content Type", 0
crlf                        db CR, LF, 0

date_format                 db "ddd, dd MMM yyyy", 0
dir_browse_all              db "/*.*", 0
dir_browse_directories      db "<strong style=", 34, "color: #FF0000; font-size: 16px;", 34, ">Directories found:</strong><br /><br />"
                            db LF, "<table>", LF, 9, "<th>&nbsp;</th><th align=", 34, "left", 34, "><strong style=", 34, "font-"
                            db "size: 14px;", 34, ">Name</strong></th>", LF, 9, "<th align=", 34, "left", 34, "><strong style=", 34
                            db "font-size: 14px;", 34, ">Last-Modified</strong></th>", LF, 9, "<tr>", LF, 9, 9, "<td width=", 34
                            db "18", 34, "></td>", LF, "<td width=", 34, "250", 34, "><a href=", 34, "../", 34, ">../ Parent Directo"
                            db "ry</a></td>", LF, 9, 9, "<td width=", 34, "150", 34, ">-</td>", LF, 9, "</tr>", LF, 0
dir_browse_files            db "<strong style=", 34, "color: #FF0000; font-size: 16px;", 34, ">Files found:</strong><br /><br />", LF
                            db LF, "<table>", LF, 9, "<th>&nbsp;</th><th align=", 34, "left", 34, "><strong style=", 34, "font-"
                            db "size: 14px;", 34, ">Name</strong></th>", LF, 9, "<th align=", 34, "left", 34, "><strong style=", 34
                            db "font-size: 14px;", 34, ">Last-Modified</strong></th>", LF, 9, "<th align=", 34, "left", 34, "><stro"
                            db "ng style=", 34, "font-size: 14px;", 34, ">Size</strong></th>", LF
                            db 9, "<tr>", LF, 9, 9, "<td width=", 34, "18", 34, "></td>", LF, "<td width=", 34, "250", 34, ">"
                            db "<a href=", 34, "../", 34, ">../ Parent Directory</a></td>", LF
                            db 9, 9, "<td width=", 34, "150", 34, ">-</td>", LF, 9, 9, "<td width=", 34, "150", 34, ">-</td>", LF
                            db 9, "</tr>", LF, 0
dir_browse_page_part1_1     db "<!DOCTYPE html PUBLIC ", 34, "-//W3C//DTD XHTML 1.0 Transitional//"
                            db "EN", 34, " ", 34, "http://www.w3.org/TR/xhtml1/DTD/xh"
                            db "tml1-transitional.dtd", 34, ">", LF
                            db "<html xmlns=", 34, "http://www.w3.org/1999/xhtml", 34, ">", LF
                            db "<head>", LF
                            db "<meta http-equiv=", 34, "Content-Type", 34, " content=", 34, "text/html; charset=", 0
dir_browse_page_part1_2     db 34, " />", LF
                            db "<title>Contents of ", 0
dir_browse_page_part2       db "</title>", LF
                            db "<style type=", 34, "text/css", 34, ">", LF
                            db "<!--", LF
                            db "body", LF
                            db "{", LF
                            db 9, "color: #333333;", LF
                            db 9, "font-family: Courier New, Courier, monospace;", LF
                            db 9, "font-size: 12px;", LF
                            db "}", LF, LF
                            db "a, a:link, a:visited, a:active", LF
                            db "{", LF
                            db 9, "color: #0000FF;", LF
                            db "}", LF, LF
                            db "a:hover", LF
                            db "{", LF
                            db 9, "color: #FF6600;", LF
                            db 9, "text-decoration: none;", LF
                            db "}", LF
                            db "-->", LF
                            db "</style>", LF
                            db "</head>", LF, LF
                            db "<body>", LF
                            db "<strong style=", 34, "font-size: 24px;", 34, ">Contents of ", 0
dir_browse_page_part3       db "</strong>", LF
                            db "<br />", LF
                            db "<br />", LF
                            db 0
dir_browse_page_part4       db "<br />", LF
                            db "<br />", LF
                            db 0
dir_browse_page_part5       db "<br />", LF
                            db "<br />", LF
                            db "<hr />", LF
                            db "<div align=", 34, "right", 34, "><a href=", 34, WEBSITE_URL, 34, ">Mereo</a></div>", LF
                            db "</body>", LF,"</html>"
                            db 0
DownloadFile_template       db "%s\%s", 0
double_crlf                 db CR, LF, CR, LF, 0

EnvironmentTemp             db "TEMP", 0
EnvironmentUserProfile      db "USERPROFILE", 0
error_logging_disabled      db "Logging is disabled. You can enable it in the preferences (Settings -> Preferences...).", 0
error_page_filename_format  db ".\errorpages\%d.html", 0

fav_bind_address            db "BindAddress=", 0
fav_dirbrowse               db "DirectoryBrowsing=", 0
fav_header_check            db "[Favorite", 0
fav_invalid_name            db "Favorite name invalid, the following characters are not allowed:", CR, LF
                            db "    / [ ]", 0
fav_number                  db "[Favorite%lu]", 0
fav_path                    db "Path=", 0
fav_path_invalid            db "Webroot invalid, the string '[Favorite' is not allowed because it's used"
                            db " internally by the favorites parser function.", 0
fav_port                    db "Port=", 0
fav_name                    db "Name=", 0
favAbort                    db "Currently another directory is being hosted, do you want to stop it and host the selected favorite?", 0
favAbortFlag                dd 0
favExplanation1             db "The name can be anything without '=' characters.", 0
favExplanation2             db "Note: relative paths like '.\web' are possible too.", 0
favNext                     db "&Next", 0
favNoFavFileCreated         db "You can't add/remove favorites because the file 'mereo.cfg' could not be opened/created or no wri"
                            db "te access is allowed.", CR, LF, CR, LF, "Maybe you are using Mereo from a CD-ROM, if this is th"
                            db "e case, then you can only read and use the favorites (if any).", 0
favNotFound                 db "Favorite item not found, please verify that you've entered a valid number (i.e. the favorite item"
                            db " can be found in the menu).", CR, LF
                            db "Numbers are indicated in front of the favorite name in the 'Favorites' menu.", 0
favOK                       db "&OK", 0
favParseTemplate            db "Favorites parsing error in 'mereo.cfg' at favorite: %lu", 0
favRemError                 db "Please enter a number between 0-32.", 0
favTemplate                 db "%02d. %s", 0
favTooMuch                  db "There are too much favorites found (maximum is 32), not all items might appear in the list", 0

file_index_html             db "index.html", 0
file_index_htm              db "index.htm", 0
FileExt                     db "txt", 0
filesize_format             db "%lu", 0
filesize_b                  db " B", 0
filesize_kib                db " KiB", 0
filesize_mib                db " MiB", 0
filesize_gib                db " GiB", 0
FilterString                db "Text Documents (*.txt)", 0,"*.txt", 0,"All Files (*.*)", 0,"*.*", 0, 0

gThreadCount                dd 0

help_contact                db "mailto:", AUTHOR_EMAIL, "?Subject=Mereo", 0
help_contact_if_not_working db "If no mail program opened, you can send a mail to '", AUTHOR_EMAIL, "' manually.", 0
help_filename               db ".\help.html", 0
help_filenotfound           db "The help file (help.html) could not be find, please ensure you have the complete Mereo", CR, LF
                            db "package and not moved any of the installed files.", 0
help_nobrowser              db "There was no application found that is able to open the website.", 0
help_operation              db "open", 0
host_async_error            db "Synchronization selection error", 0
host_path_invalid           db "An incorrect path has been given, either the directory does not exist or no access is allowed.", 0
host_path_not_a_dir         db "The chosen path is not a directory, only directories can be hosted.", 0
host_bind_address_invalid   db "Requested bind address invalid", 0
host_socket_state_error     db "Socket state error", 0
host_socket_create_error    db "Socket creation error", 0
htm                         db ".htm", 0
html                        db ".html", 0
html_tr_tag_open            db 9, "<tr>", CR, CR, 0
html_img_dir                db 9, 9, "<td width=", 34, "18", 34, "><img src=", 34, "/mereo_icons/dir.png", 34, " alt=", 34, "D", 34, " /></td>", CR, LF, 0
html_img_file               db 9, 9, "<td width=", 34, "18", 34, "><img src=", 34, "/mereo_icons/file.png", 34, " alt=", 34, "F", 34, " /></td>", CR, LF, 0
html_td_a_tag_open          db 9, 9, "<td width=", 34, "250", 34, ">", "<a href=", 34, 0
html_a_tag_open             db "<a href=", 34, 0
html_a_tag_first_close      db 34,">", 0
html_a_tag_second_close     db "</a>", 0
html_td_a_tag_second_close  db "</a></td>", CR, LF, 9, 9, "<td width=", 34, "150", 34, ">", 0
html_td_tag_open            db 9, 9, "<td width=", 34, "150", 34, ">", 0
html_td_tag_close           db "</td>", CR, LF, 0
html_tdtr_tag_close         db "</td>", CR, LF, 9, "</tr>", CR, LF, 0
html_table_tag_close        db "</table>", CR, LF, CR, LF, 0
http_close                  db "close", 0
http_connection             db "connection:", 0
http_get                    db "GET", 0
http_keep_alive             db "keep-alive", 0

http_msg200                 db "HTTP/1.1 200 OK", CR, LF
                            db "Expires: 0", CR, LF
                            db "Last-Modified: %s", CR, LF
                            db "Accept-Ranges: bytes", CR, LF
                            db "Connection: keep-alive", CR, LF
                            db "Content-Type: %s", CR, LF
                            db "Content-Length: %s", CR, LF
                            db CR, LF, 0

http_msg200_dec             db "HTTP/1.1 200 OK", CR, LF
                            db "Expires: 0", CR, LF
                            db "Accept-Ranges: bytes", CR, LF
                            db "Connection: keep-alive", CR, LF
                            db "Content-Type: %s", CR, LF
                            db "Content-Length: %lu", CR, LF
                            db CR, LF, 0

http_msg206                 db "HTTP/1.1 206 Partial Content", CR, LF
                            db "Expires: 0", CR, LF
                            db "Last-Modified: %s", CR, LF
                            db "Accept-Ranges: bytes", CR, LF
                            db "Connection: keep-alive", CR, LF
                            db "Content-Type: %s", CR, LF
                            db "Content-Range: bytes %s-%s/%s", CR, LF
                            db "Content-Length: %s", CR, LF
                            db CR, LF, 0

http_msg301                 db "HTTP/1.1 301 Moved Permanently", CR, LF
                            db "Location: %s", CR, LF
                            db "Connection: keep-alive", CR, LF
                            db "Content-Length: 0", CR, LF
                            db CR, LF, 0

; Error pages
http_msg400                 db "HTTP/1.1 400 Bad Request", CR, LF
                            db "Expires: 0", CR, LF
                            db "Last-Modified: 0", CR, LF
                            db "Accept-Ranges: bytes", CR, LF
                            db "Connection: keep-alive", CR, LF
                            db "Content-Type: text/html", CR, LF
                            db "Content-Length: %lu", CR, LF
                            db CR, LF
                            db 0
http_msg400_page            db "<!DOCTYPE html PUBLIC ", 34, "-//W3C//DTD XHTML 1.0 Transitional//"
                            db "EN", 34, " ", 34, "http://www.w3.org/TR/xhtml1/DTD/xh"
                            db "tml1-transitional.dtd", 34, ">", LF
                            db "<html xmlns=", 34, "http://www.w3.org/1999/xhtml", 34, ">", LF
                            db "<head>", LF
                            db "<meta http-equiv=", 34, "Content-Type", 34, " content=", 34, "text/html; charset=ISO-8859-1", 34, " />", LF
                            db "<title>HTTP 400 STATUS - Bad Request</title>", LF
                            db "<style type=", 34, "text/css", 34, ">", LF
                            db "<!--", LF
                            db "body {", LF
                            db 9, "color: #333333;", LF
                            db 9, "font-family: Verdana, Arial, Helvetica, sans-serif;", LF
                            db 9, "font-size: 12px;", LF
                            db "}", LF
                            db "-->", LF
                            db "</style>", LF
                            db "</head>", LF, LF
                            db "<body>", LF
                            db "<strong>HTTP 400 STATUS: Bad Request</strong><br />", LF
                            db "<br />", LF
                            db "A bad request has been received. This is most likely an error in the browser software you are using.<br />", LF
                            db "You could try to look for an update of your browser if the error does not disappear.<br />", LF
                            db "<br />", LF
                            db "<br />", LF
                            db "<hr />", LF
                            db "<div align=", 34, "right", 34, "><a href=", 34, WEBSITE_URL, 34, ">Mereo</a></div>", LF
                            db "</body>", LF,"</html>"
                            db 0

http_msg403                 db "HTTP/1.1 403 Forbidden", CR, LF
                            db "Expires: 0", CR, LF
                            db "Last-Modified: 0", CR, LF
                            db "Accept-Ranges: bytes", CR, LF
                            db "Connection: keep-alive", CR, LF
                            db "Content-Type: text/html", CR, LF
                            db "Content-Length: %lu", CR, LF
                            db CR, LF
                            db 0
; It would require some extra code if we'd be using %s
; for "connection: %s" instead of defining two 403 messages,
; and since 'http_msg403_close' is pretty small I decided to
; make two variables.
http_msg403_close           db "HTTP/1.1 403 Forbidden", CR, LF
                            db "Connection: close", CR, LF
                            db "Content-Type: text/html", CR, LF
                            db CR, LF
                            db 0
http_msg403_page            db "<!DOCTYPE html PUBLIC ", 34, "-//W3C//DTD XHTML 1.0 Transitional//"
                            db "EN", 34, " ", 34, "http://www.w3.org/TR/xhtml1/DTD/xh"
                            db "tml1-transitional.dtd", 34, ">", LF
                            db "<html xmlns=", 34, "http://www.w3.org/1999/xhtml", 34, ">", LF
                            db "<head>", LF
                            db "<meta http-equiv=", 34, "Content-Type", 34, " content=", 34, "text/html; charset=ISO-8859-1", 34, " />", LF
                            db "<title>HTTP 403 STATUS - Forbidden</title>", LF
                            db "<style type=", 34, "text/css", 34, ">", LF
                            db "<!--", LF
                            db "body {", LF
                            db 9, "color: #333333;", LF
                            db 9, "font-family: Verdana, Arial, Helvetica, sans-serif;", LF
                            db 9, "font-size: 12px;", LF
                            db "}", LF
                            db "-->", LF
                            db "</style>", LF
                            db "</head>", LF, LF
                            db "<body>", LF
                            db "<strong>HTTP 403 STATUS: Forbidden</strong><br />", LF
                            db "<br />", LF
                            db "You are not allowed to access this resource.<br />", LF
                            db "<br />", LF
                            db "<br />", LF
                            db "<hr />", LF
                            db "<div align=", 34, "right", 34, "><a href=", 34, WEBSITE_URL, 34, ">Mereo</a></div>", LF
                            db "</body>", LF,"</html>"
                            db 0

http_msg404                 db "HTTP/1.1 404 Not Found", CR, LF
                            db "Expires: 0", CR, LF
                            db "Last-Modified: 0", CR, LF
                            db "Accept-Ranges: bytes", CR, LF
                            db "Connection: keep-alive", CR, LF
                            db "Content-Type: text/html", CR, LF
                            db "Content-Length: %lu", CR, LF
                            db CR, LF
                            db 0
http_msg404_page            db "<!DOCTYPE html PUBLIC ", 34, "-//W3C//DTD XHTML 1.0 Transitional//"
                            db "EN", 34, " ", 34, "http://www.w3.org/TR/xhtml1/DTD/xh"
                            db "tml1-transitional.dtd", 34, ">", LF
                            db "<html xmlns=", 34, "http://www.w3.org/1999/xhtml", 34, ">", LF
                            db "<head>", LF
                            db "<meta http-equiv=", 34, "Content-Type", 34, " content=", 34, "text/html; charset=ISO-8859-1", 34, " />", LF
                            db "<title>HTTP 404 STATUS - Not Found</title>", LF
                            db "<style type=", 34, "text/css", 34, ">", LF
                            db "<!--", LF
                            db "body {", LF
                            db 9, "color: #333333;", LF
                            db 9, "font-family: Verdana, Arial, Helvetica, sans-serif;", LF
                            db 9, "font-size: 12px;", LF
                            db "}", LF
                            db "-->", LF
                            db "</style>", LF
                            db "</head>", LF, LF
                            db "<body>", LF
                            db "<strong>HTTP 404 STATUS: Page Not Found</strong><br />", LF
                            db "<br />", LF
                            db "The URL does not exist, please ensure the link and spelling are correct.<br />", LF
                            db "<br />", LF
                            db "<br />", LF
                            db "<hr />", LF
                            db "<div align=", 34, "right", 34, "><a href=", 34, WEBSITE_URL, 34, ">Mereo</a></div>", LF
                            db "</body>", LF,"</html>"
                            db 0

http_msg500                 db "HTTP/1.1 500 Internal Server Error", CR, LF
                            db "Expires: 0", CR, LF
                            db "Last-Modified: 0", CR, LF
                            db "Accept-Ranges: bytes", CR, LF
                            db "Connection: keep-alive", CR, LF
                            db "Content-Type: text/html", CR, LF
                            db "Content-Length: %lu", CR, LF
                            db CR, LF
                            db 0
http_msg500_page            db "<!DOCTYPE html PUBLIC ", 34, "-//W3C//DTD XHTML 1.0 Transitional//"
                            db "EN", 34, " ", 34, "http://www.w3.org/TR/xhtml1/DTD/xh"
                            db "tml1-transitional.dtd", 34, ">", LF
                            db "<html xmlns=", 34, "http://www.w3.org/1999/xhtml", 34, ">", LF
                            db "<head>", LF
                            db "<meta http-equiv=", 34, "Content-Type", 34, " content=", 34, "text/html; charset=ISO-8859-1", 34, " />", LF
                            db "<title>HTTP 500 STATUS - Internal Server Error</title>", LF
                            db "<style type=", 34, "text/css", 34, ">", LF
                            db "<!--", LF
                            db "body {", LF
                            db 9, "color: #333333;", LF
                            db 9, "font-family: Verdana, Arial, Helvetica, sans-serif;", LF
                            db 9, "font-size: 12px;", LF
                            db "}", LF
                            db "-->", LF
                            db "</style>", LF
                            db "</head>", LF, LF
                            db "<body>", LF
                            db "<strong>HTTP 500 STATUS: Internal Server Error</strong><br />", LF
                            db "<br />", LF
                            db "The server had an internal error.<br />", LF
                            db "<br />", LF
                            db "<br />", LF
                            db "<hr />", LF
                            db "<div align=", 34, "right", 34, "><a href=", 34, WEBSITE_URL, 34, ">Mereo</a></div>", LF
                            db "</body>", LF,"</html>"
                            db 0

http_msg501                 db "HTTP/1.1 501 Not Implemented", CR, LF
                            db "Expires: 0", CR, LF
                            db "Last-Modified: 0", CR, LF
                            db "Accept-Ranges: bytes", CR, LF
                            db "Connection: keep-alive", CR, LF
                            db "Content-Type: text/html", CR, LF
                            db "Content-Length: %lu", CR, LF
                            db CR, LF
                            db 0
http_msg501_page            db "<!DOCTYPE html PUBLIC ", 34, "-//W3C//DTD XHTML 1.0 Transitional//"
                            db "EN", 34, " ", 34, "http://www.w3.org/TR/xhtml1/DTD/xh"
                            db "tml1-transitional.dtd", 34, ">", LF
                            db "<html xmlns=", 34, "http://www.w3.org/1999/xhtml", 34, ">", LF
                            db "<head>", LF
                            db "<meta http-equiv=", 34, "Content-Type", 34, " content=", 34, "text/html; charset=ISO-8859-1", 34, " />", LF
                            db "<title>HTTP 501 STATUS - Service Unavailable</title>", LF
                            db "<style type=", 34, "text/css", 34, ">", LF
                            db "<!--", LF
                            db "body {", LF
                            db 9, "color: #333333;", LF
                            db 9, "font-family: Verdana, Arial, Helvetica, sans-serif;", LF
                            db 9, "font-size: 12px;", LF
                            db "}", LF
                            db "-->", LF
                            db "</style>", LF
                            db "</head>", LF, LF
                            db "<body>", LF
                            db "<strong>HTTP 501 STATUS: Not Implemented</strong><br />", LF
                            db "<br />", LF
                            db "This server is not built for the type of received request.<br />", LF
                            db "<br />", LF
                            db "<br />", LF
                            db "<hr />", LF
                            db "<div align=", 34, "right", 34, "><a href=", 34, WEBSITE_URL, 34, ">Mereo</a></div>", LF
                            db "</body>", LF,"</html>"
                            db 0

; Not a customizable error page.
; Why? Simple, it would be inefficient to load a page from the harddisk
; when the server is under heavy load. Message 503 is used to let clients
; know that the server is very busy.
http_msg503                 db "HTTP/1.1 503 Service Unavailable", CR, LF
                            db "Expires: 0", CR, LF
                            db "Last-Modified: 0", CR, LF
                            db "Accept-Ranges: bytes", CR, LF
                            db "Connection: keep-alive", CR, LF
                            db "Content-Type: text/html", CR, LF
                            db "Content-Length: %lu", CR, LF
                            db "Retry-After: 60", CR, LF
                            db CR, LF
                            db 0
http_msg503_page            db "<!DOCTYPE html PUBLIC ", 34, "-//W3C//DTD XHTML 1.0 Transitional//"
                            db "EN", 34, " ", 34, "http://www.w3.org/TR/xhtml1/DTD/xh"
                            db "tml1-transitional.dtd", 34, ">", LF
                            db "<html xmlns=", 34, "http://www.w3.org/1999/xhtml", 34, ">", LF
                            db "<head>", LF
                            db "<meta http-equiv=", 34, "Content-Type", 34, " content=", 34, "text/html; charset=ISO-8859-1", 34, " />", LF
                            db "<title>HTTP 503 STATUS - Service Unavailable</title>", LF
                            db "<style type=", 34, "text/css", 34, ">", LF
                            db "<!--", LF
                            db "body {", LF
                            db 9, "color: #333333;", LF
                            db 9, "font-family: Verdana, Arial, Helvetica, sans-serif;", LF
                            db 9, "font-size: 12px;", LF
                            db "}", LF
                            db "-->", LF
                            db "</style>", LF
                            db "</head>", LF, LF
                            db "<body>", LF
                            db "<strong>HTTP 503 STATUS: Service Unavailable</strong><br />", LF
                            db "<br />", LF
                            db "The server is currently busy processing requests, please try again later.<br />", LF
                            db "<br />", LF
                            db "<br />", LF
                            db "<hr />", LF
                            db "<div align=", 34, "right", 34, "><a href=", 34, WEBSITE_URL, 34, ">Mereo</a></div>", LF
                            db "</body>", LF,"</html>"
                            db 0
http_msg503_size            equ $ - http_msg503_page - 1

http_msgGET                 db "GET %s HTTP/1.1", CR, LF
                            db "Host: %s", CR, LF
                            db "User-Agent: Mereo Updater", CR, LF
                            db "Accept: text/plain,*/*", CR, LF
                            db "Keep-Alive: 300", CR, LF
                            db "Connection: keep-alive", CR, LF
                            db CR, LF
                            db 0

HTTP_PORT                   dd 80
http_range                  db "range: bytes", 0
http_status200              db "200", 0

icons_path                  db "\mereo_icons\", 0
icons_dir                   db "dir.png", 0
icons_file                  db "file.png", 0
incompatible_os             db "Your Operating System is incompatible with the requested WinSock version,"
                            db " please check for an update of your Windows libraries.", 0
iniFilename                 db ".\mereo.cfg",0
input_error                 db "Please enter text...", 0
input_name_error            db "Please enter a name for the favorite (this will be displayed in the favorites menu).", 0
input_path_error            db "Please enter a webroot path.", 0
invalid_port                db "An invalid port value has been specified, a port number needs to be in range"
                            db " of 1 to 65535.", 0

logcleared                  db "Log successfully cleared.", 0
logfull                     db "** FULL, LOGGING STOPPED **", 0
logok                       db "OK", 0
logstatusmsg                db "Log memory status:", 9, "%s", 0

MainDialogHidden            dd FALSE
max_32_bits                 REAL8 4294967295.0
max_32_bits_plus1           REAL8 4294967296.0

no_memory                   db "Could not allocate memory.", 0

posix_dot_dirW              db 02eh, 00h, 00h, 00h
posix_dotdot_dirW           db 02eh, 00h, 2eh, 00h, 00h, 00h

; *'new_cfg_item-pref_var'*
prefs_dirbrowse_force       db 0 ; Forces showing contents of a directory regardless whether or not index.htm(l) exists.
                                 ; This setting can be changed in the 'Preferences...' under the 'Settings' menu.
prefs_logenable             db 1
prefs_logmem                dd LOGMEM
prefs_maxthreads            dw MAXTHREADS
prefs_update_atstart        db 1
prefs_updateinterval        db 0
progressbar_class           db "msctls_progress32", 0

root_openNotDirError        db "To prevent accidents, opening files is disallowed.", 0
root_openNotFound           db "File not found or access is denied to the resource.", 0

SavingLogError              db "Error while saving log file, no access.", 0
SavingLogSuccess            db "Log file saved successfully.", 0
settings_update_daily       db "Daily", 0
settings_update_weekly      db "Weekly", 0
settings_update_monthly     db "Monthly", 0
settings_update_never       db "Never", 0
settings_save_error         db "Couldn't save the settings, please verify access to the 'mereo.cfg' file.", 0
settings_save_e_maxthreads  db "An invalid number was entered for the 'maximum number of threads' setting.", 0
settings_save_e_mthreadlim  db "Maximum number of threads is 65535, please choose a lower value.", 0
settings_save_e_logmem      db "An invalid number was entered for the 'log memory' setting.", 0
settings_save_e_logmemlim   db "Maximum number of KiB to allocate for 'log memory' is 4294967295, please choose a lower value.", 0
sorry_caption               db "Sorry", 0
statusbar_class             db "msctls_statusbar32", 0
status_deftext              db "Mereo v", MEREO_VERSIONSTRING, 0
status_read_error           db "READ ERROR", 0
status_bRunning             db 0
status_bUpdateTimerRunning  db 0
status_running              db "Server running", 0
status_notrunning           db "Server not running", 0
status_conncount            db "%lu connections(s)", 0
status_parts                dd 85,85+110,-1
szComctl32                  db "comctl32", 0
szInitCommonControlsEx      db "InitCommonControlsEx", 0
szNull                      dd 0

taskbar_created_msg         db "TaskbarCreated", 0
time_format                 db "hh:mm:ss", 0
time_gmt                    db " GMT", 0
notarea_host                db "&Host", 0
notarea_restore             db "&Restore", 0
notarea_stophost            db "&Stop Server", 0
notarea_exit                db "E&xit", 0
true                        db "true", 0
type_html                   db "text/html", 0
type_text                   db "text/plain", 0
DateFormat                  db "ddddddddd',' MMM dd yyyy", 0
TimeFormat                  db " hh':'mm':'ss tt", 13, 10, 0
WhitespacePadding           db " ", 0

update_available            db "There is an update available for Mereo, do you want to download it?", CR, LF
                            db CR, LF, "If you want to review what changed, read 'changelog.txt'"
                            db " (included in the package).", CR, LF, CR, LF, "Download the update?", 0
update_busy                 db "The program is already checking for updates (this can happen at startup or regular intervals), please wait...", 0
update_check_question       db "The program will now check for updates, do you want to continue?", 0
update_check_no_updates     db "No updates are available at the time.", 0
update_done                 db "The update of Mereo has been downloaded to your desktop.", CR, LF, CR, LF
                            db "Please read the ChangeLog to review what's new (the ChangeLog can be found under the Start Menu shortcuts for 'Mereo').", 0
update_download_template    db "%s\mereo_%s.zip", 0
update_error_conn           db "A connection error occured with the remote host. Update not downloaded.", 0
update_error_hnf            db "The update could not be downloaded, please verify that you are connected to"
                            db " the Internet and that 'http://www.sourceforge.net/' can be reached.", 0
update_error_fnf            db "The update file could not be found on the server, please contact the author.", 0
update_error_fna            db "The update could not be downloaded, please verify that you have access"
                            db " to your desktop and that there is enough free space on your disk.", CR, LF
                            db "Also, please verify the update does not already exist on your desktop.", 0
update_error_mem            db "There is not enough memory available to download the update.", 0
update_fail                 db "The program could not contact the Mereo website, please check your connection.", 0
update_file                 db ".\version.txt",0
update_string               db MEREO_VERSIONSTRING, 0
update_url                  db WEBSITE_DOMAIN, "/update/version.txt", 0
update_url_template         db WEBSITE_DOMAIN, "/update/mereo_%s.zip", 0
update_versiontxt           db ".\version.txt", 0
UpdateAvailable             dd FALSE

verdana                     db "Verdana", 0

webroot_standard            db "C:\WEBROOT", 0

yousure_question            db "Are you sure?", 0

.data?
autostart_arguments         db 1024 dup (?)
autostart_runline           db 2048 dup (?)
autostart_tempbuf           db 2048 dup (?)

browse_buffer               db 2048 dup (?)
buffer_size                 dd ?

client                      sockaddr_in <?>
cmdline_ip_bind_address     dd ?
comp_name_buffer            db 128 dup (?)
comp_fullname_buffer        db 128 dup (?)
comp_fullipaddress_buffer   db 128 dup (?)
ConnInfoStructs             dd ?
CurrentDateTime             db 80 dup (?)

DownloadFile_host           db 512 dup (?)
DownloadFile_buffer         db 4096 dup (?)

EndAddress                  dd ?
error_page_filename_buffer  db 128 dup (?)

favCheckBuf                 db 2048 dup (?)
favDirBrowse                dd ?
iniFilenameBuf              db 2048 dup (?)
fav_data_name               db 64 dup (?)
fav_ip_bind_address         db 16 dup (?)
favMenuInfo                 MENUITEMINFO <?>
favReadOnly                 dd ?
favRemoveNumber             dd ?
favParseErrorBuffer         db 128 dup (?)
fav_data_path               db 512 dup (?)
fav_data_port               db 6 dup (?)
favTemp                     dd ?
favTempBuf                  db 2048 dup (?)

FilePath                    db 2048 dup (?)

GetAbsPathFromBase_temp     db 2048 dup (?)
GetAbsPathFromBase_temp2    db 2048 dup (?)

gCmdLine                    dd ? ; global commandline pointer
ghwndDlg                    dd ? ; global hwndDlg
gPID                        dd ? ; global process identifier

help_filebuffer             db 2048 dup (?)
http_msgBuffer              db 2048 dup (?)
http_logBuffer              dd ?

initcomctl                  INITCOMMONCONTROLSEX <?>

; Extra data for hyperlink processing
hHBrush                     dd ?
pt                          POINT <?>
lfont                       LOGFONT <?>
hAboutDlg                   dd ?
hFinger                     dd ?
ps                          PAINTSTRUCT <?>
hGUIFont                    dd ?
hLogFont                    dd ?
HyperlinkActive             dd ?

; Handles
hAutoStart                  dd ?
hAutoStartCurrent           dd ?
hAutoStartAll               dd ?
hBindAddress                dd ?
hDirBrowsing                dd ?
hFavBindAddress             dd ?
hFavDirBrowsing             dd ?
hIniFile                    dd ?
hItem                       dd ?
hKey                        dd ?
hMainIcon                   dd ?
hInstance                   dd ?
hLogDialog                  dd ?
hMenu                       dd ?
hPopupMenu                  dd ?
hReg                        dd ?
hServerSock                 dd ?
hStatus                     dd ?
host_path                   db 4096 dup (?)

int_ok                      dd ?

last_write_time             FILETIME <?>
logstatus                   db 128 dup (?)
logfullflag                 db ?

ofn                         OPENFILENAME <?>

pm_msg                      MSG <?>

rect                        RECT <?>
relativePath                db 2048 dup (?)
root_openBuffer             db 4096 dup (?)

serversock                  sockaddr_in <?>
status_buffer               db 128 dup (?)
str_charset                 db 256 dup (?)
system_time                 SYSTEMTIME <?>

taskbar_created_msg_code    dd ?
temp_thread_id              dd ?
tempvar                     dd ?

notarea_cursor_point        POINT <?>
notarea_icon_data           NOTIFYICONDATA2 <?>
notarea_item_info           MENUITEMINFO <?>
notarea_icon_rect           RECT <?>
notarea_icon_pt             POINT <?>
notarea_icon_timer_running  dd ?

updateDoneStatus            dd ?
updateMainThreadHandle      dd ?
updateTimerFirstMsg         dd ?
updateReadBuf               db 2048 dup (?)
update_url2                 db 2048 dup (?)
updateStopStatus            dd ?
updateTempBuf               db 2048 dup (?)
updateThreadHandle          dd ?
updateThreadStatus          dd ?

webroot_buffer              db 2048 dup (?)
wsadata                     WSADATA <?>

.code

; sub-components
include autostart.asm
include cfg.asm
include cmdline.asm
include connectionhandler.asm
include error.asm
include favorites.asm
include file.asm
include loadresource.asm
include log.asm
include macros.asm
include net.asm
include preferences.asm
include shell.asm
include string.asm
include tempfile.asm
include updater.asm
include utf8.asm
include wndproc.asm

program:
    ; Initialize common control styles
    invoke  GetVersion
    test    eax, 080000000h
    jz      @@INITCOMCTL_WINNT
    ; Win95 way
    invoke  InitCommonControls
    jmp     @@INITCOMCTL_DONE
  @@INITCOMCTL_WINNT:
    ; WinNT way
    mov     [initcomctl.dwSize], SIZEOF INITCOMMONCONTROLSEX
    mov     [initcomctl.dwICC], ICC_BAR_CLASSES or ICC_INTERNET_CLASSES or ICC_STANDARD_CLASSES or ICC_USEREX_CLASSES
    ; Get base addr for comctl32 (won't have to check for errors since the DLL
    ; will be loaded by the Windows Loader).
    invoke  GetModuleHandle, ADDR szComctl32
    invoke  GetProcAddress, eax, ADDR szInitCommonControlsEx
    .if eax != NULL
        push    OFFSET initcomctl
        call    eax
    .endif
  @@INITCOMCTL_DONE:

    ; ** Commandline parsing **
    invoke  GetCmdLineArgs
    mov     [gCmdLine], eax

    .if eax != 0
        invoke  GetCmdLineArgPtr, eax, ADDR CmdArgDE
        .if eax != -1
            mov     [ArgDE], TRUE
        .endif

        invoke  GetCmdLineArgPtr, [gCmdLine], ADDR CmdArgHide
        .if eax != -1
            mov     [ArgHide], TRUE
        .endif

        invoke  GetCmdLineArgString, [gCmdLine], ADDR CmdArgH
        .if eax != -1
            ; EAX now contains the index to the found string
            ; ECX now contains the length of the found string
            mov     [ArgH], TRUE
            push    eax
            push    eax
            inc     ecx ; Including NULL byte
            invoke  lstrcpyn, ADDR webroot_buffer, eax, ecx
            pop     eax
            mov     dl, byte ptr [eax]
            .if (byte ptr [eax+1] != ':') || (!((dl >= 'a' && dl <= 'z') || (dl >= 'A' && dl <= 'Z')))
                invoke  GetAbsPathFromBase, ADDR webroot_buffer, ADDR webroot_buffer
            .endif
            pop     eax
            invoke  GlobalFree, eax
        .endif

        invoke  GetCmdLineArgString, [gCmdLine], ADDR CmdArgB
        .if eax != -1
            mov     [ArgB], TRUE
            mov     [cmdline_ip_bind_address], eax
        .endif

        invoke  GetCmdLineArgInt, [gCmdLine], ADDR CmdArgP
        .if eax != -1 && eax > 0 && eax <= 0FFFFh ; Valid port?
            mov     [HTTP_PORT], eax ; Then use it :)
        .endif
    .endif
    ; ** Commandline parsing done **

    invoke  GetCurrentProcessId
    mov     [gPID], eax

    ; Initialize variables
    mov     [hLogDialog], 0
    mov     [notarea_icon_timer_running], 0

    ; Start program
    invoke  CoInitialize, 0
    invoke  WSAStartup, WSA_REQ_VERSION, ADDR wsadata
    test    eax, eax
    jnz     @@INCOMPATIBLEOS

    cmp     [wsadata.wVersion], WSA_REQ_VERSION
    jb      @@INCOMPATIBLEOS

    ; Get module handle (base address of executable in memory)
    invoke  GetModuleHandle, 0
    mov     [hInstance], eax

    ; Load the main icon for the
    invoke  LoadIcon, [hInstance], ID_MAIN_ICON
    mov     [hMainIcon], eax

    ; Open the configuration file 'mereo.cfg'
    invoke  GetAbsPathFromBase, ADDR iniFilenameBuf, ADDR iniFilename
    invoke  GetFileAttributes, ADDR iniFilenameBuf
    .if eax != -1
        and     eax, FILE_ATTRIBUTE_READONLY ; Leave FILE_ATTRIBUTE_READONLY bits
        ; Actually the whole check could be done by 'test eax, FILE_ATTR.....' and then 'jnz XXX' but who knows
        ; if Microsoft is going to change the value for FILE_ATTRIBUTE_READONLY to more than 1 bits set to 1
        .if eax == FILE_ATTRIBUTE_READONLY ; Check if all of the bits which define FILE_ATTRIBUTE_READONLY were set or not
            ; If readonly, then read with READ access (instead of READ + WRITE)
            mov     [favReadOnly], TRUE
            invoke  CreateFile, ADDR iniFilenameBuf, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0
        .else
            invoke  CreateFile, ADDR iniFilenameBuf, GENERIC_READ or GENERIC_WRITE, FILE_SHARE_READ, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0
            .if eax == -1
                invoke  CreateFile, ADDR iniFilenameBuf, GENERIC_READ, FILE_SHARE_READ or FILE_SHARE_WRITE, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0
                .if eax != -1
                    push    eax
                    invoke  MessageBox, 0, ADDR cfgReadOnly, ADDR app_name, MB_ICONEXCLAMATION or MB_YESNO
                    cmp     eax, IDNO
                    pop     eax
                    jz      @@CLEANUP_AND_EXIT
                .endif
            .endif
        .endif ; Else, read with READ + WRITE access
    .else
        invoke  CreateFile, ADDR iniFilenameBuf, GENERIC_READ or GENERIC_WRITE, FILE_SHARE_READ or FILE_SHARE_WRITE, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0
    .endif
    .if eax == -1
        invoke  MessageBox, 0, ADDR cfgNoAccess, ADDR app_name, MB_ICONEXCLAMATION or MB_YESNO
        cmp     eax, IDNO
        jz      @@CLEANUP_AND_EXIT
        mov     [hIniFile], -1
    .else
        mov     [hIniFile], eax
    .endif

    ; Parse the 'update interval' setting
    invoke  cfg_ParseInteger, [hIniFile], ADDR cfg_update_interval_header
    .if eax != -1
        mov     [prefs_updateinterval], al
    .endif

    ; Parse the 'maximum number of threads' setting
    invoke  cfg_ParseInteger, [hIniFile], ADDR cfg_prefs_maxthreads_header
    .if eax != -1
        mov     [prefs_maxthreads], ax
    .endif
    .if [prefs_maxthreads] == 1
        invoke  Log_AllocateMemory
    .endif

    ; Get memory for ConnInfo structures
    ; Memory to allocate = (SIZEOF ConnInfo * prefs_maxthreads)
    movzx   ecx, [prefs_maxthreads]
    mov     eax, (SIZEOF ConnInfo)
    mul     ecx ; EAX = EAX * ECX
    mov     [tempvar], eax
    ; END OF (SIZEOF ConnInfo * eax)
    invoke  GlobalAlloc, GMEM_ZEROINIT, eax
    test    eax, eax
    jz      @@NO_MEMORY
    mov     [ConnInfoStructs], eax
    add     eax, [tempvar]
    mov     [EndAddress], eax

    ; Allocate memory for the log buffer (if logging is enabled)
    invoke  cfg_ParseInteger, [hIniFile], ADDR cfg_prefs_logenable_header
    .if eax != -1
        mov     [prefs_logenable], al
    .endif
    .if [prefs_logenable] == 1
        invoke  Log_AllocateMemory
    .endif

    ; Check if updates are available for Mereo (*** spawns a thread ***)
    invoke  cfg_ParseInteger, [hIniFile], ADDR cfg_update_atstart_header
    .if eax != -1
        mov     [prefs_update_atstart], al
    .endif
    .if [prefs_update_atstart] == 1
        invoke  CheckForProgramUpdates, 0, UPDATE_CHECK_NO_NOTIFY, 0, 0
    .endif

    ; Startup main dialog
    invoke  DialogBoxParam, [hInstance], ID_MAIN_DIALOG, 0, ADDR MainDialogProc, 0

    ; Uninitialize and cleanup
    @@CLEANUP_AND_EXIT:
    invoke  WSACleanup
    invoke  CoUninitialize
    jmp     @@EXITPROCESS

    @@NO_MEMORY:
    invoke  MessageBox, 0, ADDR no_memory, ADDR sorry_caption, MB_ICONSTOP
    jmp     @@EXITPROCESS

    @@INCOMPATIBLEOS:
    invoke  MessageBox, 0, ADDR incompatible_os, ADDR sorry_caption, MB_ICONSTOP

    ; Exit the program
    @@EXITPROCESS:
    .if [updateMainThreadHandle] != 0
        .while TRUE
            invoke  GetExitCodeThread, [updateMainThreadHandle], ADDR updateThreadStatus
            .if eax == 0 || [updateThreadStatus] != STILL_ACTIVE
                .break
            .else
                invoke  Sleep, 10
            .endif
        .endw
    .endif

    ; Free log memory
    .if [prefs_logenable] == 1
        invoke  Log_FreeMemory
    .endif

    ; Free thread memory
    invoke  GlobalFree, [ConnInfoStructs]

    ; Close configuration file handle
    .if [hIniFile] != -1
        invoke  CloseHandle, [hIniFile]
    .endif

    invoke  DestroyIcon, [hMainIcon]
    invoke  ExitProcess, 0
end program
