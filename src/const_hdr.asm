; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

NIM_ADD                             equ 000h ;Adds an icon to the taskbar status area.
NIM_MODIFY                          equ 001h ;Changes the icon, tooltip text, or notification message identifier for an icon in the taskbar status area.
NIM_DELETE                          equ 002h ;Deletes an icon from the taskbar status area.

TTF_IDISHWND                        equ 001h ;Specifies to use custom window handle in uId for TOOLINFO struct.

TTI_NONE                            equ 000h ;No icon (Icon for a balloon tooltip)
TTI_INFO                            equ 001h ;Information icon (Icon for a balloon tooltip)
TTI_WARNING                         equ 002h ;Warning icon (Icon for a balloon tooltip)
TTI_ERROR                           equ 003h ;Error icon (Icon for a balloon tooltip)

TTS_ALWAYSTIP                       equ 001h ;The ToolTip will appear when the cursor is on a tool, regardless of whether the ToolTip control's owner window is active or inactive.
TTS_NOPREFIX                        equ 002h ;Prevents the system from stripping the ampersand (&) character from a string.
TTS_BALLOON                         equ 040h ;The ToolTip control has the appearance of a cartoon "balloon", with rounded corners and a stem pointing to the item.

NIF_MESSAGE                         equ 001h ;The uCallbackMessage member is valid.
NIF_ICON                            equ 002h ;The hIcon member is valid.
NIF_TIP                             equ 004h ;The szTip member is valid.
NIF_INFO                            equ 010h ;Use balloon tip instead of a standard tooltip. The szInfo, uTimeout, szInfoTitle, and dwInfoFlags members are valid.

NIIF_NONE                           equ 000h ;No icon.
NIIF_INFO                           equ 001h ;An information icon.
NIIF_WARNING                        equ 002h ;A warning icon.
NIIF_ERROR                          equ 003h ;An error icon.
NIIF_USER                           equ 004h ;Version 6.0. Use the icon specified in NOTIFYICONDATA.hIcon
NIIF_QUESTION                       equ 006h ;A question icon. (Don't know if NIIF_QUESTION is the right name)
NIIF_NOSOUND                        equ 010h ;Version 6.0. Do not play the associated sound. Applies only to balloon ToolTips.

NOTIFYICONDATA2 STRUCT
; NOTIFYICONDATA in windows.inc is old, here is new
; (same as in you can see in MFC apps) now we can make balloon tooltips.
; I've called this structure NOTIFYICONDATA2 otherwise MASM assembler
; would give the error 'structure redifinition'.
    cbSize              dd ?
    hWnd                dd ?
    uID                 dd ?
    uFlags              dd ?
    uCallbackMessage    dd ?
    hIcon               dd ?
    szTip               db 128 dup(?)
    dwState             dd ?
    dwStateMask         dd ?
    szInfo              db 256 dup(?)
    union ;Union means both names 'uTimeout' and 'uVersion' refer to the same memory location
        uTimeout            dd ?
        uVersion            dd ?
    ends
    szInfoTitle         db 64 dup(?)
    dwInfoFlags         dd ?
NOTIFYICONDATA2 ENDS

; Constants for API's or Win32 Structures
BIF_BROWSEINCLUDEURLS               equ 80h
BIF_EDITBOX                         equ 10h
BIF_NEWDIALOGSTYLE                  equ 40h
BIF_RETURNONLYFSDIRS                equ 1
BIF_USENEWUI                        equ BIF_NEWDIALOGSTYLE or BIF_EDITBOX

ICC_STANDARD_CLASSES                equ 4000h
ICC_NATIVEFNTCTL_CLASS              equ 2000h
ICC_BAR_CLASSES                     equ 4

LOCALE_SYSTEM_DEFAULT               equ 800h

CP_UTF8                             equ 65001

; Constants for the window elements
ID_MAIN_DIALOG                      equ 100
ID_MAIN_MENU                        equ 101
ID_MAIN_ICON                        equ 102
IDE_ROOT                            equ 103
IDC_DIRBROWSING                     equ 104
IDM_HOST                            equ 105
IDM_STOPHOST                        equ 106
IDM_SELECTDIRFILE                   equ 107
IDM_EXIT                            equ 108
IDM_HELP                            equ 109
IDM_CONTACT                         equ 110
IDM_ABOUT                           equ 111
IDT_COMPNAME                        equ 112
IDT_COMPIPADDRESS                   equ 113
ID_NOTAREA_ICON_STOPPED             equ 114
ID_NOTAREA_ICON_RUNNING             equ 115
ID_MAIN_PORT                        equ 116
IDM_VIEWLOG                         equ 117
IDM_SAVELOG                         equ 118
IDC_AUTOSTART                       equ 119
IDC_AUTOSTART_CURRENT               equ 120
IDC_AUTOSTART_ALL                   equ 121
ID_OPEN_WEBROOT                     equ 122
ID_MAIN_CHARSET                     equ 123
IDM_CLEARLOG                        equ 124
IDE_BIND_ADDRESS                    equ 125
ID_BIND_ADDRESS_CLR                 equ 126

ID_ABOUT_DIALOG                     equ 200
ID_ABOUT_TEXT                       equ 201
ID_ABOUT_TEXT2                      equ 202
ID_ABOUT_TEXT3                      equ 203
ID_ABOUT_TEXT4                      equ 204
ID_ABOUT_ICON                       equ 205

ID_LOG_DIALOG                       equ 300
ID_LOG_TEXT                         equ 301
ID_HYPERLINK                        equ 302
IDM_ADDFAV                          equ 303
IDM_REMFAV                          equ 304
IDT_LOG_STATUS                      equ 305

IDM_FAV                             equ 306
IDM_MAXFAV                          equ 338

ID_ADDFAV_DIALOG                    equ 400
ID_ADDFAV_NAME                      equ 401
ID_ADDFAV_PATH                      equ 402
ID_ADDFAV_PORT                      equ 403
ID_ADDFAV_FINISH                    equ 404
ID_ADDFAV_QUESTION                  equ 405
ID_ADDFAV_BROWSE                    equ 406
IDE_ADDFAV_BIND_ADDRESS             equ 407

ID_REMFAV_DIALOG                    equ 500
ID_REMFAV_INTEGER                   equ 501

ID_UPDATE_DIALOG                    equ 600
ID_UPDATE_STOP                      equ 601

ID_AUTOSTART_DIALOG                 equ 700
ID_AUTOSTART_PATH                   equ 701
ID_AUTOSTART_BROWSE                 equ 702
ID_AUTOSTART_PORT                   equ 703
ID_AUTOSTART_FINISH                 equ 704
IDC_AUTOSTART_DIRBROWSING           equ 705
IDC_AUTOSTART_HIDDEN                equ 706
IDE_AUTOSTART_BIND_ADDRESS          equ 707

ID_SETTINGS_UPDATE_DIALOG           equ 800
ID_SETTINGS_UPDATE_INTERVAL         equ 801
IDM_SETTINGS_UPDATE                 equ 802
ID_SETTINGS_UPDATE_APPLY            equ 803
IDM_UPDATE_CHECK                    equ 804
IDC_SETTINGS_UPDATE_ATSTART         equ 805
IDM_SETTINGS_PREFS                  equ 806
ID_SETTINGS_PREFS_DIALOG            equ 807
ID_SETTINGS_PREFS_APPLY             equ 808
IDC_SETTINGS_DIRBROWSE_FORCE        equ 809
IDC_SETTINGS_LOGENABLE              equ 810
IDE_SETTINGS_MAXTHREADS             equ 811
IDE_SETTINGS_LOGMEM                 equ 812
; *'new_cfg_item-widget_id'*

IDM_NOTAREA_EXIT                    equ 900

ID_IMG_HTTP_DB_DIR                  equ 1000
ID_IMG_HTTP_DB_FILE                 equ 1001

; Extra components of main window
ID_STATUSBAR                        equ 4000
ID_TIMER1                           equ 4001
ID_TIMER2                           equ 4002
ID_NOTAREA_ICON_MSG                 equ 4003

; Extra components of update window
ID_UPDATE_TIMER                     equ 5000
ID_UPDATE_CHECK_TIMER               equ 5001

; Notification area icon ID numbers for notification area icon menu
IDM_RESTORE                         equ 6000

; Contants for the window messages & notification area icon etc
WM_SOCKET                           equ WM_APP+1
WM_NOTAREA_ICON                     equ WM_APP+2
WM_LOG_READ_ERROR                   equ WM_USER+3
WM_UPDATE_NOTIFY                    equ WM_USER+4
UPDATE_CHECK_NOTIFY                 equ WM_TIMER
UPDATE_CHECK_NO_NOTIFY              equ WM_TIMER+1

; General constants
CR                                  equ 0Dh
LF                                  equ 0Ah

; Custom constants
WSA_REQ_VERSION                     equ 2
ONE_KILOBYTE                        equ 1024
ONE_MEGABYTE                        equ 1048576
ONE_GIGABYTE                        equ 1073741824
CONTENT_DIRECTORIES                 equ 1
CONTENT_FILES                       equ 2
LOGMEM                              equ (ONE_MEGABYTE*10)
LOGMEM_LIMIT                        equ 0FFFFFFFFh ; *** when changing this, update the 'settings_save_e_logmemlim' error message too ***
LOGMEM_LINELIMIT                    equ 256 ; The maximum amount one line in the log file can take.
ERRORPAGE_MAX_LENGTH                equ 4096
HANDLECONNECTIONTHREAD_STACKSIZE    equ 32768
MAXTHREADS                          equ 1000
MAXTHREADS_LIMIT                    equ 0FFFFh ; *** when changing this, update the 'settings_save_e_mthreadlim' error message too ***
PATH_BUFFER_WIDE_LEN                equ 12288

; Connection related
SOCK_STREAM_SEND_SIZE               equ 8192
HTTP_DEFAULT_PORT                   equ 80

ConnInfo struct
    hSock               dd ? ; handle to socket
    hThread             dd ? ; handle to thread
    hEventStart         dd ? ; handle to event
    WaitingStatus       dd ? ; yes/no
    ThreadRunning       dd ? ; yes/no
    RecvData            dd ? ; yes/no
    fd_close            dd ? ; yes/no
    kaTimer             dd ? ; 0-300
ConnInfo ends
