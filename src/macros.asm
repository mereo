; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

incboth MACRO incfile
    include     incfile.inc
    includelib  incfile.lib
ENDM

; Converts an IP address (taken as DWORD value) to four arguments that are
; pushed on the stack, the last octet is pushed as first argument (and so on).
PushIPOctets MACRO dwIp
    mov     eax, [dwIp]
    movzx   ecx, al ; extract last octet
    push    ecx     ; push argument
    movzx   ecx, ah ; extract third octet
    push    ecx     ; push argument
    shr     eax, 16 ; shift in the next 2 octets
    movzx   ecx, al ; extract second octet
    push    ecx     ; push argument
    movzx   ecx, ah ; extract first octet
    push    ecx     ; push argument
ENDM
