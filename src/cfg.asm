; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

; Ask the user if errors should be ignored.
; Returns 1 if the error should be ignored, 0 if not.
cfg_CheckIgnoreError proc hwndDlg:DWORD
    ; Check if the user already answered the question.
    .if [cfg_ignore_errors] == 0
        invoke  MessageBox, [hwndDlg], ADDR cfg_save_error, ADDR app_name, MB_ICONEXCLAMATION or MB_YESNO
        ; Did the user want to ignore the error?
        .if eax != IDYES
            ; User didn't want to ignore the error.
            xor     eax, eax
        .else
            ; Ignore the error and remember to ignore these errors for this session.
            mov     [cfg_ignore_errors], 1
        .endif
    .endif
    .if [cfg_ignore_errors] == 1
        ; Ignore the error (user already confirmed that errors should be ignored).
        xor     eax, eax
        inc     eax
    .endif
    ret
cfg_CheckIgnoreError endp

; Sets the current file pointer for the configuration file and retrieves the current
; file data and returns it in EAX, the current filesize is returned in ECX.
; Returns 0 on error (no more memory available for allocation or file access denied)
;
; Check GetLastError() for code 1 or 2
; 1 means no more memory could be allocated
; 2 means file access was denied
cfg_GetSizeSetPtr proc hFile:DWORD
LOCAL pMem:DWORD, iniFileSize:DWORD, numb:DWORD

    invoke  SetLastError, 2
    cmp     [hIniFile], -1
    jz      @@ERROR_RET
    ; get current filesize of the ini file
    invoke  GetFileSize, [hFile], 0
    cmp     eax, -1
    jz      @@FILE_ERROR
    mov     [iniFileSize], eax
    add     eax, 4096 ; max size of new value to be added
    invoke  GlobalAlloc, GPTR, eax
    or      eax, eax
    jz      @@MEM_ERROR
    mov     [pMem], eax
    invoke  SetFilePointer, [hFile], 0, 0, FILE_BEGIN
    invoke  ReadFile, [hFile], [pMem], [iniFileSize], ADDR numb, 0
    or      eax, eax
    jz      @@FILE_ERROR

    mov     eax, [pMem]
    mov     ecx, [iniFileSize]
    ret

  @@MEM_ERROR:
    invoke  SetLastError, 1
    jmp     @@ERROR_RET

  @@FILE_ERROR:
    invoke  SetLastError, 2
    ;jmp     @@ERROR_RET

  @@ERROR_RET:
    xor     eax, eax
    ret
cfg_GetSizeSetPtr endp

; Parses a value from the configuration file
;
; Returns   EAX = memory pointer
;           ECX = value string pointer
;
; Returns EAX = 0 on failure meaning the file could not be accessed, no more memory
; could be allocated, or the configuration file didn't contain a charset header
cfg_ParseString proc uses edi hFile:DWORD, lpszValueHeader:DWORD
LOCAL pMem:DWORD, iniFileSize:DWORD

    invoke  cfg_GetSizeSetPtr, [hFile]
    or      eax, eax
    jnz     @F
    ret
  @@:
    mov     [pMem], eax
    mov     [iniFileSize], ecx

    invoke  instringi, [pMem], [lpszValueHeader]
    .if eax == -1
        xor     eax, eax
        ret
    .endif

    add     eax, [pMem]
    push    eax
    invoke  StrLen, [lpszValueHeader]
    pop     ecx
    add     eax, ecx
    mov     edi, eax
    invoke  instringi, eax, ADDR crlf
    .if eax == -1
        jmp     @@RET
    .else
        add     eax, edi
        mov     byte ptr [eax], 0
    .endif

  @@RET:
    mov     ecx, edi
    mov     eax, [pMem]
    ret
cfg_ParseString endp

; Saves value to the configuration file
; Returns TRUE on success or FALSE on error (no more memory available for allocation or file access denied)
;
; Check GetLastError() for code 1 or 2
; 1 means no more memory could be allocated
; 2 means file access was denied
cfg_SaveString proc uses ebx esi edi hwndDlg:DWORD, hFile:DWORD, lpszValueHeader:DWORD, lpszValue:DWORD
LOCAL pMem:DWORD, pSecondPart:DWORD, SecondPartLength:DWORD, numb:DWORD

    invoke  cfg_GetSizeSetPtr, [hFile]
    or      eax, eax
    jnz     @F
    ret
  @@:
    mov     [pMem], eax

    mov     edi, [pMem]
    ; check if value header is found
    invoke  instringi, edi, [lpszValueHeader]
    .if eax == -1
        invoke  StrLen, [pMem]
        mov     esi, [pMem]
        jmp     @@CREATE_HEADER
    .endif

    ; ========================================================================================
    ; =  Now put away the contents of the file after the value                               =
    ; ========================================================================================
    mov     esi, edi
    add     esi, eax
    ; search for CR LF (go to end of the value header)
    invoke  instringi, esi, ADDR crlf
    .if eax != -1           ; check if there was a CR LF sequence found
        mov     ebx, esi    ; save the position of the start of the line
        ; move the rest of the file to the start of the line (delete the line)
        mov     edi, esi    ; position of the start of the line
        mov     esi, eax    ; position of CR LF
        add     esi, edi    ; add position of the start of the line
        add     esi, 2      ; skip the CR LF
        invoke  StrLen, esi
        mov     ecx, eax
        ; cld
        rep     movsb
        mov     byte ptr [edi], 0
        mov     esi, ebx    ; restore the position of the start of the line
    .else
        mov     byte ptr [esi], 0
    .endif

  @@CREATE_HEADER:
    ; Put the lines after the header 4kB away further to make sure they do
    ; not get overwritten while creating the new value header.
    mov     ebx, esi
    mov     edi, esi
    add     edi, 4095 ; reserve one byte for NULL
    mov     [pSecondPart], edi
    invoke  StrLen, esi
    mov     ecx, eax
    mov     [SecondPartLength], ecx
    ; cld
    rep     movsb
    mov     byte ptr [edi], 0
    mov     edi, ebx

    ; write the new header to the current line
    invoke  StrCpy, edi, [lpszValueHeader]
    invoke  StrLen, [lpszValueHeader]
    add     edi, eax
    invoke  StrCpy, edi, [lpszValue]

    ; put back the second part of the configuration file
    invoke  StrLen, edi
    add     edi, eax
    mov     byte ptr [edi], CR
    inc     edi
    mov     byte ptr [edi], LF
    inc     edi
    mov     ecx, [SecondPartLength]
    mov     esi, [pSecondPart]
    ; cld
    rep     movsb
    mov     byte ptr [edi], 0

    ; write the new information to the file
    invoke  SetFilePointer, [hFile], 0, 0, FILE_BEGIN
    invoke  WriteFile, [hFile], [pMem], 1, ADDR numb, 0 ; write a dummy byte, quick check to see if still acccess to file
    or      eax, eax
    jz      @@FILE_ERROR                                ; but if no access, than we see it now, rather than first truncating file :)
    invoke  SetFilePointer, [hFile], 0, 0, FILE_BEGIN
    invoke  SetEndOfFile, [hFile]
    invoke  StrLen, [pMem]
    lea     edx, [numb]
    invoke  WriteFile, [hFile], [pMem], eax, edx, 0
    invoke  GlobalFree, [pMem]
    xor     eax, eax
    inc     eax
    ret

  @@FILE_ERROR:
    push    eax
    ; Check if errors should be ignored (ask the user).
    invoke  cfg_CheckIgnoreError, [hwndDlg]
    or      eax, eax
    pop     eax
    jz      @@FILE_ERROR_RET ; Error not ignored.
    ; User wants to ignore the error.
    xor     eax, eax
    inc     eax
  @@FILE_ERROR_RET:
    ret
cfg_SaveString endp

; Parses an integer from the configuration file
;
; Returns   EAX = integer
;
; Returns EAX = -1 on failure meaning the file could not be accessed, no more memory
; could be allocated, or the configuration file didn't contain the header.
cfg_ParseInteger proc uses edi hFile:DWORD, lpszValueHeader:DWORD
LOCAL pMem:DWORD, integer:DWORD

    invoke  cfg_GetSizeSetPtr, [hFile]
    or      eax, eax
    jnz     @F
    mov     eax, -1
    ret
  @@:
    mov     [pMem], eax

    invoke  instringi, [pMem], [lpszValueHeader]
    .if eax == -1
        xor     eax, eax
        dec     eax
        ret
    .endif

    add     eax, [pMem]
    push    eax
    invoke  StrLen, [lpszValueHeader]
    pop     ecx
    add     eax, ecx
    mov     edi, eax
    invoke  instringi, eax, ADDR crlf
    .if eax != -1
        mov     byte ptr [edi+eax], 0
    .endif

    invoke  atodw, edi
    mov     [integer], eax

  @@RET:
    invoke  GlobalFree, [pMem]
    mov     eax, [integer]
    ret
cfg_ParseInteger endp

; Saves an integer to the configuration file
; Returns TRUE on success or FALSE on error (no more memory available for allocation or file access denied)
;
; Check GetLastError() for code 1 or 2
; 1 means no more memory could be allocated
; 2 means file access was denied
cfg_SaveInteger proc uses ebx esi edi hwndDlg:DWORD, hFile:DWORD, lpszValueHeader:DWORD, integer:DWORD
LOCAL pMem:DWORD, pSecondPart:DWORD, SecondPartLength:DWORD, save_integer[16]:BYTE, numb:DWORD

    invoke  cfg_GetSizeSetPtr, [hFile]
    or      eax, eax
    jnz     @F
    ret
  @@:
    mov     [pMem], eax

    mov     edi, [pMem]
    ; check if value header is found
    invoke  instringi, edi, [lpszValueHeader]
    .if eax == -1
        jmp     @@CREATE_HEADER
    .endif

    ; search for CR LF (go to end of value header)
    lea     eax, [eax+edi]
    mov     esi, eax
    mov     ebx, esi
    invoke  instringi, eax, ADDR crlf
    .if eax != -1
        ; clear out the line from the CR LF sequence to the beginning of the line
        mov     edi, eax
        add     edi, esi
        add     edi, 2

        ; copy the rest of the file to start of the value header the line
        mov     esi, edi
        invoke  StrLen, edi ; get the length of the rest of the file after the value header line
        mov     ecx, eax
        mov     edi, ebx
        rep     movsb
        mov     byte ptr [edi], 0
    .endif

  @@CREATE_HEADER:
    ; Put the rest of the file 4kB away further to make sure they do
    ; not get overwritten while creating the new value header.
    mov     esi, [pMem]
    mov     ebx, esi
    mov     edi, esi
    add     edi, 4095 ; reserve one byte for NULL
    mov     [pSecondPart], edi
    invoke  StrLen, esi
    mov     ecx, eax
    mov     [SecondPartLength], ecx
    ; cld
    rep     movsb
    mov     byte ptr [edi], 0
    mov     edi, ebx

    ; write the new header to the current line
    invoke  StrCpy, edi, [lpszValueHeader]
    invoke  StrLen, [lpszValueHeader]
    add     edi, eax
    invoke  dwtoa, [integer], ADDR save_integer
    invoke  StrCpy, edi, ADDR save_integer

    ; put back the second part of the configuration file
    invoke  StrLen, edi
    add     edi, eax
    mov     byte ptr [edi], CR
    inc     edi
    mov     byte ptr [edi], LF
    inc     edi
    mov     ecx, [SecondPartLength]
    mov     esi, [pSecondPart]
    ; cld
    rep     movsb
    mov     byte ptr [edi], 0

    ; write the new information to the file
    invoke  SetFilePointer, [hFile], 0, 0, FILE_BEGIN
    invoke  WriteFile, [hFile], [pMem], 1, ADDR numb, 0 ; write a dummy byte, quick check to see if still acccess to file
    or      eax, eax
    jz      @@FILE_ERROR                                ; but if no access, than we see it now, rather than first truncating file :)
    invoke  SetFilePointer, [hFile], 0, 0, FILE_BEGIN
    invoke  SetEndOfFile, [hFile]
    invoke  StrLen, [pMem]
    lea     edx, [numb]
    invoke  WriteFile, [hFile], [pMem], eax, edx, 0
    invoke  GlobalFree, [pMem]
    xor     eax, eax
    inc     eax
    ret

  @@FILE_ERROR:
    push    eax
    ; Check if errors should be ignored (ask the user).
    invoke  cfg_CheckIgnoreError, [hwndDlg]
    or      eax, eax
    pop     eax
    jz      @@FILE_ERROR_RET ; Error not ignored.
    ; User wants to ignore the error.
    xor     eax, eax
    inc     eax
  @@FILE_ERROR_RET:
    ret
cfg_SaveInteger endp
