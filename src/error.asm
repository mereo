; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

HandleError proc uses edi hwndDlg:DWORD
LOCAL error_address         :DWORD

    invoke  GetLastError
    lea     edi, [error_address]
    invoke  FormatMessage, FORMAT_MESSAGE_ALLOCATE_BUFFER or FORMAT_MESSAGE_FROM_SYSTEM, 0, eax, \
            LANG_NEUTRAL, edi, 4096, 0
    invoke  MessageBox, [hwndDlg], [edi], ADDR app_name, MB_ICONSTOP
    ret
HandleError endp

WSAHandleError proc uses edi hwndDlg:DWORD, lpszCaption:DWORD
LOCAL error_address         :DWORD

    invoke  WSAGetLastError
    lea     edi, [error_address]
    invoke  FormatMessage, FORMAT_MESSAGE_ALLOCATE_BUFFER or FORMAT_MESSAGE_FROM_SYSTEM, 0, eax, \
            LANG_NEUTRAL, edi, 4096, 0
    invoke  MessageBox, [hwndDlg], [edi], [lpszCaption], MB_ICONSTOP
    ret
WSAHandleError endp
