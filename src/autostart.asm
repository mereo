; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

AutoStartDialogProc proc uses ebx esi hwndDlg:DWORD, uMsg:DWORD, wParam:DWORD, lParam:DWORD
LOCAL hTempWnd              :DWORD
LOCAL lbGetInt              :DWORD
LOCAL lbDirBrowse           :DWORD
LOCAL lbHidden              :DWORD
LOCAL lTempArg[8]           :BYTE
LOCAL lPathBuffer[2048]     :BYTE
LOCAL ip_bind_buffer[16]    :BYTE

    .if [uMsg] == WM_INITDIALOG
        invoke  SendMessage, [hwndDlg], WM_SETICON, ICON_BIG, [hMainIcon]

        ; ========================================================================================
        ; =  Parse all auto-start options and set them                                           =
        ; ========================================================================================

        ; Parse the port (if any) and set it (or set the default)
        invoke  GetCmdLineArgInt, ADDR autostart_arguments, ADDR CmdArgP
        .if eax == -1
            invoke  GetDlgItemInt, [ghwndDlg], ID_MAIN_PORT, ADDR lbGetInt, FALSE
            .if eax == 0 || eax > 0FFFFh
                mov     eax, [HTTP_DEFAULT_PORT]
            .endif
        .endif
        invoke  SetDlgItemInt, [hwndDlg], ID_AUTOSTART_PORT, eax, FALSE

        ; Parse the 'start hidden' option
        invoke  GetCmdLineArgPtr, ADDR autostart_arguments, ADDR CmdArgHide
        .if eax != -1
            invoke  GetDlgItem, [hwndDlg], IDC_AUTOSTART_HIDDEN
            invoke  SendMessage, eax, BM_SETCHECK, BST_CHECKED, 0
        .endif

        ; Parse the 'Directory Browsing' option
        invoke  GetCmdLineArgPtr, ADDR autostart_arguments, ADDR CmdArgDE
        mov     ebx, eax
        .if eax != -1
            invoke  GetDlgItem, [hwndDlg], IDC_AUTOSTART_DIRBROWSING
            invoke  SendMessage, eax, BM_SETCHECK, BST_CHECKED, 0
        .elseif
            invoke  GetDlgItem, [ghwndDlg], IDC_DIRBROWSING
            invoke  SendMessage, eax, BM_GETCHECK, 0, 0
            mov     ebx, eax
            invoke  GetDlgItem, [hwndDlg], IDC_AUTOSTART_DIRBROWSING
            invoke  SendMessage, eax, BM_SETCHECK, ebx, 0
        .endif

        ; Parse the 'Webroot' option
        invoke  GetCmdLineArgString, ADDR autostart_arguments, ADDR CmdArgH
        mov     ebx, eax
        .if ebx != -1
            invoke  GetDlgItem, [hwndDlg], ID_AUTOSTART_PATH
            invoke  SendMessage, eax, WM_SETTEXT, 0, ebx
            invoke  GlobalFree, ebx
        .else
            invoke  GetDlgItemText, [ghwndDlg], IDE_ROOT, ADDR lPathBuffer, SIZEOF lPathBuffer
            invoke  SetDlgItemText, [hwndDlg], ID_AUTOSTART_PATH, ADDR lPathBuffer
        .endif

        ; Parse the 'Bind Address' option
        invoke  GetDlgItem, [hwndDlg], IDE_AUTOSTART_BIND_ADDRESS
        mov     ebx, eax
        invoke  GetCmdLineArgString, ADDR autostart_arguments, ADDR CmdArgB
        mov     esi, eax
        .if esi != -1
            invoke  SendMessage, ebx, WM_SETTEXT, 0, esi
            invoke  GlobalFree, esi
        .else
            invoke  SendMessage, [hBindAddress], WM_GETTEXT, SIZEOF ip_bind_buffer, ADDR ip_bind_buffer
            invoke  SendMessage, ebx, WM_SETTEXT, 0, ADDR ip_bind_buffer
        .endif
    .elseif [uMsg] == WM_COMMAND
        .if [wParam] == ID_AUTOSTART_FINISH
            ; Validate path
            invoke  GetDlgItemText, [hwndDlg], ID_AUTOSTART_PATH, ADDR lPathBuffer, SIZEOF lPathBuffer
            .if eax == 0
                invoke  MessageBox, [hwndDlg], ADDR input_path_error, ADDR app_name, MB_ICONSTOP
                jmp     @@EXIT
            .endif

            invoke  GetFileAttributes, ADDR lPathBuffer
            .if eax == -1
                invoke  MessageBox, [hwndDlg], ADDR host_path_invalid, ADDR app_name, MB_ICONSTOP
                jmp     @@EXIT
            .elseif ! (eax & 010h)
                invoke  MessageBox, [hwndDlg], ADDR host_path_not_a_dir, ADDR app_name, MB_ICONSTOP
                jmp     @@EXIT
            .endif

            ; Validate port
            invoke  GetDlgItemInt, [hwndDlg], ID_AUTOSTART_PORT, ADDR lbGetInt, FALSE
            .if eax == 0 || eax > 0FFFFh
                invoke  MessageBox, [hwndDlg], ADDR invalid_port, ADDR app_name, MB_ICONSTOP
                jmp     @@EXIT
            .endif
            mov     [lbGetInt], eax ; Save port number

            ; Check the states of the checkboxes
            invoke  GetDlgItem, [hwndDlg], IDC_AUTOSTART_DIRBROWSING
            invoke  SendMessage, eax, BM_GETCHECK, 0, 0
            mov     [lbDirBrowse], eax
            invoke  GetDlgItem, [hwndDlg], IDC_AUTOSTART_HIDDEN
            invoke  SendMessage, eax, BM_GETCHECK, 0, 0
            mov     [lbHidden], eax

            ; ========================================================================================
            ; =  Combine all arguments and put them into the argument buffer, then close the dialog  =
            ; ========================================================================================
            invoke  ClearBuffer, ADDR autostart_arguments, SIZEOF autostart_arguments
            .if [lbDirBrowse] == BST_CHECKED
                invoke  StrCat, ADDR autostart_arguments, ADDR CmdArgDE
                invoke  StrCat, ADDR autostart_arguments, ADDR WhitespacePadding
            .endif

            .if [lbHidden] == BST_CHECKED
                invoke  StrCat, ADDR autostart_arguments, ADDR CmdArgHide
                invoke  StrCat, ADDR autostart_arguments, ADDR WhitespacePadding
            .endif

            .if [lbGetInt] != HTTP_DEFAULT_PORT
                invoke  StrCat, ADDR autostart_arguments, ADDR CmdArgP
                invoke  StrCat, ADDR autostart_arguments, ADDR WhitespacePadding
                invoke  dwtoa, [lbGetInt], ADDR lTempArg
                invoke  StrCat, ADDR autostart_arguments, ADDR lTempArg
                invoke  StrCat, ADDR autostart_arguments, ADDR WhitespacePadding
            .endif

            invoke  GetDlgItem, [hwndDlg], IDE_AUTOSTART_BIND_ADDRESS
            lea     edx, [ip_bind_buffer]
            invoke  SendMessage, eax, WM_GETTEXT, SIZEOF ip_bind_buffer, edx
            or      eax, eax
            jz      @F
            invoke  StrCat, ADDR autostart_arguments, ADDR CmdArgB
            invoke  StrCat, ADDR autostart_arguments, ADDR WhitespacePadding
            invoke  StrCat, ADDR autostart_arguments, ADDR ip_bind_buffer
            invoke  StrCat, ADDR autostart_arguments, ADDR WhitespacePadding
          @@:

            invoke  StrCat, ADDR autostart_arguments, ADDR CmdArgH
            invoke  StrCat, ADDR autostart_arguments, ADDR WhitespacePadding
            mov     byte ptr [lTempArg], '"'
            mov     byte ptr [lTempArg+1], 0
            invoke  StrCat, ADDR autostart_arguments, ADDR lTempArg
            invoke  StrCat, ADDR autostart_arguments, ADDR lPathBuffer
            mov     byte ptr [lTempArg], '"'
            mov     byte ptr [lTempArg+1], 0
            invoke  StrCat, ADDR autostart_arguments, ADDR lTempArg

            ; Close the dialog
            invoke  EndDialog, [hwndDlg], 0
        .elseif [wParam] == ID_AUTOSTART_BROWSE
            invoke  SHGetOpenDirName, [hwndDlg], ADDR lPathBuffer
            or      eax, eax
            jz      @@EXIT
            invoke  SetDlgItemText, [hwndDlg], ID_AUTOSTART_PATH, ADDR lPathBuffer
        .elseif [wParam] == IDC_AUTOSTART_DIRBROWSING || [wParam] == IDC_AUTOSTART_HIDDEN
            invoke  GetDlgItem, [hwndDlg], [wParam]
            mov     [hTempWnd], eax
            invoke  SendMessage, eax, BM_GETCHECK, 0, 0
            .if eax == BST_CHECKED
                mov     eax, BST_UNCHECKED
            .elseif eax == BST_UNCHECKED
                mov     eax, BST_CHECKED
            .endif
            invoke  SendMessage, [hTempWnd], BM_SETCHECK, eax, 0
        .elseif [wParam] == IDCANCEL
            invoke  SendMessage, [hwndDlg], WM_CLOSE, 0, 0
        .endif
    .elseif [uMsg] == WM_CLOSE
        invoke  EndDialog, [hwndDlg], -2
    .endif

    @@EXIT:
    xor     eax, eax
    ret
AutoStartDialogProc endp

; Retrieves the current startup setting for Mereo
; Returns -1 on error (no settings/no access to view the settings)
; Returns 0 if using HKEY_CURRENT_USER (HKCU) as current startup method or using HKCU and HKLM both as current startup method
; Returns 1 if using HKEY_LOCAL_MACHINE (HKLM) as current startup method
GetAutoStartSetting proc
LOCAL kHandle               :DWORD
LOCAL kResult               :DWORD
LOCAL ResultHKCU            :DWORD
LOCAL ResultHKLM            :DWORD
LOCAL temp                  :DWORD
LOCAL temp2                 :DWORD

    mov     [ResultHKCU], FALSE
    mov     [ResultHKLM], FALSE
    invoke  RegCreateKeyEx, HKEY_CURRENT_USER, ADDR autostart_run, 0, 0, REG_OPTION_NON_VOLATILE, \
            KEY_ALL_ACCESS, 0, ADDR kHandle, ADDR kResult
    .if eax == 0
        mov     [temp], SIZEOF autostart_arguments
        mov     [temp2], REG_SZ
        invoke  RegQueryValueEx, [kHandle], ADDR app_name, 0, \
                ADDR temp2, ADDR autostart_arguments, ADDR temp
        .if eax == 0
            mov     [ResultHKCU], TRUE
        .endif
        invoke  RegCloseKey, [kHandle]
    .endif

    invoke  RegCreateKeyEx, HKEY_LOCAL_MACHINE, ADDR autostart_run, 0, 0, REG_OPTION_NON_VOLATILE, \
            KEY_ALL_ACCESS, 0, ADDR kHandle, ADDR kResult
    .if eax == 0
        mov     [temp], SIZEOF autostart_arguments
        mov     [temp2], REG_SZ
        invoke  RegQueryValueEx, [kHandle], ADDR app_name, 0, \
                ADDR temp2, ADDR autostart_arguments, ADDR temp
        .if eax == 0
            mov     [ResultHKLM], TRUE
        .endif
        invoke  RegCloseKey, [kHandle]
    .endif

    .if [ResultHKLM] == TRUE
        mov     eax, 1
    .endif

    .if [ResultHKCU] == TRUE
        xor     eax, eax
    .endif

    .if [ResultHKCU] == FALSE && [ResultHKLM] == FALSE
        mov     eax, -1
    .endif
    ret
GetAutoStartSetting endp

; Deletes all auto-start settings for Mereo
DeleteAutoStartSettings proc newSetting:DWORD
LOCAL kHandle               :DWORD
LOCAL kResult               :DWORD

    .if [newSetting] == HKEY_LOCAL_MACHINE
        ; If new setting should be HKLM then old setting HKCU needs to be deleted
        mov     ecx, HKEY_CURRENT_USER
    .elseif [newSetting] == HKEY_CURRENT_USER
        ; And vice versa
        mov     ecx, HKEY_LOCAL_MACHINE
    .endif

    invoke  RegCreateKeyEx, ecx, ADDR autostart_run, 0, 0, REG_OPTION_NON_VOLATILE, \
            KEY_ALL_ACCESS, 0, ADDR kHandle, ADDR kResult
    .if eax == 0
        invoke  RegDeleteValue, [kHandle], ADDR app_name
        invoke  RegCloseKey, [kHandle]
    .endif
    ret
DeleteAutoStartSettings endp

; Writes the new auto-start setting to the registry
; Returns -1 if an unknown setting value has been specified
; Returns 0 if successful
SetAutoStartSetting proc uses edi newSetting:DWORD, delete:DWORD
LOCAL temp                  :DWORD
LOCAL temp2                 :DWORD
LOCAL tempKey               :DWORD
LOCAL kResult               :DWORD

    mov     eax, [newSetting]
    .if eax == HKEY_CURRENT_USER || eax == HKEY_LOCAL_MACHINE
        ; Set new settings
        invoke  RegCreateKeyEx, [newSetting], ADDR autostart_run, 0, 0, REG_OPTION_NON_VOLATILE, \
                KEY_ALL_ACCESS, 0, ADDR hKey, ADDR kResult
        .if eax == 0
            .if [newSetting] == HKEY_LOCAL_MACHINE
                ; If new setting should be HKLM then old setting HKCU should be retrieved (to display the old arguments so user can edit it)
                mov     ecx, HKEY_CURRENT_USER
            .elseif [newSetting] == HKEY_CURRENT_USER
                ; And vice versa
                mov     ecx, HKEY_LOCAL_MACHINE
            .endif

            invoke  RegCreateKeyEx, ecx, ADDR autostart_run, 0, 0, REG_OPTION_NON_VOLATILE, \
                    KEY_ALL_ACCESS, 0, ADDR tempKey, ADDR kResult
            mov     [temp], SIZEOF autostart_arguments
            mov     [temp2], REG_SZ
            invoke  RegQueryValueEx, [tempKey], ADDR app_name, 0, \
                    ADDR temp2, ADDR autostart_arguments, ADDR temp
            invoke  RegCloseKey, [tempKey]
            ; Parse current arguments
            cmp     byte ptr [autostart_arguments], 0
            jz      @@DIALOG
            cmp     byte ptr [autostart_arguments], '"'
            jnz     @@SEARCH_SPACE
            invoke  StrLen, ADDR autostart_arguments
            mov     ecx, eax
            mov     eax, '"'
            mov     edi, OFFSET autostart_arguments
            cld
            repnz   scasb
            jnz     @@CLEAR
            repnz   scasb
            jnz     @@CLEAR
            inc     edi
            jmp     @@FOUND
        @@CLEAR:
            xor     eax, eax
            mov     ecx, (128/4)
            mov     edi, OFFSET autostart_arguments
            rep     stosd
        @@SEARCH_SPACE:
            invoke  StrLen, ADDR autostart_arguments
            mov     ecx, eax
            mov     eax, ' '
            mov     edi, OFFSET autostart_arguments
            cld
            repnz   scasb
        @@FOUND:
            invoke  StrCpy, ADDR autostart_arguments, edi
        @@DIALOG:
            invoke  DialogBoxParam, [hInstance], ID_AUTOSTART_DIALOG, [ghwndDlg], ADDR AutoStartDialogProc, 0
            .if eax == 0
                ; Delete current settings
                invoke  DeleteAutoStartSettings, [newSetting]
                invoke  GetModuleFileName, 0, ADDR autostart_tempbuf, 2048
                invoke  wsprintf, ADDR autostart_runline, ADDR autostart_runtemplate, ADDR autostart_tempbuf, ADDR autostart_arguments
                invoke  StrLen, ADDR autostart_runline
                inc     eax
                ; Set the new settings
                invoke  RegSetValueEx, [hKey], ADDR app_name, 0, \
                        REG_SZ, ADDR autostart_runline, eax
                push    eax
                invoke  RegCloseKey, [hKey]
                pop     eax
                .if eax != 0
                    mov     eax, -1
                    ret
                .endif
                .if eax != 0
                    mov     eax, -1
                .endif
            .endif
        .else
            mov     eax, -1
            ret
        .endif
    .elseif [delete] == TRUE
        ; No auto-start, delete current settings
        invoke  DeleteAutoStartSettings, HKEY_CURRENT_USER
        invoke  DeleteAutoStartSettings, HKEY_LOCAL_MACHINE
        mov     byte ptr [autostart_arguments], 0
    .else
        ; Unknown requested new setting, ignore
        mov     eax, -1
        ret
    .endif
    ret
SetAutoStartSetting endp
