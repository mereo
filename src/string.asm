; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

; Clears an entire buffer
;
; Takes argument 'DST' as OFFSET to the buffer
; to clear and 'CNT' as length of the buffer.
ClearBuffer proc uses edi SRC:DWORD,
                          CNT:DWORD
    mov     ecx, [CNT]
    shr     ecx, 2
    mov     edi, [SRC]
    xor     eax, eax
    cld
    rep     stosd
    mov     ecx, [CNT]
    and     ecx, 3
    rep     stosb
    ret
ClearBuffer endp

; Calculates a string's length
;
; Takes argument 'SRC' as OFFSET to the string buffer
StrLen proc uses edi SRC:DWORD
    or      ecx, -1
    mov     edi, [SRC]
    xor     eax, eax
    cld
    repnz   scasb
    inc     ecx
    not     ecx
    mov     eax, ecx
    ret
StrLen endp

StrCmp proc uses esi edi SRC:DWORD,
                         DST:DWORD
    invoke  StrLen, [SRC]
    inc     eax
    mov     ecx, eax
    mov     esi, [SRC]
    mov     edi, [DST]
    cld
    repz    cmpsb
    mov     eax, 0
    jz      @F
    mov     eax, -2

    @@:
    ret
StrCmp endp

StrCmpN proc uses ebx esi edi SRC:DWORD,
                              DST:DWORD,
                              LEN:DWORD
    mov     ebx, [LEN]
    mov     esi, [SRC]
    mov     edi, [DST]

  @@LOOP:
    mov     al, byte ptr [esi]
    mov     ah, byte ptr [edi]
    cmp     al, ah
    lea     esi, [esi+1]
    lea     edi, [edi+1]
    jnz     @@FAIL
    dec     ebx
    jz      @@SUCCESS
    jmp     @@LOOP

  @@SUCCESS:
    xor     eax, eax
    jmp     @@RET

  @@FAIL:
    mov     eax, -2
    ; jmp     @@RET

  @@RET:
    ret
StrCmpN endp

StrCmpI proc uses ebx esi edi SRC:DWORD,
                              DST:DWORD
LOCAL lpLowCharBuffer[2]    :BYTE
LOCAL lpLowCharBuffer2[2]   :BYTE

    mov     byte ptr [lpLowCharBuffer+1], 0
    mov     byte ptr [lpLowCharBuffer2+1], 0

    invoke  StrLen, [SRC]
    mov     ebx, eax
    inc     ebx ; increase by one to make sure the NULL byte is also compared
    mov     esi, [SRC]
    mov     edi, [DST]

  @@LOOP:
    mov     al, byte ptr [esi]
    mov     byte ptr [lpLowCharBuffer], al
    invoke  CharLower, ADDR lpLowCharBuffer
    mov     al, byte ptr [edi]
    mov     byte ptr [lpLowCharBuffer2], al
    invoke  CharLower, ADDR lpLowCharBuffer2
    mov     al, byte ptr [lpLowCharBuffer]
    mov     ah, byte ptr [lpLowCharBuffer2]
    cmp     al, ah
    lea     esi, [esi+1]
    lea     edi, [edi+1]
    jnz     @@FAIL
    dec     ebx
    jz      @@SUCCESS
    jmp     @@LOOP

  @@SUCCESS:
    xor     eax, eax
    jmp     @@RET

  @@FAIL:
    mov     eax, -2
    ; jmp     @@RET

  @@RET:
    ret
StrCmpI endp

StrCmpIn proc uses ebx esi edi SRC:DWORD,
                               DST:DWORD,
                               LEN:DWORD
LOCAL lpLowCharBuffer[2]    :BYTE
LOCAL lpLowCharBuffer2[2]   :BYTE

    mov     byte ptr [lpLowCharBuffer+1], 0
    mov     byte ptr [lpLowCharBuffer2+1], 0

    mov     ebx, [LEN]
    mov     esi, [SRC]
    mov     edi, [DST]

  @@LOOP:
    mov     al, byte ptr [esi]
    mov     byte ptr [lpLowCharBuffer], al
    invoke  CharLower, ADDR lpLowCharBuffer
    mov     al, byte ptr [edi]
    mov     byte ptr [lpLowCharBuffer2], al
    invoke  CharLower, ADDR lpLowCharBuffer2
    mov     al, byte ptr [lpLowCharBuffer]
    mov     ah, byte ptr [lpLowCharBuffer2]
    cmp     al, ah
    lea     esi, [esi+1]
    lea     edi, [edi+1]
    jnz     @@FAIL
    dec     ebx
    jz      @@SUCCESS
    jmp     @@LOOP

  @@SUCCESS:
    xor     eax, eax
    jmp     @@RET

  @@FAIL:
    mov     eax, -2
    ; jmp     @@RET

  @@RET:
    ret
StrCmpIn endp

StrCat proc uses esi edi DST:DWORD,
                         SRC:DWORD
    invoke  StrLen, [SRC]
    inc     eax     ; One more for NULL byte
    push    eax
    invoke  StrLen, [DST]
    mov     edi, [DST]
    add     edi, eax
    mov     esi, [SRC]
    pop     ecx
    cld
    rep     movsb
    ret
StrCat endp

StrCpy proc uses esi edi DST:DWORD,
                         SRC:DWORD
    invoke  StrLen, [SRC]
    mov     ecx, eax
    inc     ecx     ; One more for NULL byte
    mov     esi, [SRC]
    mov     edi, [DST]
    cld
    rep     movsb
    ret
StrCpy endp

; Cuts off spaces at the end and start of a string
;
; Takes argument 'DST' as the address of the string
; of which to clear spaces.
StrCutSpaces proc uses edi DST:DWORD
LOCAL start                 :DWORD
LOCAL stringlength          :DWORD

    ; Initialize local(s)
    mov     [start], -1

    ; Cut off spaces from the beginning
    mov     edi, [DST]
    invoke  StrLen, edi
    mov     [stringlength], eax
    .if byte ptr [edi] == ' '
        .while eax != 0 && byte ptr [edi] == ' '
            inc     [start]
            inc     edi
            dec     eax
        .endw
        ; Were there spaces at the beginning? Cut 'm off
        .if [start] != -1
            mov     edi, [DST]
            add     edi, [start]
            invoke  StrCpy, [DST], edi
        .endif
    .endif

    ; Cut off spaces from the end
    mov     edi, [DST]
    mov     eax, [stringlength]
    dec     eax
    .while eax != 0 && byte ptr [edi+eax] == ' '
        mov     byte ptr [edi+eax], 0
        dec     eax
    .endw

    ret
StrCutSpaces endp

; DQ 2 Decimal ASCIIZ
; Input  = 64-bit bit value in dqtoa_value:dqtoa_value2
; Output = converted value in dqtoa_buffer, and REAL4 value in dqtoa_real
; NOTE: Procedure converts the 64-bit value to an unsigned decimal value (in ASCIIZ format)
.data
real4n                      REAL4 4294967296.0
.code
dqtoa proc dqtoa_value:DWORD, dqtoa_value2:DWORD, dqtoa_buffer:DWORD, dqtoa_real:DWORD
LOCAL temp                  :DWORD
LOCAL temp_val1             :DWORD
LOCAL temp_val2             :DWORD
LOCAL temp_val3             :DWORD
LOCAL content[108]          :BYTE

    fsave   [content]
    mov     [temp], -1
    fild    [temp]
    mov     [temp_val3], FALSE
    cmp     [dqtoa_value], 07FFFFFFFh
    jbe     @F
    sub     [dqtoa_value], 07FFFFFFFh
    mov     [temp_val3], 07FFFFFFFh

  @@:
    fild    [dqtoa_value]
    fcom    st(1)
    fstsw   ax
    test    ax, 256
    jz      @F
    fchs

  @@:
    cmp     [temp_val3], FALSE
    jz      @F
    fild    [temp_val3]
    faddp   st(1), st(0)

  @@:
    mov     [temp_val3], FALSE
    cmp     [dqtoa_value2], 07FFFFFFFh
    jbe     @F
    sub     [dqtoa_value2], 07FFFFFFFh
    mov     [temp_val3], 07FFFFFFFh

  @@:
    fild    [dqtoa_value2]
    fcom    st(1)
    fstsw   ax
    test    ax, 256
    jz      @F
    fchs

  @@:
    cmp     [temp_val3], FALSE
    jz      @F
    fild    [temp_val3]
    faddp   st(1), st(0)

  @@:
    ; X = val 2 * 4294967296 + val 1
    fld     [real4n]
    fchs
    fmulp   st(1), st(0)
    faddp   st(1), st(0)
    mov     eax, [dqtoa_real]
    fst     qword ptr [eax]
    .if [dqtoa_buffer] != 0
        invoke  FpuFLtoA, 0, 0, [dqtoa_buffer], SRC1_FPU
    .endif
    frstor  [content]
    ret
dqtoa endp

; Counts how many times 'char' was found in the buffer pointed to
; by 'lpBuffer'.
;
; lpBuffer      = points to the buffer in which to search
; char          = the character to search for
;
; Return values:
; Returns the number of times that 'char' was found in the buffer
; pointed to by 'lpBuffer'.
strchrcnt proc uses edi lpBuffer:DWORD, char:DWORD
LOCAL lcCounter             :DWORD

    mov     [lcCounter], 0

    mov     edi, [lpBuffer]
    invoke  StrLen, edi
    mov     ecx, eax
    mov     eax, [char]
    cld
  @@LOOP:
    repnz   scasb
    jnz     @@RET
    inc     [lcCounter]
    jmp     @@LOOP

  @@RET:
    mov     eax, [lcCounter]
    ret
strchrcnt endp

; Searches for a string inside another string
;
; The comparison is not case-sensitive.
;
; lpBuffer       = points to the buffer in which to search
; lpSearchString = a null-terminated string to search for
;
; Return Values:
; a non-zero value indicating the index offset where lpSearchString
; is found in lpBuffer, -1 on failure
instringi proc uses ebx esi edi lpszBuffer:DWORD, lpszKeyword:DWORD
LOCAL keywordLower[2]       :BYTE
LOCAL keywordLen            :DWORD
LOCAL bufferChar[2]         :BYTE

    xor     ebx, ebx
    dec     ebx
    mov     esi, [lpszBuffer]
    mov     edi, [lpszKeyword]
    invoke  StrLen, edi
    mov     [keywordLen], eax
    mov     al, [edi]
    mov     [keywordLower], al
    mov     [keywordLower+1], 0
    invoke  CharLower, ADDR keywordLower
    mov     [bufferChar+1], 0

    @@LOOP:
        @@INNER_LOOP:
        inc     ebx
        mov     dl, [esi+ebx]
        mov     [bufferChar], dl
        invoke  CharLower, ADDR bufferChar
        mov     dl, [bufferChar]
        or      dl, dl
        jz      @@RET_FAILURE
        mov     al, [keywordLower]
        cmp     al, dl
        jnz     @@INNER_LOOP
    lea     eax, [esi+ebx]
    invoke  StrCmpIn, eax, edi, [keywordLen]
    or      eax, eax
    jnz     @@LOOP
    ; success
    mov     eax, ebx
    jmp     @@RET

    @@RET_FAILURE:
    xor     eax, eax
    dec     eax
    ; jmp   @@RET

    @@RET:
    ret
instringi endp

; Converts all backslashes to forward slashes
ConvertToSlashes proc SRC:DWORD
    mov     edx, [SRC]
    .while byte ptr [edx] != 0
        .if byte ptr [edx] == '\'
            mov byte ptr [edx], '/'
        .endif
        inc     edx
    .endw
    ret
ConvertToSlashes endp
