; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

HyperlinkProc proc uses ebx esi hwndDlg:DWORD, uMsg:DWORD, wParam:DWORD, lParam:DWORD
    cmp     [uMsg], WM_NCHITTEST
    jnz     @F
    mov     eax, TRUE
    ret

    @@:
    .if [uMsg] == WM_PAINT
        invoke  BeginPaint, [hwndDlg], ADDR ps
        mov     ebx, eax
        .if [HyperlinkActive] == TRUE
            invoke  SetTextColor, ebx, 002866F7h
        .else
            invoke  SetTextColor, ebx, 00FF0000h
        .endif
        invoke  GetSysColor, COLOR_BTNFACE
        invoke  SetBkColor, ebx, eax
        invoke  GetStockObject, DEFAULT_GUI_FONT
        mov     [hGUIFont], eax
        invoke  GetObject, eax, SIZEOF LOGFONT, ADDR lfont
        mov     [lfont.lfWeight], 900
        mov     [lfont.lfUnderline], 1
        mov     [lfont.lfQuality], PROOF_QUALITY
        invoke  StrCpy, ADDR [lfont.lfFaceName], ADDR verdana
        invoke  CreateFontIndirect, ADDR lfont
        mov     [hLogFont], eax
        invoke  SelectObject, ebx, eax
        mov     esi, eax
        invoke  GetClientRect, [hwndDlg], ADDR rect
        invoke  DrawText, ebx, ADDR about_hyperlink, -1, ADDR rect, DT_LEFT or DT_TOP or DT_SINGLELINE
        invoke  SelectObject, ebx, esi
        invoke  EndPaint, [hwndDlg], ADDR ps
    .elseif [uMsg] == WM_MOUSEMOVE
        invoke  GetClientRect, [hwndDlg], ADDR rect
        movzx   eax, word ptr [lParam]
        mov     [pt.x], eax
        movzx   eax, word ptr [lParam+2]
        mov     [pt.y], eax
        invoke  PtInRect, ADDR rect, [pt.x], [pt.y]
        .if eax == TRUE
            mov     [HyperlinkActive], TRUE
            invoke  InvalidateRect, [hwndDlg], 0, 0
            invoke  SetCapture, [hwndDlg]
        .else
            mov     [HyperlinkActive], FALSE
            invoke  InvalidateRect, [hwndDlg], 0, 0
            invoke  ReleaseCapture
        .endif
    .elseif [uMsg] == WM_SETCURSOR
        invoke  SetCursor, [hFinger]
    .elseif [uMsg] == WM_LBUTTONDOWN
            invoke  ShellExecute, 0, ADDR help_operation, ADDR about_hyperlink, 0, 0, SW_SHOWMAXIMIZED
            .if eax == SE_ERR_ACCESSDENIED || eax == SE_ERR_DLLNOTFOUND || eax == SE_ERR_NOASSOC
                invoke  MessageBox, [hwndDlg], ADDR help_nobrowser, ADDR sorry_caption, MB_ICONEXCLAMATION
            .else
                mov     [HyperlinkActive], FALSE
                invoke  PostMessage, [hAboutDlg], WM_CLOSE, 0, 0
            .endif
    .else
            invoke  GetWindowLong, [hwndDlg], GWL_USERDATA
            invoke  CallWindowProc, eax, [hwndDlg], [uMsg], [wParam], [lParam]
            ret
    .endif

    xor     eax, eax
    ret
HyperlinkProc endp

AreYouSure proc hwndDlg:DWORD
    invoke  MessageBox, [hwndDlg], ADDR yousure_question, ADDR app_name, MB_ICONQUESTION or MB_YESNOCANCEL
    xor     eax, 7
    ; EAX = TRUE on IDYES and FALSE on IDNO
    ret
AreYouSure endp

AboutDialogProc proc hwndDlg:DWORD, uMsg:DWORD, wParam:DWORD, lParam:DWORD
    .if [uMsg] == WM_INITDIALOG
        mov     eax, [hwndDlg]
        mov     [hAboutDlg], eax
        invoke  LoadIcon, [hInstance], ID_MAIN_ICON
        push    eax
        invoke  SendMessage, [hwndDlg], WM_SETICON, ICON_BIG, eax
        pop     eax
        invoke  DestroyIcon, eax
        invoke  SetDlgItemText, [hwndDlg], ID_ABOUT_TEXT, ADDR about_msg
        invoke  SetDlgItemText, [hwndDlg], ID_ABOUT_TEXT2, ADDR about_msg2
        invoke  SetDlgItemText, [hwndDlg], ID_ABOUT_TEXT3, ADDR about_msg3
        invoke  SetDlgItemText, [hwndDlg], ID_ABOUT_TEXT4, ADDR about_thanksto
        invoke  SetDlgItemText, [hwndDlg], ID_HYPERLINK, ADDR about_hyperlink
        invoke  GetStockObject, HOLLOW_BRUSH
        mov     [hHBrush], eax
        invoke  LoadCursor, 0, IDC_HAND
        mov     [hFinger], eax
        invoke  GetDlgItem, [hwndDlg], ID_HYPERLINK
        push    eax
        invoke  SetWindowLong, eax, GWL_WNDPROC, ADDR HyperlinkProc
        pop     ecx
        invoke  SetWindowLong, ecx, GWL_USERDATA, eax
    .elseif [uMsg] == WM_COMMAND
        mov     eax, [wParam]
        cmp     ax, IDM_EXIT
        jz      @F
        cmp     ax, IDCANCEL
        jz      @F
    .elseif [uMsg] == WM_CLOSE
    @@:
        invoke  DeleteObject, [hLogFont]
        invoke  DeleteObject, [hGUIFont]
        invoke  DestroyCursor, [hFinger]
        invoke  DeleteObject, [hHBrush]
        invoke  EndDialog, [hwndDlg], 0
    .endif
    xor     eax, eax
    ret
AboutDialogProc endp

MainDialogProc proc uses ebx esi edi hwndDlg:DWORD, uMsg:DWORD, wParam:DWORD, lParam:DWORD
LOCAL fav_buffer[32]:BYTE, fav_deletePos:DWORD, favRelocate:DWORD, iniFileSize:DWORD, pMem:DWORD
LOCAL charsetStandard:DWORD, ThreadID:DWORD, accept_incoming_addr:DWORD, ip_bind_address:DWORD
LOCAL ip_bind_buffer[16]:BYTE, hAcceptSocket:DWORD, numb:DWORD

    mov     eax, [uMsg]
    .if eax == WM_INITDIALOG
        mov     eax, hwndDlg
        mov     [ghwndDlg], eax
        invoke  SendMessage, [hwndDlg], WM_SETICON, ICON_BIG, [hMainIcon]
        invoke  CreatePopupMenu
        mov     [hPopupMenu], eax
        invoke  AppendMenu, eax, MF_STRING, IDM_HOST, ADDR notarea_host
        invoke  AppendMenu, [hPopupMenu], MF_STRING, IDM_STOPHOST, ADDR notarea_stophost
        invoke  AppendMenu, [hPopupMenu], MF_SEPARATOR, 0, 0
        invoke  AppendMenu, [hPopupMenu], MF_STRING, IDM_RESTORE, ADDR notarea_restore
        invoke  AppendMenu, [hPopupMenu], MF_SEPARATOR, 0, 0
        invoke  AppendMenu, [hPopupMenu], MF_STRING, IDM_NOTAREA_EXIT, ADDR notarea_exit

        ; Register for taskbar creation messages
        invoke  RegisterWindowMessage, ADDR taskbar_created_msg
        mov     [taskbar_created_msg_code], eax

        mov     [notarea_icon_data.cbSize], SIZEOF notarea_icon_data
        mov     [notarea_icon_data.uID], ID_NOTAREA_ICON_MSG
        mov     [notarea_icon_data.uTimeout], 5000 ; 5 seconds
        mov     [notarea_icon_data.uCallbackMessage], WM_NOTAREA_ICON
        mov     [notarea_icon_data.uFlags], NIF_MESSAGE or NIF_ICON or NIF_TIP
        mov     [notarea_icon_data.dwInfoFlags], 0
        push    [hwndDlg]
        pop     [notarea_icon_data.hWnd]
        invoke  LoadIcon, [hInstance], ID_NOTAREA_ICON_STOPPED
        push    eax
        mov     [notarea_icon_data.hIcon], eax
        invoke  StrCpy, ADDR notarea_icon_data.szTip, ADDR app_name
        invoke  Shell_NotifyIcon, NIM_ADD, ADDR notarea_icon_data
        pop     eax
        invoke  DestroyIcon, eax

        ; Get handle of bind address control
        invoke  GetDlgItem, [hwndDlg], IDE_BIND_ADDRESS
        mov     [hBindAddress], eax

        invoke  GetMenu, [hwndDlg]
        mov     [hMenu], eax
        invoke  EnableMenuItem, eax, IDM_STOPHOST, MF_GRAYED
        invoke  EnableMenuItem, [hPopupMenu], IDM_STOPHOST, MF_GRAYED
        invoke  EnableMenuItem, [hPopupMenu], IDM_RESTORE, MF_GRAYED

        invoke  GetDlgItem, [hwndDlg], IDC_AUTOSTART
        mov     [hAutoStart], eax
        invoke  GetDlgItem, [hwndDlg], IDC_AUTOSTART_CURRENT
        mov     [hAutoStartCurrent], eax
        invoke  GetDlgItem, [hwndDlg], IDC_AUTOSTART_ALL
        mov     [hAutoStartAll], eax
        invoke  GetAutoStartSetting
        push    eax ; Save auto-start setting
        .if eax != -1
            invoke  SendMessage, [hAutoStart], BM_SETCHECK, BST_CHECKED, 0
            invoke  EnableWindow, [hAutoStartCurrent], TRUE
            invoke  EnableWindow, [hAutoStartAll], TRUE
        .endif
        pop     eax
        .if eax == 0 ; Restore auto-start setting
            invoke  SendMessage, [hAutoStartCurrent], BM_SETCHECK, BST_CHECKED, 0
        .elseif eax == 1
            invoke  SendMessage, [hAutoStartAll], BM_SETCHECK, BST_CHECKED, 0
        .endif

        invoke  GetDlgItem, [hwndDlg], IDC_DIRBROWSING
        mov     [hDirBrowsing], eax
        .if [ArgDE] == TRUE
            invoke  SendMessage, [hDirBrowsing], BM_SETCHECK, BST_CHECKED, 0
            invoke  SetFocus, 0
        .endif
        .if [ArgHide] == TRUE
            invoke  PostMessage, [hwndDlg], WM_SIZE, SIZE_MINIMIZED, 0
        .endif
        invoke  GetStockObject, DEFAULT_GUI_FONT
        push    eax
        invoke  CreateWindowEx, 0, ADDR statusbar_class, ADDR status_deftext, \
                WS_CHILD or WS_VISIBLE, 0, 0, 0, 0, [hwndDlg], 0, 0, 0
        mov     [hStatus], eax
        pop     ecx
        invoke  SendMessage, eax, WM_SETFONT, ecx, TRUE
        invoke  SendMessage, [hStatus], SB_SETPARTS, 3, ADDR status_parts
        invoke  SendMessage, [hStatus], SB_SETTEXT, 1, ADDR status_notrunning
        invoke  wsprintf, ADDR status_buffer, ADDR status_conncount, 0
        invoke  SendMessage, [hStatus], SB_SETTEXT, 2, ADDR status_buffer

        .if [ArgH] == TRUE
            invoke  SetDlgItemText, [hwndDlg], IDE_ROOT, ADDR webroot_buffer
            invoke  PostMessage, [hwndDlg], WM_COMMAND, IDM_HOST, 0
        .else
            invoke  SetDlgItemText, [hwndDlg], IDE_ROOT, ADDR webroot_standard
        .endif

        .if [ArgB] == TRUE
            invoke  SendMessage, [hBindAddress], WM_SETTEXT, 0, [cmdline_ip_bind_address]
            invoke  GlobalFree, [cmdline_ip_bind_address]
        .endif

        invoke  SetDlgItemInt, [hwndDlg], ID_MAIN_PORT, [HTTP_PORT], FALSE

        ; Get local machine name
        invoke  gethostname, ADDR comp_name_buffer, 128
        invoke  wsprintf, ADDR comp_fullname_buffer, ADDR comp_name, ADDR comp_name_buffer
        ; Get local machine IP address
        invoke  gethostbyname, ADDR comp_name_buffer
        .if eax == 0
            mov     eax, OFFSET comp_noipaddress
        .else
            mov     eax, [eax+12]
            mov     eax, [eax]
            mov     eax, [eax]
            invoke  inet_ntoa, eax
        .endif
        invoke  wsprintf, ADDR comp_fullipaddress_buffer, ADDR comp_ipaddress, eax
        invoke  SetDlgItemText, [hwndDlg], IDT_COMPNAME, ADDR comp_fullname_buffer
        invoke  SetDlgItemText, [hwndDlg], IDT_COMPIPADDRESS, ADDR comp_fullipaddress_buffer
        invoke  wsprintf, ADDR logstatus, ADDR logstatusmsg, ADDR logok
        invoke  SetDlgItemText, [hwndDlg], IDT_LOG_STATUS, ADDR logstatus

        invoke  cfg_ParseString, [hIniFile], ADDR cfg_charset_header
        .if eax != 0
            push    eax
            invoke  SetDlgItemText, [hwndDlg], ID_MAIN_CHARSET, ecx
            pop     eax
            invoke  GlobalFree, eax
        .else
            invoke  SetDlgItemText, [hwndDlg], ID_MAIN_CHARSET, ADDR charset_standard
        .endif

        ; Timers
        invoke  SetTimer, [hwndDlg], ID_TIMER1, 1000, ADDR CheckThreadCount
        invoke  SetTimer, [hwndDlg], ID_TIMER2, 1000, ADDR CheckKeepAliveTimeout

        ; Setup the 'updates check' timer
        invoke  UpdateProgramUpdateTimer, [hwndDlg]

        ; Load the 'prefs_dirbrowse_force' setting
        invoke  cfg_ParseInteger, [hIniFile], ADDR cfg_prefs_db_force_header
        .if eax == 1
            mov     [prefs_dirbrowse_force], 1
        .endif
    .elseif eax == [taskbar_created_msg_code]
        invoke  Shell_NotifyIcon, NIM_ADD, ADDR notarea_icon_data
    .elseif eax == WM_SIZE
        cmp     [wParam], SIZE_MINIMIZED
        jz      @@HIDE_WINDOW
    .elseif eax == WM_NOTAREA_ICON
        cmp     [wParam], ID_NOTAREA_ICON_MSG
        jnz     @@EXIT
        cmp     [lParam], WM_RBUTTONUP
        jz      @@SHOW_MENU
        cmp     [lParam], WM_LBUTTONDBLCLK
        jnz     @@EXIT

    @@HIDE_OR_SHOW:
        invoke  IsWindowVisible, [hwndDlg]
        test    eax, eax
        jz      @@SHOW_WINDOW

        ; Hide window
    @@HIDE_WINDOW:
        invoke  EnableMenuItem, [hPopupMenu], IDM_RESTORE, MF_ENABLED
        invoke  ShowWindow, [hwndDlg], SW_HIDE
        invoke  GetWindow, [hwndDlg], GW_HWNDNEXT
        invoke  SetActiveWindow, eax
        mov     [MainDialogHidden], TRUE
        jmp     @@EXIT

        ; Restore window
    @@SHOW_WINDOW:
        invoke  EnableMenuItem, [hPopupMenu], IDM_RESTORE, MF_GRAYED
        invoke  ShowWindow, [hwndDlg], SW_SHOWNORMAL
        invoke  SetForegroundWindow, [hwndDlg]
        mov     [MainDialogHidden], FALSE
        jmp     @@EXIT

        ; Show menu if right button up
    @@SHOW_MENU:
        invoke  GetCursorPos, ADDR notarea_cursor_point
        invoke  SetForegroundWindow, [hwndDlg]
        invoke  TrackPopupMenu,
                [hPopupMenu],
                TPM_RIGHTBUTTON or \
                TPM_LEFTALIGN or \
                TPM_TOPALIGN or \
                TPM_BOTTOMALIGN,
                [notarea_cursor_point.x], \
                [notarea_cursor_point.y],
                0,
                [hwndDlg],
                0
        invoke  PostMessage, [hwndDlg], WM_NULL, 0, 0
    .elseif eax == WM_MENUSELECT
        mov     eax, [wParam]
        .if ax == 3
            invoke  ParseFavorites, [hwndDlg]
        .endif
    .elseif eax == WM_UPDATE_NOTIFY
        invoke  MessageBox, [hwndDlg], [lParam], ADDR app_name, [wParam]
    .elseif eax == WM_COMMAND
        mov     eax, [wParam]
        cmp     ax, IDM_VIEWLOG
        jz      @@VIEWLOG
        cmp     ax, IDM_CLEARLOG
        jz      @@CLEARLOG
        cmp     ax, IDM_SAVELOG
        jz      @@SAVELOG
        cmp     ax, IDM_RESTORE
        jz      @@SHOW_WINDOW

        .if ax == IDCANCEL
            invoke  SendMessage, [hwndDlg], WM_CLOSE, 0, 0
        .elseif ax == IDM_UPDATE_CHECK
            cmp     [updateMainThreadHandle], 0
            jz      @F
            invoke  MessageBox, [hwndDlg], ADDR update_busy, ADDR app_name, MB_ICONEXCLAMATION
            jmp     @@EXIT
          @@:

            invoke  MessageBox, [hwndDlg], ADDR update_check_question, ADDR app_name, MB_ICONQUESTION or MB_YESNOCANCEL
            .if eax == IDYES
                invoke  CheckForProgramUpdates, 0, UPDATE_CHECK_NOTIFY, 0, 0
            .endif
        .elseif ax == IDM_SETTINGS_UPDATE
            invoke  DialogBoxParam, [hInstance], ID_SETTINGS_UPDATE_DIALOG, [hwndDlg], ADDR UpdateSettingsDialogProc, 0
        .elseif ax == IDM_SETTINGS_PREFS
            invoke  DialogBoxParam, [hInstance], ID_SETTINGS_PREFS_DIALOG, [hwndDlg], ADDR PreferencesDialogProc, 0
        .elseif ax >= IDM_FAV && ax < IDM_MAXFAV
            invoke  GetDlgItem, [hwndDlg], IDE_ROOT
            invoke  IsWindowEnabled, eax
            .if eax != 0
                mov     eax, [wParam]
                and     eax, 00000FFFFh
                sub     eax, (IDM_FAV)
                push    eax
                invoke  GetFavoriteHeaderValue, eax, ADDR fav_path
                pop     ecx
                .if eax == -1
                    invoke  wsprintf, ADDR favParseErrorBuffer, ADDR favParseTemplate, ecx
                    invoke  MessageBox, [hwndDlg], ADDR favParseErrorBuffer, ADDR app_name, MB_ICONSTOP
                .else
                    mov     esi, ecx
                    mov     ebx, eax
                    invoke  SetDlgItemText, [hwndDlg], IDE_ROOT, ebx
                    invoke  GlobalFree, ebx

                    invoke  SetDlgItemInt, [hwndDlg], ID_MAIN_PORT, HTTP_DEFAULT_PORT, FALSE
                    invoke  GetFavoriteHeaderValue, esi, ADDR fav_port
                    mov     ebx, eax
                    .if ebx != -1
                        invoke  SetDlgItemText, [hwndDlg], ID_MAIN_PORT, ebx
                        invoke  GlobalFree, ebx
                    .endif

                    invoke  SendMessage, [hDirBrowsing], BM_SETCHECK, BST_UNCHECKED, 0
                    invoke  GetFavoriteHeaderValue, esi, ADDR fav_dirbrowse
                    mov     ebx, eax
                    .if ebx != -1
                        invoke  StrCmpI, ebx, ADDR true
                        .if eax == 0
                            invoke  SendMessage, [hDirBrowsing], BM_SETCHECK, BST_CHECKED, 0
                        .endif
                        invoke  GlobalFree, ebx
                    .endif

                    invoke  SendMessage, [hBindAddress], WM_SETTEXT, 0, ADDR szNull
                    invoke  GetFavoriteHeaderValue, esi, ADDR fav_bind_address
                    mov     ebx, eax
                    .if ebx != -1
                        invoke  SendMessage, [hBindAddress], WM_SETTEXT, 0, ADDR fav_bind_address
                        invoke  GlobalFree, ebx
                    .endif

                    invoke  SetFocus, 0
                    invoke  SendMessage, [hwndDlg], WM_COMMAND, IDM_HOST, 0
                .endif
            .else
                invoke  MessageBox, [hwndDlg], ADDR favAbort, ADDR app_name, MB_ICONQUESTION or MB_YESNO
                .if eax == IDYES
                    mov     [favAbortFlag], TRUE
                    invoke  SendMessage, [hwndDlg], WM_COMMAND, IDM_STOPHOST, 0
                    invoke  SendMessage, [hwndDlg], WM_COMMAND, [wParam], 0
                .endif
            .endif
        .elseif ax == IDM_ADDFAV
            .if [hIniFile] == -1 || [favReadOnly] == TRUE
                ; Tell user 'mereo.cfg' can't be edited because there was no access and thus no favorites can be read/added/removed
                invoke  MessageBox, [hwndDlg], ADDR favNoFavFileCreated, ADDR app_name, MB_ICONEXCLAMATION
            .else
                ; Ask user to insert favorite name and filename
                invoke  DialogBoxParam, [hInstance], ID_ADDFAV_DIALOG, [hwndDlg], ADDR AddFavDialogProc, 0
                ; Add it to 'mereo.cfg'
                .if eax != -1
                    invoke  cfg_GetSizeSetPtr, [hIniFile]
                    or      eax, eax
                    jnz     @F
                    invoke  GetLastError

                    ; CASE 1:
                    dec     eax
                    jz      @@MEM_ERROR

                    ; CASE 2:
                    dec     eax
                    jz      @@FILE_ERROR

                @@:
                    mov     [pMem], eax

                    ; Add CR LF sequences at end of file if needed
                    .if ecx != 0 ; Check if length is 0
                        ; Check if last 4 bytes are CR LF or only last 2 are or if none are
                        mov     dl, byte ptr [eax+ecx-1]
                        mov     dh, byte ptr [eax+ecx-2]
                        mov     bl, byte ptr [eax+ecx-3]
                        mov     bh, byte ptr [eax+ecx-4]
                        .if (dl != CR && dl != LF) && (dh != CR && dh != LF) && \
                            (bl != CR && bl != LF) && (bh != CR && bh != LF)
                            ; No CR LF were found at the end of the file, add two CR LF sequences
                            invoke  StrCat, [pMem], ADDR double_crlf
                        .elseif (dl == CR || dl == LF) && (dh == CR || dh == LF) && \
                                (bl != CR && bl != LF) && (bh != CR && bh != LF)
                            ; One CR LF was found at the end of the file, add one more
                            invoke  StrCat, [pMem], ADDR crlf
                        .endif
                    .endif

                    ; ***************************************************
                    ;  Add [FavoriteXX] header
                    ; ***************************************************
                    invoke  StrLen, [pMem]
                    add     eax, [pMem]
                    push    eax
                    invoke  GetNextFavNumber
                    pop     ecx
                    invoke  wsprintf, ecx, ADDR fav_number, eax

                    ; New line after [FavoriteXX] header
                    invoke  StrCat, [pMem], ADDR crlf

                    ; ***************************************************
                    ;  Now write Name=X
                    ; ***************************************************
                    invoke  StrCat, [pMem], ADDR fav_name
                    invoke  StrCat, [pMem], ADDR fav_data_name
                    invoke  StrCat, [pMem], ADDR crlf

                    ; ***************************************************
                    ;  Now write Path=X
                    ; ***************************************************
                    invoke  StrCat, [pMem], ADDR fav_path
                    invoke  StrCat, [pMem], ADDR fav_data_path

                    ; ***************************************************
                    ;  Now write Port=X
                    ; ***************************************************
                    .if [fav_data_port] != FALSE
                        invoke  atodw, ADDR fav_data_port
                        .if eax != HTTP_DEFAULT_PORT
                            ; New line after Path=X
                            invoke  StrCat, [pMem], ADDR crlf
                            invoke  StrCat, [pMem], ADDR fav_port
                            invoke  StrCat, [pMem], ADDR fav_data_port
                        .endif
                    .endif

                    ; ***************************************************
                    ;  Now write DirectoryBrowsing=X
                    ; ***************************************************
                    .if [favDirBrowse] == TRUE
                        ; New line after Port=X
                        invoke  StrCat, [pMem], ADDR crlf
                        invoke  StrCat, [pMem], ADDR fav_dirbrowse
                        invoke  StrCat, [pMem], ADDR true
                    .endif

                    ; ***************************************************
                    ;  Now write BindAddress=X
                    ; ***************************************************
                    .if byte ptr [fav_ip_bind_address] != 0
                        ; New line after Port=X
                        invoke  StrCat, [pMem], ADDR crlf
                        invoke  StrCat, [pMem], ADDR fav_bind_address
                        invoke  StrCat, [pMem], ADDR fav_ip_bind_address
                    .endif

                    ; Write file
                    invoke  SetFilePointer, [hIniFile], 0, 0, FILE_BEGIN
                    invoke  WriteFile, [hIniFile], [pMem], 1, ADDR numb, 0  ; Write a dummy byte, quick check to see if still acccess to file
                    or      eax, eax
                    jz      @@FILE_ERROR                                    ; But if no access, than we see it now, rather than first truncating file :)
                    invoke  SetFilePointer, [hIniFile], 0, 0, FILE_BEGIN
                    invoke  SetEndOfFile, [hIniFile]
                    invoke  StrLen, [pMem]
                    lea     edx, [numb]
                    invoke  WriteFile, [hIniFile], [pMem], eax, edx, 0
                    invoke  GlobalFree, [pMem]
                    jmp     @@EXIT
                @@MEM_ERROR:
                    invoke  MessageBox, [ghwndDlg], ADDR no_memory, ADDR app_name, MB_ICONSTOP
                    jmp     @@EXIT
                @@FILE_ERROR:
                    invoke  GlobalFree, [pMem]
                    invoke  MessageBox, [ghwndDlg], ADDR favNoFavFileCreated, ADDR app_name, MB_ICONSTOP
                .endif
            .endif
        .elseif ax == IDM_REMFAV
            .if [hIniFile] == -1 || [favReadOnly] == TRUE
                ; Tell user 'mereo.cfg' can't be edited because there was no access and thus no favorites can be read/added/removed
                invoke  MessageBox, [hwndDlg], ADDR favNoFavFileCreated, ADDR app_name, MB_ICONEXCLAMATION
            .else
                ; Ask user to select favorite to remove
                invoke  DialogBoxParam, [hInstance], ID_REMFAV_DIALOG, [hwndDlg], ADDR RemFavDialogProc, 0
                ; Remove it from 'mereo.cfg'
                .if eax != -1
                    invoke  GetFileSize, [hIniFile], 0
                    cmp     eax, -1
                    jz      @@FILE_ERROR
                    mov     [iniFileSize], eax
                    inc     eax             ; One extra to terminate with NULL (easy to use StrLen on it now)
                    invoke  GlobalAlloc, GPTR, eax
                    or      eax, eax
                    jz      @@MEM_ERROR
                    mov     [pMem], eax
                    invoke  SetFilePointer, [hIniFile], 0, 0, FILE_BEGIN
                    invoke  ReadFile, [hIniFile], [pMem], [iniFileSize], ADDR numb, 0
                    or      eax, eax
                    jz      @@FILE_ERROR
                    mov     edi, [pMem]

                    ; Prepare [FavoriteXX] search
                    invoke  wsprintf, ADDR fav_buffer, ADDR fav_number, [favRemoveNumber]

                    ; Check if favorite found
                    invoke  instringi, [pMem], ADDR fav_buffer
                    cmp     eax, -1
                    jz      @@REMFAV_FAV_NOT_FOUND

                    add     eax, [pMem]
                    ; EAX = Offset of where next favorites are going to be written (after the one to be deleted)
                    mov     [fav_deletePos], eax

                    mov     byte ptr [fav_buffer+9], 0
                    mov     ecx, eax
                    inc     ecx
                    invoke  instringi, ecx, ADDR fav_buffer
                    .if eax != -1
                        add     eax, [fav_deletePos]
                        inc     eax
                        invoke  StrCpy, [fav_deletePos], eax
                    .else
                        mov     eax, [fav_deletePos]
                        mov     byte ptr [eax], 0
                        invoke  StrLen, [pMem]
                        mov     ecx, eax
                        mov     edi, eax
                        add     edi, [pMem]
                        dec     edi
                        .while byte ptr [edi] == CR || byte ptr [edi] == LF
                            dec     edi
                        .endw
                        inc     edi
                        mov     byte ptr [edi], 0
                    .endif
                    ; EAX = Offset of next favorites after the one to be deleted

                @@REMFAV_WRITE_FAVS:
                    invoke  SetFilePointer, [hIniFile], 0, 0, FILE_BEGIN
                    invoke  SetEndOfFile, [hIniFile]
                    invoke  StrLen, [pMem]
                    lea     edx, [numb]
                    invoke  WriteFile, [hIniFile], [pMem], eax, edx, 0
                    or      eax, eax
                    jz      @@FILE_ERROR
                    jmp     @@REMFAV_FREE_MEM
                @@REMFAV_FAV_NOT_FOUND:
                    invoke  MessageBox, [hwndDlg], ADDR favNotFound, ADDR app_name, MB_ICONSTOP
                @@REMFAV_FREE_MEM:
                    invoke  GlobalFree, [pMem]
                .endif
            .endif
        .elseif eax == IDC_AUTOSTART
            invoke  SendMessage, [hAutoStart], BM_GETCHECK, 0, 0
            ; Check if checkbox is checked
            .if eax == BST_CHECKED
                ; Uncheck
                invoke  SendMessage, [hAutoStartCurrent], BM_SETCHECK, BST_UNCHECKED, 0
                invoke  SendMessage, [hAutoStartAll], BM_SETCHECK, BST_UNCHECKED, 0
                invoke  EnableWindow, [hAutoStartCurrent], FALSE
                invoke  EnableWindow, [hAutoStartAll], FALSE
                invoke  SendMessage, [hAutoStart], BM_SETCHECK, BST_UNCHECKED, 0
                ; Set settings
                invoke  SetAutoStartSetting, 0, TRUE
                .if eax == 0
                    invoke  MessageBox, [ghwndDlg], ADDR autostart_done, ADDR app_name, MB_ICONINFORMATION
                    jmp     @@EXIT
                .elseif eax == -1
                    invoke  MessageBox, [ghwndDlg], ADDR autostart_error, ADDR app_name, MB_ICONSTOP
                .endif
            .else
                ; Check
                invoke  SendMessage, [hAutoStartCurrent], BM_SETCHECK, BST_CHECKED, 0
                invoke  EnableWindow, [hAutoStartCurrent], TRUE
                invoke  EnableWindow, [hAutoStartAll], TRUE
                invoke  SendMessage, [hAutoStart], BM_SETCHECK, BST_CHECKED, 0
                ; Set settings
                invoke  SetAutoStartSetting, HKEY_CURRENT_USER, FALSE
                .if eax == 0
                    invoke  MessageBox, [ghwndDlg], ADDR autostart_done, ADDR app_name, MB_ICONINFORMATION
                    jmp     @@EXIT
                .elseif eax == -1
                    invoke  MessageBox, [ghwndDlg], ADDR autostart_error, ADDR app_name, MB_ICONSTOP
                .endif
                ; Uncheck
                invoke  SendMessage, [hAutoStartCurrent], BM_SETCHECK, BST_UNCHECKED, 0
                invoke  SendMessage, [hAutoStartAll], BM_SETCHECK, BST_UNCHECKED, 0
                invoke  EnableWindow, [hAutoStartCurrent], FALSE
                invoke  EnableWindow, [hAutoStartAll], FALSE
                invoke  SendMessage, [hAutoStart], BM_SETCHECK, BST_UNCHECKED, 0
            .endif
        .elseif eax == IDC_AUTOSTART_CURRENT
            invoke  SetFocus, 0
            ; Check if checkbox is checked
            invoke  SendMessage, [hAutoStartCurrent], BM_GETCHECK, 0, 0
            .if eax == BST_UNCHECKED
                ; Check Current User
                invoke  SendMessage, [hAutoStartCurrent], BM_SETCHECK, BST_CHECKED, 0
                ; Uncheck All Users
                invoke  SendMessage, [hAutoStartAll], BM_SETCHECK, BST_UNCHECKED, 0
                ; Set new startup setting
                invoke  SetAutoStartSetting, HKEY_CURRENT_USER, FALSE
                .if eax == 0
                    invoke  MessageBox, [ghwndDlg], ADDR autostart_done, ADDR app_name, MB_ICONINFORMATION
                    jmp     @@EXIT
                .elseif eax == -1
                    invoke  MessageBox, [ghwndDlg], ADDR autostart_error, ADDR app_name, MB_ICONSTOP
                .endif
                ; Check All Users
                invoke  SendMessage, [hAutoStartAll], BM_SETCHECK, BST_CHECKED, 0
                ; Uncheck Current User
                invoke  SendMessage, [hAutoStartCurrent], BM_SETCHECK, BST_UNCHECKED, 0
            .endif
        .elseif eax == IDC_AUTOSTART_ALL
            invoke  SetFocus, 0
            ; Check if checkbox is checked
            invoke  SendMessage, [hAutoStartAll], BM_GETCHECK, 0, 0
            .if eax == BST_UNCHECKED
                ; Check All Users
                invoke  SendMessage, [hAutoStartAll], BM_SETCHECK, BST_CHECKED, 0
                ; Uncheck Current User
                invoke  SendMessage, [hAutoStartCurrent], BM_SETCHECK, BST_UNCHECKED, 0
                ; Set new startup setting
                invoke  SetAutoStartSetting, HKEY_LOCAL_MACHINE, FALSE
                .if eax == 0
                    invoke  MessageBox, [ghwndDlg], ADDR autostart_done, ADDR app_name, MB_ICONINFORMATION
                    jmp     @@EXIT
                .elseif eax == -1
                    invoke  MessageBox, [ghwndDlg], ADDR autostart_error, ADDR app_name, MB_ICONSTOP
                .endif
                ; Check Current User
                invoke  SendMessage, [hAutoStartCurrent], BM_SETCHECK, BST_CHECKED, 0
                ; Uncheck All Users
                invoke  SendMessage, [hAutoStartAll], BM_SETCHECK, BST_UNCHECKED, 0
            .endif
        .elseif ax == IDC_DIRBROWSING
            ; Check if checkbox is checked
            invoke  SendMessage, [hDirBrowsing], BM_GETCHECK, 0, 0
            .if eax == BST_CHECKED
                ; Uncheck
                invoke  SendMessage, [hDirBrowsing], BM_SETCHECK, BST_UNCHECKED, 0
            .else
                ; Check
                invoke  SendMessage, [hDirBrowsing], BM_SETCHECK, BST_CHECKED, 0
            .endif
        .elseif ax == ID_OPEN_WEBROOT
            invoke  GetDlgItemText, [hwndDlg], IDE_ROOT, ADDR root_openBuffer, 4096
            invoke  GetFileAttributes, ADDR root_openBuffer
            cmp     eax, -1
            jz      @@OPEN_NOT_FOUND
            and     eax, 16 ; Directory check...
            jz      @@OPEN_NOT_A_DIRECTORY
            invoke  CreateThread, 0, 0, ADDR OpenWebrootThread, ADDR root_openBuffer, 0, ADDR ThreadID
            jmp     @@EXIT

        @@OPEN_NOT_FOUND:
            invoke  MessageBox, [hwndDlg], ADDR root_openNotFound, ADDR app_name, MB_ICONEXCLAMATION
            jmp     @@EXIT

        @@OPEN_NOT_A_DIRECTORY:
            invoke  MessageBox, [hwndDlg], ADDR root_openNotDirError, ADDR app_name, MB_ICONEXCLAMATION
            jmp     @@EXIT
        .elseif ax == ID_BIND_ADDRESS_CLR
            invoke  SendMessage, [hBindAddress], WM_SETTEXT, 0, ADDR szNull
        .elseif ax == IDM_HOST
            ; Check what charset is currently set in the configuration file. This will be
            ; used later on to prevent writing the charset to the file if the charset to
            ; be written is the same as the one already set.
            invoke  cfg_ParseString, [hIniFile], ADDR cfg_charset_header
            xor     esi, esi ; Indicate no charset header has been found yet
            .if eax != 0
                mov     ebx, eax ; Free this memory block later
                mov     esi, ecx ; Value pointer
            .endif

            invoke  GetDlgItemText, [hwndDlg], ID_MAIN_CHARSET, ADDR str_charset, 256 ; including NULL character
            test    eax, eax
            jnz     @F
            invoke  SetDlgItemText, [hwndDlg], ID_MAIN_CHARSET, ADDR charset_standard
            invoke  StrCpy, ADDR str_charset, ADDR charset_standard
            jmp     @@DONT_SAVE_CHARSET

            @@:
            .if esi != 0
                invoke  StrCmpI, esi, ADDR str_charset
            .endif
            .if eax != 0 || esi == 0
                invoke  cfg_SaveString, [hwndDlg], [hIniFile], ADDR cfg_charset_header, ADDR str_charset
            .endif
            @@DONT_SAVE_CHARSET:
            .if esi != 0
                invoke  GlobalFree, ebx ; free charset
            .endif
            invoke  GetDlgItemText, [hwndDlg], IDE_ROOT, ADDR host_path, 4096
            test    eax, eax
            jz      @@NOT_VALID_ROOT
            invoke  CheckForDDots, ADDR host_path
            cmp     eax, 1
            jz      @@NOT_VALID_ROOT

            @@:
            invoke  GetFileAttributes, ADDR host_path
            cmp     eax, -1
            jz      @@NOT_VALID_ROOT
            and     eax, 10h
            jz      @@NOT_A_DIRECTORY
            invoke  GetDlgItemInt, [hwndDlg], ID_MAIN_PORT, ADDR int_ok, FALSE
            cmp     [int_ok], TRUE
            jnz     @@INVALID_PORT
            test    eax, eax
            jz      @@INVALID_PORT
            cmp     eax, 0FFFFh
            ja      @@INVALID_PORT
            mov     [HTTP_PORT], eax

            ; Create the server socket
            invoke  socket, AF_INET, SOCK_STREAM, IPPROTO_TCP
            .if eax == INVALID_SOCKET
                .if [ArgH] == TRUE
                    mov     [ArgH], FALSE
                .else
                    invoke  WSAHandleError, [hwndDlg], ADDR host_socket_create_error
                .endif
                jmp     @@EXIT
            .endif
            mov     [hServerSock], eax
            invoke  ClearBuffer, ADDR serversock, SIZEOF serversock
            mov     [serversock.sin_family], AF_INET
            invoke  htons, [HTTP_PORT]
            mov     [serversock.sin_port], ax
            invoke  WSAAsyncSelect, [hServerSock], [hwndDlg], WM_SOCKET, FD_ACCEPT
            .if eax != 0
                .if [ArgH] == TRUE
                    mov     [ArgH], FALSE
                .else
                    invoke  WSAHandleError, [hwndDlg], ADDR host_async_error
                .endif
                jmp     @@EXIT
            .endif

            ; Bind the server socket
            invoke  bind, [hServerSock], ADDR serversock, SIZEOF serversock
            .if eax != 0
                .if [ArgH] == TRUE
                    mov     [ArgH], FALSE
                .else
                    invoke  WSAHandleError, [hwndDlg], ADDR host_bind_address_invalid
                .endif
                jmp     @@EXIT
            .endif

            ; Put the server socket in listening state
            invoke  listen, [hServerSock], SOMAXCONN
            .if eax != 0
                .if [ArgH] == TRUE
                    mov     [ArgH], FALSE
                .else
                    invoke  WSAHandleError, [hwndDlg], ADDR host_socket_state_error
                .endif
                jmp     @@EXIT
            .endif

            ; If all went well, set status message and change menu items
            invoke  LoadIcon, [hInstance], ID_NOTAREA_ICON_RUNNING
            mov     [notarea_icon_data.hIcon], eax
            push    eax
            invoke  Shell_NotifyIcon, NIM_MODIFY, ADDR notarea_icon_data
            pop     eax
            invoke  DestroyIcon, eax
            invoke  EnableMenuItem, [hMenu], IDM_HOST, MF_GRAYED
            invoke  EnableMenuItem, [hMenu], IDM_STOPHOST, MF_ENABLED
            invoke  EnableMenuItem, [hMenu], IDM_SELECTDIRFILE, MF_GRAYED
            invoke  EnableMenuItem, [hPopupMenu], IDM_HOST, MF_GRAYED
            invoke  EnableMenuItem, [hPopupMenu], IDM_STOPHOST, MF_ENABLED
            invoke  GetDlgItem, [hwndDlg], IDE_ROOT
            invoke  EnableWindow, eax, FALSE
            invoke  GetDlgItem, [hwndDlg], ID_MAIN_CHARSET
            invoke  EnableWindow, eax, FALSE
            invoke  GetDlgItem, [hwndDlg], ID_MAIN_PORT
            invoke  EnableWindow, eax, FALSE
            invoke  EnableWindow, [hBindAddress], FALSE
            invoke  GetDlgItem, [hwndDlg], ID_BIND_ADDRESS_CLR
            invoke  EnableWindow, eax, FALSE
            invoke  SendMessage, [hStatus], SB_SETTEXT, 1, ADDR status_running
            mov     [status_bRunning], TRUE
            jmp     @@EXIT

        @@NOT_VALID_ROOT:
            .if [ArgH] == TRUE
                mov     [ArgH], FALSE
            .else
                invoke  MessageBox, [hwndDlg], ADDR host_path_invalid, ADDR app_name, MB_ICONSTOP
            .endif
            jmp     @@EXIT

        @@NOT_A_DIRECTORY:
            .if [ArgH] == TRUE
                mov     [ArgH], FALSE
            .else
                invoke  MessageBox, [hwndDlg], ADDR host_path_not_a_dir, ADDR app_name, MB_ICONSTOP
            .endif
            jmp     @@EXIT

        @@INVALID_PORT:
            .if [ArgH] == TRUE
                mov     [ArgH], FALSE
            .else
                invoke  MessageBox, [hwndDlg], ADDR invalid_port, ADDR app_name, MB_ICONSTOP
            .endif
        .elseif ax == IDM_STOPHOST
            .if [favAbortFlag] == TRUE
                mov     [favAbortFlag], FALSE
                mov     eax, TRUE
            .else
                invoke  AreYouSure, [hwndDlg]
            .endif
            .if eax == TRUE
                invoke  closesocket, [hServerSock]
                invoke  CloseAllConnectionsAndCleanup

            @@:
                invoke  PeekMessage, ADDR pm_msg, [hwndDlg], WM_SOCKET, WM_SOCKET, PM_REMOVE
                cmp     eax, 1
                jz      @B
                ; Set status message and change menu items
                invoke  LoadIcon, [hInstance], ID_NOTAREA_ICON_STOPPED
                mov     [notarea_icon_data.hIcon], eax
                push    eax
                invoke  Shell_NotifyIcon, NIM_MODIFY, ADDR notarea_icon_data
                pop     eax
                invoke  DestroyIcon, eax
                invoke  EnableMenuItem, [hMenu], IDM_HOST, MF_ENABLED
                invoke  EnableMenuItem, [hMenu], IDM_STOPHOST, MF_GRAYED
                invoke  EnableMenuItem, [hMenu], IDM_SELECTDIRFILE, MF_ENABLED
                invoke  EnableMenuItem, [hPopupMenu], IDM_HOST, MF_ENABLED
                invoke  EnableMenuItem, [hPopupMenu], IDM_STOPHOST, MF_GRAYED
                invoke  GetDlgItem, [hwndDlg], IDE_ROOT
                invoke  EnableWindow, eax, TRUE
                invoke  GetDlgItem, [hwndDlg], ID_MAIN_CHARSET
                invoke  EnableWindow, eax, TRUE
                invoke  GetDlgItem, [hwndDlg], ID_MAIN_PORT
                invoke  EnableWindow, eax, TRUE
                invoke  EnableWindow, [hBindAddress], TRUE
                invoke  GetDlgItem, [hwndDlg], ID_BIND_ADDRESS_CLR
                invoke  EnableWindow, eax, TRUE
                ; update main window statusbar
                invoke  SendMessage, [hStatus], SB_SETTEXT, 1, ADDR status_notrunning
                invoke  wsprintf, ADDR status_buffer, ADDR status_conncount, 0
                invoke  SendMessage, [hStatus], SB_SETTEXT, 2, ADDR status_buffer
                ; update notification area tooltip
                invoke  StrCpy, ADDR notarea_icon_data.szTip, ADDR app_name
                invoke  Shell_NotifyIcon, NIM_MODIFY, ADDR notarea_icon_data
                ; set global server status
                mov     [status_bRunning], FALSE
            .endif
        .elseif ax == IDM_SELECTDIRFILE
            invoke  SHGetOpenDirName, [hwndDlg], ADDR browse_buffer
            or      eax, eax
            jz      @@EXIT
            invoke  SetDlgItemText, [hwndDlg], IDE_ROOT, ADDR browse_buffer
        .elseif ax == IDM_HELP
            invoke  GetAbsPathFromBase, ADDR help_filebuffer, ADDR help_filename
            invoke  GetFileAttributes, ADDR help_filebuffer
            .if eax != -1
                invoke  ShellExecute, 0, ADDR help_operation, ADDR help_filebuffer, 0, 0, SW_SHOWMAXIMIZED
                .if eax == SE_ERR_ACCESSDENIED || eax == SE_ERR_DLLNOTFOUND || eax == SE_ERR_NOASSOC
                    invoke  MessageBox, [hwndDlg], ADDR help_nobrowser, ADDR sorry_caption, MB_ICONEXCLAMATION
                .endif
            .else
                invoke  MessageBox, [hwndDlg], ADDR help_filenotfound, ADDR sorry_caption, MB_ICONSTOP
            .endif
        .elseif ax == IDM_CONTACT
            invoke  ShellExecute, 0, ADDR help_operation, ADDR help_contact, 0, 0, SW_SHOWNORMAL
            .if eax == SE_ERR_ACCESSDENIED || eax == SE_ERR_DLLNOTFOUND || eax == SE_ERR_NOASSOC
                invoke  MessageBox, [hwndDlg], ADDR help_contact_if_not_working, ADDR app_name, MB_ICONEXCLAMATION
            .endif
        .elseif ax == IDM_ABOUT
            invoke  DialogBoxParam, [hInstance], ID_ABOUT_DIALOG, [hwndDlg], ADDR AboutDialogProc, 0
        .elseif ax == IDM_EXIT
            invoke  SendMessage, [hwndDlg], WM_CLOSE, 0, 0
        .elseif ax == IDM_NOTAREA_EXIT
            invoke  SendMessage, [hwndDlg], WM_CLOSE, 0, 0
        .endif
    .elseif eax == WM_SOCKET
        mov     eax, [lParam]
        .if ax == FD_ACCEPT
            invoke  accept, [hServerSock], ADDR client, ADDR clientlen
            mov     [hAcceptSocket], eax
            invoke  inet_ntoa, dword ptr [client.sin_addr]
            mov     [accept_incoming_addr], eax

            ; Check if incoming IP address matches the one which the user has given access to
            invoke  SendMessage, [hBindAddress], WM_GETTEXT, SIZEOF ip_bind_buffer, ADDR ip_bind_buffer
            .if eax == 0
                ; Bind address not set
                jmp     @@CONNECTION_ACCEPTED
            .endif
            invoke  inet_addr, ADDR ip_bind_buffer
            .if eax == INADDR_NONE
                ; Bind address is not a valid IP address
                jmp     @@CONNECTION_ACCEPTED
            .endif
            invoke  StrCmp, ADDR ip_bind_buffer, [accept_incoming_addr]
            or      eax, eax
            jz      @@CONNECTION_ACCEPTED
            ; It doesn't match, close the socket
            inc     [gThreadCount]
            invoke  CreateThread, 0, 0, ADDR CloseConnectionThread, [hAcceptSocket], 0, ADDR temp_thread_id
            jmp     @@EXIT

          @@CONNECTION_ACCEPTED:
            ; Logging
            .if [prefs_logenable] == 1
                invoke  StrLen, [http_logBuffer]
                mov     ecx, [prefs_logmem]
                sub     ecx, LOGMEM_LINELIMIT
                cmp     eax, ecx
                jb      @@LOG
                cmp     [logfullflag], TRUE
                jz      @@DONT_LOG
                mov     [logfullflag], TRUE
                invoke  wsprintf, ADDR logstatus, ADDR logstatusmsg, ADDR logfull
                invoke  SetDlgItemText, [hwndDlg], IDT_LOG_STATUS, ADDR logstatus
                jmp     @@DONT_LOG
              @@LOG:
                invoke  StrCat, [http_logBuffer], [accept_incoming_addr]
                invoke  GetDateFormat, LOCALE_USER_DEFAULT, 0, 0, ADDR DateFormat, ADDR CurrentDateTime, 80

                invoke  StrLen, [accept_incoming_addr]
                sub     eax, 18
                not     eax
                inc     eax
                mov     ebx, eax
                .while ebx != 0
                    dec     ebx
                    invoke  StrCat, [http_logBuffer], ADDR WhitespacePadding
                .endw

                invoke  StrCat, [http_logBuffer], ADDR CurrentDateTime
                invoke  GetTimeFormat, LOCALE_USER_DEFAULT, 0, 0, ADDR TimeFormat, ADDR CurrentDateTime, 80
                invoke  StrCat, [http_logBuffer], ADDR CurrentDateTime
                .if [hLogDialog] != 0
                    invoke  SetDlgItemText, [hLogDialog], ID_LOG_TEXT, [http_logBuffer]
                    invoke  GetDlgItem, [hLogDialog], ID_LOG_TEXT
                    invoke  SendMessage, eax, WM_VSCROLL, SB_BOTTOM, 0
                .endif
              @@DONT_LOG:
            .endif

            ; Select the events to be notified of
            invoke  WSAAsyncSelect, [hAcceptSocket], [hwndDlg], WM_SOCKET, FD_READ or FD_CLOSE
        .elseif ax == FD_READ
            invoke  FindStruct, [ConnInfoStructs], [wParam], 0, 0
            cmp     eax, -1
            jz      @F ; If found, check if thread is running
            assume eax:PTR ConnInfo
            cmp     [eax].ThreadRunning, TRUE
            assume eax:NOTHING
            jz      @@NOTIFY_THREAD_AND_EXIT ; Thread will see that it can receive something

            push    eax
            invoke  CreateThread, 0, HANDLECONNECTIONTHREAD_STACKSIZE, ADDR HandleConnectionThread, eax, CREATE_SUSPENDED, ADDR temp_thread_id
            test    eax, eax
            mov     ecx, eax
            pop     eax
            jz      @@REMOVE_STRUCTURE_AND_EXIT
            assume eax:PTR ConnInfo
            mov     [eax].hThread, ecx
            push    eax
            invoke  SetEvent, [eax].hEventStart
            pop     eax
            invoke  ResumeThread, [eax].hThread
            assume eax:NOTHING
            jmp     @@INCREASE_THREADCOUNT_AND_EXIT

        @@NOTIFY_THREAD_AND_EXIT:
            assume eax:PTR ConnInfo
            mov     [eax].RecvData, TRUE
            invoke  SetEvent, [eax].hEventStart
            assume eax:NOTHING
            jmp     @@EXIT

        @@:
            invoke  CreateEvent, 0, FALSE, FALSE, 0
            push    eax ;hEventStart
            invoke  WSAEventSelect, [wParam], eax, FD_READ
            pop     eax ;hEventStart
            invoke  AddStruct, [ConnInfoStructs], [wParam], 0, eax, 0 ; Specify 0 for threadhandle, we haven't got it yet
            cmp     eax, -1
            jz      @@STATUS503
            ; EAX is base address of new structure
            push    eax
            invoke  CreateThread, 0, HANDLECONNECTIONTHREAD_STACKSIZE, ADDR HandleConnectionThread, eax, CREATE_SUSPENDED, ADDR temp_thread_id
            test    eax, eax
            pop     ecx
            jz      @@REMOVE_STRUCTURE_AND_EXIT
            assume ecx:PTR ConnInfo
            mov     [ecx].hThread, eax
            push    ecx
            invoke  SetEvent, [ecx].hEventStart
            pop     ecx
            invoke  ResumeThread, [ecx].hThread ; when completely filled structure, let the thread begin :-)
            assume ecx:NOTHING
            ; jmp     @@INCREASE_THREADCOUNT_AND_EXIT

        @@INCREASE_THREADCOUNT_AND_EXIT:
            ; Increase thread count
            inc     [gThreadCount]
            jmp     @@EXIT

        @@REMOVE_STRUCTURE_AND_EXIT:
            ; ECX is base address of new structure, but thread failed, so remove the structure
            invoke  RemoveStruct, ecx
            jmp     @@EXIT

        @@STATUS503:
            invoke  FindStruct, [ConnInfoStructs], [wParam], 0, 0
            cmp     eax, -1
            jz      @F
            invoke  RemoveStruct, eax

        @@:
            invoke  closesocket, [wParam]
        .elseif ax == FD_CLOSE
            invoke  FindStruct, [ConnInfoStructs], [wParam], 0, 0
            cmp     eax, -1
            jz      @F ; Rare case, but we don't care since memory is already freed if not found :)
            assume eax:PTR ConnInfo
            cmp     [eax].ThreadRunning, TRUE
            jnz     @@REMOVE_STRUCT_AND_CLOSE
            mov     [eax].fd_close, TRUE
            assume eax:NOTHING
            jmp     @F

            @@REMOVE_STRUCT_AND_CLOSE:
            invoke  RemoveStruct, eax

            @@:
            invoke  closesocket, [wParam]
        .endif
    .elseif eax == WM_LOG_READ_ERROR
        invoke  StrLen, [http_logBuffer]
        mov     ecx, [prefs_logmem]
        sub     ecx, LOGMEM_LINELIMIT
        cmp     eax, ecx
        jb      @F
        cmp     [logfullflag], TRUE
        jz      @@EXIT
        mov     [logfullflag], TRUE
        jmp     @@EXIT

        @@:
        invoke  StrCat, [http_logBuffer], ADDR status_read_error

        mov     ebx, 8
        .while ebx != 0
            dec     ebx
            invoke  StrCat, [http_logBuffer], ADDR WhitespacePadding
        .endw

        invoke  GetDateFormat, LOCALE_USER_DEFAULT, 0, 0, ADDR DateFormat, ADDR CurrentDateTime, 80
        invoke  StrCat, [http_logBuffer], ADDR CurrentDateTime
        invoke  GetTimeFormat, LOCALE_USER_DEFAULT, 0, 0, ADDR TimeFormat, ADDR CurrentDateTime, 80
        invoke  StrCat, [http_logBuffer], ADDR CurrentDateTime
        .if [hLogDialog] != 0
            invoke  SetDlgItemText, [hLogDialog], ID_LOG_TEXT, [http_logBuffer]
        .endif
    .elseif eax == WM_CLOSE
            invoke  AreYouSure, [hwndDlg]
            .if eax == TRUE
                invoke  closesocket, [hServerSock]
                invoke  CloseAllConnectionsAndCleanup
                .if [hIniFile] != -1
                    invoke  GetFileSize, [hIniFile], 0
                    mov     [iniFileSize], eax
                    invoke  SetFilePointer, [hIniFile], 0, 0, FILE_BEGIN
                    invoke  ReadFile, [hIniFile], ADDR favTempBuf, [iniFileSize], ADDR numb, 0
                    invoke  CloseHandle, [hIniFile]

                    ; Check if the standard charset was set in the file (and nothing else)
                    mov     [charsetStandard], FALSE
                    .if [iniFileSize] == (SIZEOF cfg_charset_header + SIZEOF charset_standard)
                        invoke  StrCpy, ADDR fav_buffer, ADDR cfg_charset_header
                        invoke  StrCat, ADDR fav_buffer, ADDR charset_standard
                        invoke  StrCmpIn, ADDR favTempBuf, ADDR fav_buffer, (SIZEOF charset_standard-1)
                        .if eax == 0
                            mov     [charsetStandard], TRUE
                        .endif
                    .endif

                    .if [iniFileSize] == 0 || [charsetStandard] == TRUE
                        ; If 'mereo.cfg' is 0 bytes (empty) then try to delete it
                        invoke  DeleteFile, ADDR iniFilenameBuf
                    .endif
                .endif
                invoke  Shell_NotifyIcon, NIM_DELETE, ADDR notarea_icon_data
                invoke  KillTimer, [hwndDlg], ID_TIMER1
                invoke  KillTimer, [hwndDlg], ID_TIMER2
                invoke  EndDialog, [hwndDlg], 0
            .else
            @@:
                invoke  PeekMessage, ADDR pm_msg, [hwndDlg], WM_CLOSE, WM_CLOSE, PM_REMOVE
                cmp     eax, 1
                jz      @B
                mov     eax, 1
                ret
            .endif
    .endif
    jmp     @@EXIT

    @@VIEWLOG:
    .if [prefs_logenable] == 0
        jmp     @@ERROR_LOG_DISABLED
    .else
        invoke  DialogBoxParam, [hInstance], ID_LOG_DIALOG, [hwndDlg], ADDR LogDialogProc, 0
    .endif
    jmp     @@EXIT

    @@CLEARLOG:
    .if [prefs_logenable] == 0
        jmp     @@ERROR_LOG_DISABLED
    .else
        mov     [logfullflag], FALSE
        invoke  ClearBuffer, [http_logBuffer], [prefs_logmem]
        invoke  MessageBox, [hwndDlg], ADDR logcleared, ADDR app_name, MB_ICONINFORMATION
    .endif
    jmp     @@EXIT

    @@SAVELOG:
    .if [prefs_logenable] == 0
        jmp     @@ERROR_LOG_DISABLED
    .else
        invoke  SaveLog, 0
    .endif
    jmp        @@EXIT

    @@ERROR_LOG_DISABLED:
    invoke  MessageBox, [hwndDlg], ADDR error_logging_disabled, ADDR sorry_caption, MB_ICONEXCLAMATION
    ; jmp     @@EXIT

    @@EXIT:
    xor     eax, eax
    ret
MainDialogProc endp
