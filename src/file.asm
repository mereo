; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

; Returns the absolute path for the relative file path
; calculated from the base of the program itself.
GetAbsPathFromBase proc uses edi lpDestBuffer:DWORD, lpFilePath:DWORD
    invoke  GetModuleFileName, 0, ADDR GetAbsPathFromBase_temp, 2048
    push    edi
    invoke  StrLen, ADDR GetAbsPathFromBase_temp
    mov     ecx, eax
    mov     edi, OFFSET GetAbsPathFromBase_temp
    add     edi, eax
    mov     eax, '\'
    std
    repnz   scasb
    cld
    inc     edi
    mov     byte ptr [edi+1], 0
  @@:
    invoke  StrCat, ADDR GetAbsPathFromBase_temp, [lpFilePath]
    invoke  StrCpy, [lpDestBuffer], ADDR GetAbsPathFromBase_temp
    ret
GetAbsPathFromBase endp

SetFilePointerLong proc uses edi unsignedLow:DWORD,unsignedHigh:DWORD,fHandle:DWORD
LOCAL temp                  :DWORD
LOCAL temp2                 :DWORD

    mov     edi, [unsignedLow]
    .if     edi > 07FFFFFFFh
        sub     edi, 07FFFFFFFh
        invoke  SetFilePointer, [fHandle], 07FFFFFFFh, 0, FILE_BEGIN
        .if edi < 080000000h
            invoke  SetFilePointer, [fHandle], edi, 0, FILE_CURRENT
        .endif
        .while edi > 07FFFFFFFh
            sub     edi, 07FFFFFFFh
            invoke  SetFilePointer, [fHandle], edi, 0, FILE_CURRENT
        .endw
    .elseif edi == 0
        invoke  SetFilePointer, [fHandle], edi, edi, FILE_BEGIN
    .else
        invoke  SetFilePointer, [fHandle], edi, 0, FILE_BEGIN
    .endif

    mov     edi, [unsignedHigh]
    mov     [temp], edi
    .if edi > 07FFFFFFFh
        sub     [temp], 07FFFFFFFh
        mov     [temp2], 07FFFFFFFh
        invoke  SetFilePointer, [fHandle], 0, ADDR temp2, FILE_CURRENT
        .if edi < 080000000h
            mov     [temp], edi
            invoke  SetFilePointer, [fHandle], 0, ADDR temp, FILE_CURRENT
        .endif
        .while edi > 07FFFFFFFh
            sub     [temp], 07FFFFFFFh
            invoke  SetFilePointer, [fHandle], 0, ADDR temp, FILE_CURRENT
        .endw
    .elseif edi != 0
        invoke  SetFilePointer, [fHandle], 0, ADDR temp, FILE_CURRENT
    .endif
    ret
SetFilePointerLong endp

; Returns the desktop folder location of the current logged on user
; lpBuffer = argument to be used as buffer which receives the path
; e.g. "C:\Documents and Settings\John\Desktop" (without the quotes)
GetDesktopFolder proc lpBuffer:DWORD
LOCAL pidl                  :DWORD

    invoke  SHGetSpecialFolderLocation, 0, CSIDL_DESKTOPDIRECTORY, ADDR pidl
    invoke  SHGetPathFromIDList, [pidl], [lpBuffer]
    ret
GetDesktopFolder endp
