; Mereo - An HTTP server.
; Copyright (C) 2008  Jelle Geerts
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.

PreferencesDialogProc proc uses ebx hwndDlg:DWORD, uMsg:DWORD, wParam:DWORD, lParam:DWORD
LOCAL hwndItem              :DWORD
LOCAL bSuccess              :BOOL

    .if [uMsg] == WM_INITDIALOG
        ; Set the window icon
        invoke  SendMessage, [hwndDlg], WM_SETICON, ICON_BIG, [hMainIcon]

        ; *'new_cfg_item-init'*

        ; 'IDC_SETTINGS_DIRBROWSE_FORCE'
        ; Check the current state of the 'prefs_dirbrowse_force' setting
        .if [prefs_dirbrowse_force] == 1
            invoke      GetDlgItem, [hwndDlg], IDC_SETTINGS_DIRBROWSE_FORCE
            invoke      SendMessage, eax, BM_SETCHECK, BST_CHECKED, 0
        .endif
        ; END OF 'IDC_SETTINGS_DIRBROWSE_FORCE'

        ; 'IDC_SETTINGS_LOGENABLE'
        ; Check the current state of the 'prefs_dirbrowse_force' setting
        .if [prefs_logenable] == 1
            invoke      GetDlgItem, [hwndDlg], IDC_SETTINGS_LOGENABLE
            invoke      SendMessage, eax, BM_SETCHECK, BST_CHECKED, 0
        .endif
        ; END OF 'IDC_SETTINGS_LOGENABLE'

        ; 'IDE_SETTINGS_MAXTHREADS'
        movzx       eax, [prefs_maxthreads]
        invoke      SetDlgItemInt, [hwndDlg], IDE_SETTINGS_MAXTHREADS, eax, 0
        ; END OF 'IDE_SETTINGS_MAXTHREADS'

        ; 'IDE_SETTINGS_LOGMEM'
        invoke      SetDlgItemInt, [hwndDlg], IDE_SETTINGS_LOGMEM, [prefs_logmem], 0
        invoke      GetDlgItem, [hwndDlg], IDE_SETTINGS_LOGMEM
        movzx       ecx, [status_bRunning]
        not         cl
        and         cl, 1
        invoke      EnableWindow, eax, ecx
        ; END OF 'IDE_SETTINGS_LOGMEM'
    .elseif [uMsg] == WM_COMMAND
        mov     eax, [wParam]
        .if ax == IDCANCEL
            invoke  SendMessage, [hwndDlg], WM_CLOSE, 0, 0
        .elseif ax != ID_SETTINGS_PREFS_APPLY
            movzx   eax, ax
            invoke  GetDlgItem, [hwndDlg], eax
            mov     [hwndItem], eax
            invoke  SendMessage, eax, BM_GETCHECK, 0, 0
            .if eax == BST_CHECKED
                mov     eax, BST_UNCHECKED
            .elseif eax == BST_UNCHECKED
                mov     eax, BST_CHECKED
            .endif
            invoke  SendMessage, [hwndItem], BM_SETCHECK, eax, 0
        .else ;if ax == ID_SETTINGS_PREFS_APPLY
            ; *'new_cfg_item-apply'*

            ; 'IDC_SETTINGS_DIRBROWSE_FORCE'
            ; Save the setting in the application's configuration file
            invoke  GetDlgItem, [hwndDlg], IDC_SETTINGS_DIRBROWSE_FORCE
            invoke  SendMessage, eax, BM_GETCHECK, 0, 0
            xor     ebx, ebx
            cmp     eax, BST_CHECKED
            setz    bl
            .if [prefs_dirbrowse_force] != bl
                ; Save/update the setting in the configuration file
                invoke  cfg_SaveInteger, [hwndDlg], [hIniFile], ADDR cfg_prefs_db_force_header, ebx
                .if eax != TRUE
                    jmp     @@EXIT_ERROR
                .endif
                ; Now update the setting run-time
                mov     [prefs_dirbrowse_force], bl
            .endif
            ; END OF 'IDC_SETTINGS_DIRBROWSE_FORCE'

            ; 'IDC_SETTINGS_LOGENABLE'
            ; Save the setting in the application's configuration file
            invoke  GetDlgItem, [hwndDlg], IDC_SETTINGS_LOGENABLE
            invoke  SendMessage, eax, BM_GETCHECK, 0, 0
            xor     ebx, ebx
            cmp     eax, BST_CHECKED
            setz    bl
            .if [prefs_logenable] != bl
                ; Save/update the setting in the configuration file
                invoke  cfg_SaveInteger, [hwndDlg], [hIniFile], ADDR cfg_prefs_logenable_header, ebx
                .if eax != TRUE
                    jmp     @@EXIT_ERROR
                .endif
                ; Now update the setting run-time
                mov     [prefs_logenable], bl
                ; Allocate/free memory according to the setting's state
                .if bl == 1
                    invoke  Log_AllocateMemory
                .else
                    invoke  Log_FreeMemory
                .endif
            .endif
            ; END OF 'IDC_SETTINGS_LOGENABLE'

            ; 'IDE_SETTINGS_MAXTHREADS'
            ; Save the setting in the application's configuration file
            invoke  GetDlgItemInt, [hwndDlg], IDE_SETTINGS_MAXTHREADS, ADDR bSuccess, 0
            mov     ebx, eax
            .if [bSuccess] == 0
                invoke  MessageBox, [hwndDlg], ADDR settings_save_e_maxthreads, ADDR sorry_caption, MB_ICONSTOP
                jmp     @@EXIT
            .elseif ebx > MAXTHREADS_LIMIT
                invoke  MessageBox, [hwndDlg], ADDR settings_save_e_mthreadlim, ADDR sorry_caption, MB_ICONSTOP
                jmp     @@EXIT
            .else
                ; EBX contains the entered number
                .if [prefs_maxthreads] != bx
                    ; Save/update the setting in the configuration file
                    invoke  cfg_SaveInteger, [hwndDlg], [hIniFile], ADDR cfg_prefs_maxthreads_header, ebx
                    .if eax != TRUE
                        jmp     @@EXIT_ERROR
                    .endif
                    ; Now update the setting run-time
                    mov     [prefs_maxthreads], bx
                .endif
            .endif
            ; END OF 'IDE_SETTINGS_MAXTHREADS'

            ; 'IDE_SETTINGS_LOGMEM'
            ; Save the setting in the application's configuration file
            invoke  GetDlgItemInt, [hwndDlg], IDE_SETTINGS_LOGMEM, ADDR bSuccess, 0
            mov     ebx, eax
            .if [bSuccess] == 0
                invoke  MessageBox, [hwndDlg], ADDR settings_save_e_logmem, ADDR sorry_caption, MB_ICONSTOP
                jmp     @@EXIT
            .elseif ebx > LOGMEM_LIMIT
                invoke  MessageBox, [hwndDlg], ADDR settings_save_e_logmemlim, ADDR sorry_caption, MB_ICONSTOP
                jmp     @@EXIT
            .else
                ; EBX contains the entered number
                .if [prefs_logmem] != ebx
                    ; Save/update the setting in the configuration file
                    invoke  cfg_SaveInteger, [hwndDlg], [hIniFile], ADDR cfg_prefs_logmem_header, ebx
                    .if eax != TRUE
                        jmp     @@EXIT_ERROR
                    .endif
                    ; Now update the setting run-time
                    mov     [prefs_logmem], ebx
                    ; Reallocate memory for the log, using the new settings
                    invoke  Log_FreeMemory
                    invoke  Log_AllocateMemory
                .endif
            .endif
            ; END OF 'IDE_SETTINGS_LOGMEM'

            ; If all settings we're saved, close the dialog
            invoke  SendMessage, [hwndDlg], WM_CLOSE, 0, 0
            jmp     @@EXIT

            @@EXIT_ERROR:
            invoke  MessageBox, [hwndDlg], ADDR settings_save_error, ADDR app_name, MB_ICONSTOP
        .endif
    .elseif [uMsg] == WM_CLOSE
        ; Check if the settings changed and ask if the user is sure to close the dialog
        invoke  EndDialog, [hwndDlg], 0
    .endif

    @@EXIT:
    xor     eax, eax
    ret
PreferencesDialogProc endp
