Version 1.7.8 at <>:
*******************************************************************************

- Minor updates.

Version 1.7.7 at <Friday, August 22, 2008>:
*******************************************************************************

- Project renamed to Mereo (latin word for "serve as a soldier").
  New URL: http://sourceforge.net/projects/mereo
  Old URL: http://sourceforge.net/projects/httpsv
  Until SourceForge.net processes the name change, use the old URL. Automatic
  updates will not work until SourceForge.net processes the name change.
- If a requested file does not exist, do not append ".htm" or ".html" to see if
  those files exist. Instead, send a 404 error (like other web browsers do).
- Change default character set to UTF-8.
- Support Unicode filenames and directories in the "Directory Browsing"
  feature.
- Support retrieving files with Unicode filenames.

Version 1.7.6 at <Monday, June 30, 2008>:
*******************************************************************************

- Delete 'httpsv.cfg' at exit when only the default character set was found and
  no other settings.
- Various bug fixes.

Version 1.7.5 at <Saturday, April 12, 2008>:
*******************************************************************************

- Fixed HTTP Server's `Preference' dialog crash (which was only known to happen
  in Windows 2000).
- Various bug fixes and performed small cleanups.

Version 1.7.4 at <Sunday, January 20, 2008>:
*******************************************************************************

- Added an option to never automatically check for updates.
- Fixed Windows 95 compatibility.
- Removed various bugs.

Version 1.7.3 at <Sunday, January 20, 2008>:
*******************************************************************************

- Modified system requirements listed in 'help.html'. (SHLWAPI.DLL isn't needed
  anymore.)
- Added a Table of Contents to the FAQ (in the help document).
- Removed 'resource.h', added an INCLUDE path passed on to 'RC.EXE' instead
  since 'resource.h' is part of the MASM assembler package.
- Set up a procedure for adding new configurable items (see 'procedures.txt' in
  the source control root).
- Changed e-mail format in 'readme.txt'.
- Dropped support for the 'Automatic Update' feature for versions of HTTP
  Server pre-1.4.3.
- Removed a typo from the ChangeLog.
- Removed the source codes for pre-1.6.1 versions since after that; sources are
  merged into the main "HTTP Server" package on the SourceForge.net project
  page.

Version 1.7.2 at <Thursday, January 03, 2008>:
*******************************************************************************

- Fixed Windows 95 compatibility.

Version 1.7.1 at <Wednesday, January 02, 2008>:
*******************************************************************************

- Added missing checks to see if logging was enabled/disabled. The application
  could otherwise crash when the user tried to view/save/clear the log.

Version 1.7.0 at <Wednesday, January 02, 2008>:
*******************************************************************************

- Fixed Windows 98 compatibility.
- Strongly reduced the number of page faults per second (the average page fault
  delta is now about zero). (Visit http://en.wikipedia.org/wiki/Page_fault for
  more information about the subject).
- Fixed an error in loading the update preferences for the update preferences
  dialog.

Version 1.6.9 at <Wednesday, January 02, 2008>:
*******************************************************************************

- Added an option to enable/disable logging. ('Settings -> Preferences...')
- Added an option to change the maximum number of threads.
- Changed behaviour of status message 503. No 503 status messages are sent
  anymore when the server is under heavy load.
- Changed maximum number of threads to 1000.
- Fixed various non-critical (they didn't make the application crash so far)
  bugs which had to do with overwriting the CPU's registers.

Version 1.6.8 at <Monday, October 29, 2007>:
*******************************************************************************

- The installer can now create Start Menu shortcuts for "All Users" instead
  of just the "Current User".
- Added two missing source files to the installer.

Version 1.6.7 at <Saturday, October 20, 2007>:
*******************************************************************************

- Added a setting to always show directory contents regardless whether or
  not the file 'index.htm(l)' exists. This setting only applies when the
  'Directory Browsing' feature is turned on. (feature request 1768013)

Version 1.6.6 at <Monday, October 15, 2007>:
*******************************************************************************

- The program can find default pages (index.htm/index.html)
  again. (bug report 1807225)

Version 1.6.5 at <Tuesday, August 14, 2007>:
*******************************************************************************

- Speeded up file transfers.
- The server will now hide the existence of directories
  when directory browsing isn't enabled.
- Added the missing "Directory Browsing" icons.
- Resized the "Updates" (update settings) dialog.
- Bugfix: added an extra check in the update settings dialog to also check
  if the 'Check for updates at program startup' was modified before exiting
  the dialog.

Version 1.6.4 at <Monday, August 13, 2007>:
*******************************************************************************

- Fixed the nasty bug which caused the menu text not to appear
  after minimizing the main window by it's minimize button on the
  window caption.
- Updated the FAQ with a question and answer on "Custom MIME types".
- Prevented file type checking exploits (for when the server would ever
  support scripting), this would not be able ever cause trouble before,
  it's just a prevention.

Version 1.6.2 at <Friday, June 15, 2007>:
*******************************************************************************

- Fixed a bug which would cause a transfer to hang when there was no
  memory to read a file.

Version 1.6.1 at <Tuesday, June 12, 2007>:
*******************************************************************************

- Made the main window a little bit taller because it could give
  problems with screen updates when run on Windows 2000 or XP machines
  running the 'Classic' theme style.

Version 1.6 at <Saturday, June 09, 2007>:
*******************************************************************************

- Included an installer/uninstaller program
- The source code of the program is now splitup into separate files, which
  should make maintaining the source code easier.

Version 1.5.7 at <Friday, June 01, 2007>:
*******************************************************************************

- Added customizable error pages (feature request 1727897).

Version 1.5.6 at <Friday, June 01, 2007>:
*******************************************************************************

- Version number increased (it was still 1.5.5).

Version 1.5.6 at <Thursday, May 31, 2007>:
*******************************************************************************

- Due to cleanup of the code that fills in the 'Favorites' menu, the first item
  in the menu won't work. This is now fixed.

Version 1.5.5 at <Thursday, May 31, 2007>:
*******************************************************************************

- Fixed a bug that would cause a favorite header to be written to the
  configuration file when the 'Add Favorite' dialog was closed by using the
  ESCAPE key instead of the normal 'X' (close) button.
- Changed 'arguments' term to 'configuration' for the auto-start dialog.
  Before, the user had to manually type the arguments but since that isn't
  necessary anymore we can call it 'configuration'.
- Updated the help document screenshots.
- The program didn't tell the user when it couldn't contact the HTTP Server
  website to check for updates, this is now fixed.

Version 1.5.4 at <Thursday, May 31, 2007>:
*******************************************************************************

- When successfully hosted a directory, the ?Clear? button will now be
  disabled.
- Improved the update checking system (it's now more userfriendly).

Version 1.5.3 at <Tuesday, May 29, 2007>:
*******************************************************************************

- Fixed a bug in a commandline parsing function

Version 1.5.2 at <Tuesday, May 29, 2007>:
*******************************************************************************

- Added support to limit outside access to a single IP address. This is called
  a "Bind Address" in the settings (on the main dialog) and in the
  configuration file.
- When manually checking for updates from the "Help" menu, you are now
  notified when there are no updates, before you'd only get a message if
  there is an update available.
- Fixed the padding between a log message and it's date and time.
- The autostart dialog will now copy the information like the webroot and
  hosting port from the main dialog when no autostart settings were found.
- When opening the root directory by clicking 'Open' from the main dialog
  and a file in the same directory as the directory where the root directory
  resided had the same name as the root directory, the file would be opened.
  This is now fixed.
- When the log dialog is shown and a new HTTP request is received, the edit
  control is automatically scrolled downwards.
- Fixed a bug which would not reset the 'Log Full' flag when you have cleared
  the log.
- Fixed several other (less-noticable) bugs

Version 1.5 at Sunday, May 06, 2007:
*******************************************************************************

- On the 'Add Favorite' dialog, you can now also browse for a directory instead
  of having to manually type it in.
- When the program asks if you want to update, you can now cancel it by
  pressing ESC too.
- Added function "Clear Log" to the menu "Log" on the main screen.
- The time of HTTP requests will now also be logged.
- Directory Browsing feature icons are now built-in instead of load externally
  from the 'httpsv_icons' directory.
- Fixed the bug that would cause the 'Auto-Start' dialog occasionally.
- Fixed the bug that would allow another Content-Type header to be sent than
  needed, the Content-Type was a global variable but since HTTP Server is
  multi-threaded it should be a local variable for each thread.
- Fixed the bug in the charset save function, values of an INI header before
  the charset header could get truncated in earlier versions.
- Sometimes file uploads (from server to client) suddenly aborted, this is now
  fixed.
- Removed the balloon from the notification area icon, it didn't serve any use
  really neither now or in the future.
- Removed the '-' for the 'Size' column for directories in the Directory
  Browsing feature.
- Used IsBadReadPtr before to let the 'Stop Host' code now that the user wanted
  to host another directory (when choosing one from the favorites), but I now
  discovered I used "SendMessage", which is immediately sent to the particular
  code. So no difficult ways are needed, just a global flag which is now called
  'favAbortFlag'.
- Program was sending wrong 'Content-Size' for error 400, 403, 404, 501 and
  503, fixed.
- The ChangeLog file (this one) is now readable from commandlines because all
  lines now have a maximum width of 80 characters.

Version 1.4.8 at Thursday, December 28, 2006:
*******************************************************************************

- New feature: charset can now be changed. A "bug" has been found by Lars Song,
  on a computer with a chinese charset, the directory browsing layout
  cluttered. For more information on which charset to use, refer to the help
  file. Once you have set the charset, it will be saved to the configuration
  file (but can still be changed later).
- Fixed a possible bug which would make the program close a connection while
  it's still active in some rare case.
- Fixed a bug which would make the program keep connections alive while they
  aren't (due to a bug in the keep alive checking algorithm).
- From now on 'httpsv.cfg' is being used as configuration file instead of
  'favorites.ini' (before, the configuration file was only used for the
  favorites).

Version 1.4.7 at Wednesday, December 20, 2006:
*******************************************************************************

- Compatibility issue fixed: now picking up thread variable from stack instead
  of CPU register (EBX). This to prevent errors with newer versions of Windows.
- Fixed irritating balloon of showing up each time the notification area icon
  changes.
- Fixed another irritating balloon of showing up when HTTP Server is started
  hidden.
- Directories can now be opened from the main dialog (for quick access to the
  webroot).

Version 1.4.6 at Thursday, September 28, 2006:
*******************************************************************************

- New icons for the Directory Browsing feature, turn Directory Browsing on and
  host a folder without
  index.htm(l) to see the icons.
- The port and directory browsing settings can now be given for each favorite.
  Read more about it in the help file.
- Now HTTP Server will send 403 status instead of 404 when the requested
  resource is not available. For example when a directory is trying to be
  browsed while 'Directory Browsing' is turned off.
- New parsing method for parsing a request file path, now when a
  browser/download manager/grabber sends a request with a file path which
  includes a SPACE character without formatting it in hexadecimal string format
  (%20 etc) it will now be parsed correctly.
- Bug fix: before when using the 'Stop Server' function from the menu, all
  current active connections we're kept active untill they we're done sending
  files, now all connections will be closed.
- Bug fix: download resuming (Accept-Ranges: bytes HEADER) is now working
  again, because of support for files over 4GB (included in v1.4.4) the
  download resuming feature didn't work anymore, now fixed.

Version 1.4.5 at Monday, September 18, 2006:
*******************************************************************************

- Extended the Directory Browsing feature with new sub-features, new
  sub-features include viewing filesize and last-modification times, also a
  "../ Parent Directory" link is now included in Directory Browsing. File sizes
  are rounded at 2 decimals.

Version 1.4.4 at Sunday, September 17, 2006:
*******************************************************************************

- Added information on router port forwarding in the help file 'help.html'.
- Saw I forgot to change the file version resource for the properties window of
  versions 1.4.1 till 1.4.3 but leaving this unchanged, next versions will have
  this set correctly. This will not affect HTTP Server's behaviour.
- Fixed a bug which made it impossible to use the auto-start feature. Messages
  with 'No '\' characters are allowed' etc kept coming up when trying to setup
  the HTTP Server for startup at Windows logon. Now fixed.
- Support for files over the maximum 32-bits value of 32-bits CPU's, which
  makes it now possible to share files equal to or over 4 GiB (4294967296
  bytes) in size.

Version 1.4.3 at Thursday, September 14, 2006:
*******************************************************************************

- Fixed various bugs in the update system which previously made it impossible
  to donwload an update if the user is not using english XP version (where the
  desktop folder is called 'Desktop').
- Updated the help file 'help.html' with more information on the auto-start
  feature and how to use favorites.

Version 1.4.2 at Wednesday, September 13, 2006:
*******************************************************************************

- Added auto-start feature which makes it possible to start HTTP Server
  at Windows logon, read 'help.html' for more information.

Version 1.4.1 at Sunday, September 10, 2006:
*******************************************************************************

- Added update system, now when a new version of HTTP Server is available from
  SourceForge.net, HTTP Server will notify about this at startup.
- Made the server faster, now memory is once allocated for sending a file, then
  cleaned up (but not freed) and used for the next send iteration (if file is
  larger than 1 MiB) and freed after sending the whole file. This can greatly
  improve the server speed when users download files over 1 MiB.
- Now more information in the help file about how to automatically startup HTTP
  Server at system startup. Also information on how to create a CD with AUTORUN
  to automatically startup HTTP Server from CD is included (under the FAQ).
- Designed new icon, not really a new feature but hey :)
- Now using base directory (where 'httpsv.exe' resides) as absolute path to use
  as prefix for relative paths. Then 'help.html' file (or other files like
  'httpsv.cfg') can still be accessed by HTTP Server even when running the HTTP
  Server from a Toolbar/Start Menu (which would confuse the base path) rather
  than running HTTP Server from Windows Explorer.
- It's now possible to pass arguments such as to enable directory browsing or
  which port/directory to use on the commandline, check the HTML Help document
  for more information.
- When 'httpsv.cfg' is being read in READ-ONLY mode (for example if the file
  has been written on a CD or the READ-ONLY attribute is set), the 'httpsv.cfg'
  file is first copied to the users own temporary directory, and then parsed,
  but HTTP Server will still 'know' that the original file is being located on
  CD so you can't add new favorites. This is needed so that extra EMPTY new
  lines in the file will not make the numbering system increase the next menu
  item unnecessary. If 'httpsv.cfg' can't be copied to the temporary directory,
  the standard access error message will be displayed.

Version 1.4 at Friday, August 18, 2006:
*******************************************************************************

- Added support for automatic HTML file detection. This requires some
  explanation: when a file is requested like '/test' then first is checked if
  this file exists. If it doesn't exist, the extension '.htm' is appended. Then
  again is checked '/test.htm' exists. If it doesn't exist, the extension
  '.html' is appended and a new check is done.
- Added Favorites function, now favorite hosting directories can be selected
  from menu.
- Fixed Hex String Conversion Bug, lower case 'a' is substracted with value
  that should be used for uppercase 'a' (same for 'b', 'c', 'd', 'e', 'f', 'B',
  'C', 'D', 'E' and 'F').
- Fixed code layout (visible in source codes only).
- Small readme update.
- Added a little hyperlink in the About Box to the HTTP Server page at
  SourceForge.net.
- New function -> View/Save Log File (all IP's of connected clients and time of
  connection are logged).
- Made the program accept Absolute URI's.
- Made the program only display Bad Request (status 400) when 'host' not found
  on a HTTP v1.1 request or later.
- When Directory Browsing feature is enabled, users can see the version of HTTP
  Server at top-right bottom this would make it more vulnerable because users
  can see whether you have the latest version or not.

Version 1.3 at Sunday, June 18, 2006:
*******************************************************************************

- When you have 'index.htm' as root and not 'index.html', version 1.2 gave a
  404 status, now fixed.
- Internally, HTTP Server searches for the end of an HTTP request, but didn't
  check for memory boundaries, which made the HTTP Server crash, now a fixed
  boundary is set.

Version 1.2 at Sunday, May 14, 2006:
*******************************************************************************

- Bad Request (status 400) displayed 404 on the page, fixed it to 400.

Version 1.1 at Wednesday, April 19, 2006:
*******************************************************************************

- Resume downloads support.
- Added this ChangeLog ;-)

Version 1 at Sunday, April 9, 2006:
*******************************************************************************

HTTP Server by BugHunter is born, no added support for resuming downloads yet
however :) Current release should be stable (as far as I know) and is optimized
for low memory usage even during file transfers, have fun using it :)
